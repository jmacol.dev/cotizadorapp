// import 'package:animate_do/animate_do.dart';
import 'package:animate_do/animate_do.dart';
import 'package:appcotizador/src/db/dbProvider.dart';
import 'package:appcotizador/src/model/cotizacion.dart';
import 'package:appcotizador/src/model/usuario.dart';
// import 'package:appcotizador/src/pages/cotizacionpdf.dart';
import 'package:appcotizador/src/pages/login.dart';
import 'package:appcotizador/src/widgets/header.dart';
import 'package:appcotizador/src/widgets/menu.dart';
import 'package:appcotizador/src/widgets/module.dart';
import 'package:flutter/material.dart';
import 'package:flutter/scheduler.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';


void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  final prefs = new Sesion();
  final db = new DbProvider();
  List<Cotizacion> dataax = await db.fetchCotizacion(true);
  prefs.pendientes = dataax.length;
}
class Dashboard extends StatefulWidget  {
  Dashboard({Key key}) : super(key: key);
  @override
  DashboardPageState createState() => DashboardPageState();
  }

class DashboardPageState extends State<Dashboard>{
  final TextEditingController  usuarioCtrl = new TextEditingController();
  final  TextEditingController  claveCtrl = new TextEditingController();
  
  
  @override
  Widget build(BuildContext context){ 
    final prefs = new Sesion();
    isLoadingsincronizar();
    if(!prefs.sesion){
      SchedulerBinding.instance.addPostFrameCallback((_) {
        Navigator.push(
              context,
              new MaterialPageRoute(
                  builder: (context) => Login()));
      });     
    }
    
      return Scaffold(
        // body: Center(child:  Text('App Cotizador'),)
        drawer: NavDrawer(),
        appBar: AppBar(          
          backgroundColor: Color(0xff084B8A),
          title: Text('', style:TextStyle(fontSize: 36)),
          elevation: 0,
          
      ),
        body: Stack(
          
          children: <Widget>[
         


          Container(
            margin: EdgeInsets.only( top: 165 ),
            child: ListView(
              physics: BouncingScrollPhysics(),
              children: <Widget>[
                SizedBox( height: 120, ),
                FadeInLeft(        
                  duration: Duration( milliseconds: 250 ),
                  child: BotonDashboard(
                    icon: FontAwesomeIcons.syncAlt,
                    texto:  'Sincronizar',
                    color1: Color(0xff6989F5),
                    color2:  Color(0xff906EF5),
                    onPress: () async {
                      setState((){
                        isLoading = true;                      
                      });
                      // await prefs.sincronizardatos(context).then((value) async{
                      //   if(value == true){                          
                      //     await db.updateUsuarioSync(1);
                      //     prefs.toastmessage("Sincronización completa!", Colors.greenAccent, Colors.black);
                      //     Navigator.pushReplacementNamed(context, 'dashboard');                        
                      //     }
                      // });
                      // setState((){
                      //   isLoading = false;                      
                      // });
                    }
                      )
                  ),
                FadeInLeft(        
                  duration: Duration( milliseconds: 250 ),
                  child: BotonDashboard(
                    icon: FontAwesomeIcons.userTag,
                    texto:  'Evaluar cliente',
                    color1: Color(0xff66A9F2),
                    color2:  Color(0xff536CF6),
                    onPress: () async {
                      if(prefs.sesion && prefs.token.toString().trim().isNotEmpty){ 
                        Usuario useraux = await db.fetchUsuario();
                        if(useraux.sincronizar == '1'){
                          prefs.datosevaluar = true;
                          prefs.clienteencontrado = false;
                          Navigator.pushReplacementNamed(context, 'evaluar');
                        }else{
                          prefs.toastmessage("Debe sincronizar la data para poder iniciar su trabajo.", Colors.black87, Colors.yellow[700]);        
                        }
                      }else{
                        Navigator.pushReplacementNamed(context, 'login');
                      }      
                    },
                  ),
                ),
                FadeInLeft(        
                  duration: Duration( milliseconds: 250 ),
                  child: BotonDashboard(
                    icon: FontAwesomeIcons.shareSquare,
                    texto:  'Enviar pendientes (${prefs.pendientes.toString()})',
                    color1: Color(0xffF2D572),
                    color2:  Color(0xffE06AA3),
                    onPress: () async {
                      setState(() {
                        isLoading = true;  
                        prefs.enviarpendientescotizaciones(context);  
                        isLoading = false;
                      });
                    },
                  ),
                ),
                FadeInLeft(        
                  duration: Duration( milliseconds: 250 ),
                  child: BotonDashboard(
                    icon: FontAwesomeIcons.clipboardList,
                    texto:  'Mis cotizaciones',
                    color1: Color(0xff317183), 
                    color2:  Color(0xff46997D),
                    onPress: () async {
                      Navigator.pushReplacementNamed(context, 'cotizalist');                  
                    },
                  ),
                ),
                // ...itemMap
              ],
            ),
          ),
          Visibility(
           visible: isLoading,
           child: 
         new Container(
            child: new Stack(
              children: <Widget>[
                new Container(
                  alignment: AlignmentDirectional.center,
                  decoration: new BoxDecoration(
                    color: Colors.white70,
                  ),
                  child: new Container(
                    decoration: new BoxDecoration(
                      color: Colors.black87,
                      borderRadius: new BorderRadius.circular(10.0)
                    ),
                    width: 300.0,
                    height: 200.0,
                    alignment: AlignmentDirectional.center,
                    child: new Column(
                      crossAxisAlignment: CrossAxisAlignment.center,
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: <Widget>[
                        new Center(
                          child: new SizedBox(
                            height: 50.0,
                            width: 50.0,
                            child: new CircularProgressIndicator(
                              value: null,
                              strokeWidth: 7.0,
                            ),
                          ),
                        ),
                        new Container(
                          margin: const EdgeInsets.only(top: 25.0),
                          child: new Center(
                            child: new Text(
                              "Cargando...",
                              style: new TextStyle(
                                color: Colors.white, fontSize: 30
                              ),
                            ),
                          ),
                        ),
                      ],
                    ),
                  ),
                ),
              ],
            ),
          )
         ),
          
          Header(
            icon: FontAwesomeIcons.elementor, 
                titulo: 'Cotizador - Ventas',
                subtitulo: 'Dashboard', hdif:50),
                
                 
          ],
        ),
        
        floatingActionButton: FloatingActionButton(
          onPressed: () async {   
              
              // reportView(context, 1);
            prefs.cerrarsesion(context);
          },
          child: Icon(FontAwesomeIcons.signOutAlt),
          backgroundColor: Colors.redAccent,
        ),
      );
    

  }

  isLoadingsincronizar()async{
    if(isLoading){
      await prefs.sincronizardatos(context).then((value) async{
        if(value == true){                          
          await db.updateUsuarioSync(1);
          prefs.toastmessage("Sincronización completa!", Colors.greenAccent, Colors.black);
        }
      });
      setState((){
        isLoading = false;                      
      });
    }
  }
  
}

