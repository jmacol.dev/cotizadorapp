class Categoria{  
  final int id;
  final String codigo;
  final String descripcion;
  final String categoria;
  final int superior;

  Categoria({this.id,this.codigo,this.descripcion,this.categoria, this.superior});

  Map<String,dynamic> toMap(){ // used when inserting data to the database
    return <String,dynamic>{
      "id" : id,
      "codigo" : codigo,
      "descripcion" : descripcion,
      "categoria" : categoria,
      "superior" : superior
    };
  }
}