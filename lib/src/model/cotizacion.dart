class Cotizacion{
  final int id;
  final String dni;
  final int enviada;  
  final int combo;
  final int edad;
  final int idvivienda;
  final String idsituacionlaboral;
  final String idestabilidadlaboral;
  final String idgiroactividad;
  final String idzona;
  final int cuotas;
  final int excepcion;
  final double porcentajeinicial;
  final int plazoinicial;
  final double pagoinicial;
  final double preciocompra;
  final double descuentopromo;
  final double precioreal;
  final double montocuota;
  final double montocredito;
  final double totalcredito;
  final String observacion;
  final int negociacion;
  final int idresultado;
  final int idcontacto;
  final int idpersonalregistro;
  final int asesorvta;
  final int proceso;
  final String direccion;
  final String telefono;
  final String codigoubigeo  ;
  final double factor;
  final double factortem;
  final double tea;
  final double tem;
  final double tceaimp;
  final double tcemimp;
  final double tcea;
  final double tcem;
  final double igv;
  final double inicialorig;
  final int medio;
  final String fecharegistro;

  Cotizacion({
      this.id,
      this.dni,
      this.enviada,
      this.combo,
      this.edad,
      this.idvivienda,
      this.idsituacionlaboral,
      this.idestabilidadlaboral,
      this.idgiroactividad,
      this.idzona,
      this.cuotas,
      this.excepcion,
      this.porcentajeinicial,
      this.plazoinicial,
      this.pagoinicial,
      this.preciocompra,
      this.descuentopromo,
      this.precioreal,
      this.montocuota,
      this.montocredito,
      this.totalcredito,
      this.observacion,
      this.negociacion,
      this.idresultado,
      this.idcontacto,
      this.idpersonalregistro,
      this.asesorvta,
      this.proceso,
      this.direccion,
      this.telefono,
      this.codigoubigeo,
      this.factor,
      this.factortem,
      this.tea,
      this.tem,
      this.tceaimp,
      this.tcemimp,
      this.tcea,
      this.tcem,
      this.igv,
      this.inicialorig,
      this.medio,
      this.fecharegistro
  });

    Map<String,dynamic> toMap(){ // used when inserting data to the database
    return <String,dynamic>{
      "id":id,
      "dni":dni,
      "enviada":enviada,
      "combo" : combo,
      "edad" : edad,
      "idvivienda" : idvivienda,
      "idsituacionlaboral" : idsituacionlaboral,
      "idestabilidadlaboral" : idestabilidadlaboral,
      "idgiroactividad" : idgiroactividad,
      "idzona" : idzona,
      "cuotas" : cuotas,
      "excepcion" : excepcion,
      "porcentajeinicial" : porcentajeinicial,
      "plazoinicial" : plazoinicial,
      "pagoinicial" : pagoinicial,
      "preciocompra" : preciocompra,
      "descuentopromo" : descuentopromo,
      "precioreal" : precioreal,
      "montocuota" : montocuota,
      "montocredito" : montocredito,
      "totalcredito" : totalcredito,
      "observacion" : observacion,
      "negociacion" : negociacion,
      "idresultado" : idresultado,
      "idcontacto" : idcontacto,
      "idpersonalregistro" : idpersonalregistro,
      "asesorvta" : asesorvta,
      "proceso" : proceso,
      "direccion" : direccion,
      "telefono" : telefono,
      "codigoubigeo" : codigoubigeo,
      "factor" : factor,
      "factortem" : factortem,
      "tea" : tea,
      "tem" : tem,
      "tceaimp" : tceaimp,
      "tcemimp" : tcemimp,
      "tcea" : tcea,
      "tcem" : tcem,
      "igv" : igv,
      "inicialorig" : inicialorig,
      "medio" : medio,
      "fecharegistro" : fecharegistro
    };
  }
}