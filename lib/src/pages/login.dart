import 'package:appcotizador/src/db/dbProvider.dart';
import 'package:appcotizador/src/model/cotizacion.dart';
import 'package:appcotizador/src/model/local.dart';
import 'package:appcotizador/src/pages/dashboard.dart';
import 'package:appcotizador/src/widgets/header.dart';
import 'package:flutter/material.dart';
import 'package:flutter/scheduler.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:appcotizador/src/widgets/module.dart';
import 'dart:convert'; 
import 'package:http/http.dart';
// import 'package:sweetalert/sweetalert.dart';
String mensajeval = "";
bool showmsj = false;
class Login extends StatefulWidget  {
  Login({Key key}) : super(key: key);

  @override
  LoginPageState createState() => LoginPageState();
  }
class LoginPageState extends State<Login> {
  final TextEditingController  usuarioCtrl = new TextEditingController();
  final  TextEditingController  claveCtrl = new TextEditingController();
  final globalKey = new GlobalKey<ScaffoldState>();
  int codlocal = 0;
  
  bool showloc = false;
   
 
  @override
  Widget build(BuildContext context){
    final prefs = new Sesion();
    if(prefs.sesion && prefs.local > 0){
      SchedulerBinding.instance.addPostFrameCallback((_) {
        Navigator.push(
              context,
              new MaterialPageRoute(
                  builder: (context) => Dashboard()));
      });     
    }

    
    
  
  return Scaffold(
    // body: Center(child:  Text('App Cotizador'),)
    key: globalKey,
    body: Stack(
      
      children: <Widget>[
      Container(
        margin: EdgeInsets.only(top:350, left: 70, right: 70),
        
        child: SingleChildScrollView(
          child:       
            Column(
              
              children: <Widget>[ 
                  
                Padding(                
                    padding: EdgeInsets.all(8),
                    child:       
                    TextForm(
                      show:!prefs.sesionLocal,
                      icon: FontAwesomeIcons.userAlt,
                      item: TextFormField(
                        style: new TextStyle(fontSize: 24),
                        onChanged: (newValue) {
                        setState(() {                        
                          showmsj = false;
                          mensajeval = "";
                          if(usuarioCtrl.text.trim().isEmpty){
                            // showmsj = true;
                            // mensajeval = "El usuario es requerido!";
                            prefs.toastmessage("El usuario es requerido", Colors.redAccent, Colors.white);
                          }
                        });
                      },
                        controller: usuarioCtrl,
                        decoration: new InputDecoration(
                          labelText: 'Ingrese usuario',
                          
                        ),
                        validator: validaUsuario,
                      ))),
                    
                Padding(
                    padding: EdgeInsets.all(8),
                    child:           
                    TextForm(
                      show:!prefs.sesionLocal,
                      icon: FontAwesomeIcons.key,
                      item: TextFormField(
                      style: new TextStyle(fontSize: 24),
                      onChanged: (newValue) {
                      setState(() {                        
                          showmsj = false;
                          mensajeval = "";
                          if(claveCtrl.text.trim().isEmpty){
                            // showmsj = true;
                            // mensajeval = "La contraseña es requerida!";
                            prefs.toastmessage("La contraseña es requerida.", Colors.redAccent, Colors.white);
                          }
                        });
                      },
                      controller: claveCtrl,                      
                      obscureText: true,
                      decoration: new InputDecoration(
                        labelText: 'Ingrese contraseña',                    
                      ),
                      validator: validaClave,
                    ))),
                Padding(     
                  padding: EdgeInsets.only(right: 8, left: 8, top: 20),
                    child:           
                    TextForm(
                    show:prefs.sesionLocal,
                    icon: FontAwesomeIcons.building,
                    item: DropdownButtonFormField(
                      
                    style:  new TextStyle(fontSize: 24, color: Colors.black54),
                    items: locales.map((Locales local) {
                      return new DropdownMenuItem(
                        value: local.codigo,
                        child: Row(
                          children: <Widget>[
                            Text(local.descripcion),
                          ],
                        )
                        );
                      }).toList(),
                      onChanged: (newValue) async {
                        codlocal = newValue;
                        if(codlocal > 0){
                          await estadovariables("", false); 
                        }
                      },
                      value: codlocal,
                        ),)
                ),
                    
                GestureDetector(
                  onTap: (){                    
                      login(context);
                  },
                  child: Container(
                        margin: new EdgeInsets.all(30.0),
                        alignment: Alignment.center,
                        color: Color(0xff084B8A),
                        
                        child: Text(prefs.botonSesion ,
                            style: TextStyle(
                                color: Colors.white,
                                fontSize: 24,
                                fontWeight: FontWeight.w500)),
                        padding: EdgeInsets.only(top: 16, bottom: 16),
                      )),
                      Visibility(                      
                        visible: showmsj,                      
                        child :Padding(    
                            
                        padding: EdgeInsets.symmetric(vertical: 5),     
                          child: Container(          
                            margin: new EdgeInsets.symmetric(horizontal: 30)      ,          
                          alignment: Alignment.center,
                        child: Card(  
                                          
                          color: Colors.white,
                            borderOnForeground: false,
                            
                            elevation: 0,
                            shape: 
                            RoundedRectangleBorder(borderRadius: BorderRadius.circular(0),
                            side: BorderSide(
                              color: Colors.white,
                              width: 0,
                            ),),
                            child: 
                            
                            ListTile(leading: Icon(FontAwesomeIcons.infoCircle, size: 30, color: Colors.red[600],), title: Text(mensajeval, style: TextStyle(fontSize: 20, color: Colors.red[600])))                   
                        ))
                      ))
                
              ],
            ),
          ),
        ),
        Header(
          icon: FontAwesomeIcons.lock, 
                titulo: 'Grupo Leoncito',
                subtitulo: 'Inicio Sesión', hdif:0),
                
      ] ),        
    );
  }

  estadovariables(String msj, bool state)  async{
    setState(() {
      showmsj = state;
      mensajeval = msj;          
    });

    await Future.delayed(const Duration(milliseconds: 2500), () async{
      if (mounted){
        setState(() {
          showmsj = false;
          mensajeval = "";          
        });
      }
    }); 
  }

  login(BuildContext context) async{
    final prefs = new Sesion();
    DbProvider db = DbProvider();
    try{
    if(!prefs.sesion){ 
      // VALIDA SINO EXISTE SESIÓN
      if(usuarioCtrl.text.trim().isEmpty && claveCtrl.text.trim().isEmpty){
          prefs.toastmessage("Ingrese sus credenciales", Colors.redAccent, Colors.white);        
          return false;
      }

      if(usuarioCtrl.text.trim().isEmpty){        
        prefs.toastmessage("El usuario es requerido", Colors.redAccent, Colors.white);        
        return false;
      }

      if(claveCtrl.text.trim().isEmpty){
        prefs.toastmessage("La contraseña es requerida", Colors.redAccent, Colors.white);        
        return false;
      }
      prefs.toastmessage("Validando credenciales.", Colors.black, Colors.white);
      if(!prefs.sincroniza){
        // VALIDA SINO EXISTE SESIÓN - USUARIO NO HA SINCRONIZADO
        var stateaux = await conectnet();
        if(!stateaux){
          var usuarioaux = await db.fetchUsuario();
          if(usuarioaux != null){
            sesionsinconexion();            
          }else{
            prefs.toastmessage("Verifique su conexión a internet y vuelve a iniciar sesión.", Colors.black, Colors.white);        
            return false;
          }
        }else{
        //VALIDA SINO EXISTE SESIÓN - USUARIO NO HA SINCRONIZADO LOGIN"
        await iniciarSesionAPI();
        }
      }else{
        //VALIDA SINO EXISTE SESIÓN - USUARIO HA SINCRONIZADO
        var stateaux = await conectnet();
        if(!stateaux){
          //VALIDA SINO EXISTE SESIÓN - USUARIO HA SINCRONIZADO - NO HAY CONEXIÓN A INTERNET
          sesionsinconexion();
        }else{
          // VALIDA SINO EXISTE SESIÓN - USUARIO HA SINCRONIZADO - SI HAY CONEXIÓN A INTERNET
          await iniciarSesionAPI();
        }   
        //login sqlite, cuando los datos hayan sido sincronizados.
      }
      
    }
       
       if(prefs.sesion && prefs.token.toString().trim().isNotEmpty){    
         if(!prefs.sesionLocal){
           prefs.sesionLocal = true;    
           prefs.botonSesion = "Ingresar";
           Navigator.pushReplacementNamed(context, 'login');  
         }else{
           if(codlocal > 0){
             var usuarioaux = await db.fetchUsuario();
             prefs.local = codlocal;
             prefs.sesionLocal = false;  
             prefs.botonSesion = "Iniciar sesión";
             List<Cotizacion> dataax = await db.fetchCotizacion(true);
             prefs.pendientes  = dataax.length;
             prefs.toastmessage("Bienvenido\n${usuarioaux.nombre}", Colors.greenAccent, Colors.black);
             Navigator.pushReplacementNamed(context, 'dashboard');  
           }else{
             prefs.toastmessage("Seleccione local.", Colors.redAccent, Colors.white);
              return false;
           }
         }
       }
    } catch(e, _) {
      prefs.toastmessage(e.toString(), Colors.redAccent, Colors.white);  
      debugPrint(e.toString());
    }
  } 

  sesionsinconexion()async{
    var stlogin = await db.loginsinconexion(usuarioCtrl.text.trim(), claveCtrl.text.trim());
          if(stlogin){
            var usuarioaux = await db.fetchUsuario();
            prefs.usuario = jsonEncode({"usuario": usuarioCtrl.text.trim(), "nombre":usuarioaux.nombre,"cargo":usuarioaux.cargo});                        
            prefs.sesion = true;
            prefs.token = usuarioaux.token;
            var localesaux = await db.fetchLocal('1');
            localesaux.forEach((element) { 
              locales.add(new Locales(element.codigo, element.descripcion));
            });
            prefs.toastmessage("Credenciales correctas.", Colors.greenAccent, Colors.black);                      
            prefs.sesionLocal = true;    
            prefs.botonSesion = "Ingresar";
            Navigator.pushReplacementNamed(context, 'login');
          }else{
            prefs.toastmessage("Las credenciales son incorrectas", Colors.redAccent, Colors.white);            
            prefs.sesion = false;
            prefs.token = "";
            prefs.usuario = "";
            return false;
          }
  }

  iniciarSesionAPI() async {
    DbProvider db = DbProvider();
    try{
    var stlogindb = await db.otroUsuario(usuarioCtrl.text.trim());
    // VERIFICA SI EL USUARIO ES DISTINTO
        if(stlogindb == -1){
          // VERIFICA SI EL USUARIO ES DISTINTO
          Map<String, dynamic> useraux = jsonDecode(prefs.usuario);
          prefs.toastmessage("No puede iniciar sesión con otro usuario, ya que el usuario "+ useraux['usuario'] + ": " + useraux['nombre']  + " tienes aun cotizaciones pendintes por enviar. \nRevise si el usuario a ingresar es correcto", Colors.redAccent, Colors.white);
          return false;
        }
      //  String json = jsonEncode({"usuario": "jmacol", "password":"G6v7Bu9sR3nd","recaptcha":true});      
        var data = await loginapi(usuarioCtrl.text.trim(), claveCtrl.text.trim());        
        if(data['estado']){
          prefs.toastmessage("Credenciales correctas.", Colors.greenAccent, Colors.black);          
          prefs.sesion = true;
          prefs.token = data['token'];
          String json = jsonEncode({"token": prefs.token});
          Response response = await URLS().postData('locales-usuario', json, true) ;
          var dataloc = jsonDecode(response.body);
          if(dataloc['estado']){            
            await db.deleteLocal();
              List list = dataloc['locales'] as List;
              list.forEach((element) async {
                locales.add(new Locales(element['zl_codigo'], element['zl_descripcion']));
                final mdlocal =  Local(
                  codigo: element['zl_codigo'],
                  descripcion: element['zl_descripcion'],
                  tipo: element['zl_tipo'],
                  estado: '1'
                );                
                await db.insertLocal(mdlocal);
              });
                
          }else{
            prefs.toastmessage(dataloc['mensaje'], Colors.redAccent, Colors.white);
            prefs.sesion = false;
            prefs.token = "";
            prefs.usuario = "";
            return false;
          }
          
      }else{
        prefs.toastmessage(data['mensaje'], Colors.redAccent, Colors.white);
        prefs.sesion = false;
        prefs.token = "";
        prefs.usuario = "";
        return false;
      }
      } catch(e, _) {
        prefs.toastmessage(e.toString(), Colors.redAccent, Colors.white);  
        debugPrint(e.toString());
      }
  }   
}
    




 String validaUsuario(String value) {
   String pattern = r'(^[a-zA-Z ]*$)';
   RegExp regExp = new RegExp(pattern);
   if (value.trim().length == 0) {
     return "El usuario es requerido.";
   } else if (!regExp.hasMatch(value)) {
     return "El usuario debe de ser a-z y A-Z.";
   }
   return null;
 }

 String validaClave(String value) {
   if (value.trim().length == 0) {
     return "La contraseña es requerida.";
   } 
   return null;
 }
