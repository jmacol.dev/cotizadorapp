import 'package:appcotizador/src/model/afectaciones.dart';
import 'package:appcotizador/src/model/canal.dart';
import 'package:appcotizador/src/model/centropoblado.dart';
import 'package:appcotizador/src/model/cliente.dart';
import 'package:appcotizador/src/model/cotizacion.dart';
import 'package:appcotizador/src/model/cronograma.dart';
import 'package:appcotizador/src/model/detallecotizacion.dart';
import 'package:appcotizador/src/model/estado.dart';
import 'package:appcotizador/src/model/factores.dart';
import 'package:appcotizador/src/model/inicialesmoto.dart';
import 'package:appcotizador/src/model/inicialesrecurrente.dart';
import 'package:appcotizador/src/model/nivelriesgo.dart';
import 'package:appcotizador/src/model/parametroscotizacion.dart';
import 'package:appcotizador/src/model/producto.dart';
import 'package:appcotizador/src/model/recurrencia.dart';
import 'package:appcotizador/src/model/resultadocotizacion.dart';
import 'package:appcotizador/src/model/listacotizacion.dart';
import 'package:appcotizador/src/model/vigencia.dart';
import 'package:appcotizador/src/widgets/module.dart';
import 'package:sqflite/sqflite.dart'; //sqflite package
import 'dart:io';
import 'dart:async';
import 'package:path_provider/path_provider.dart';
import 'package:path/path.dart'; //used to join paths
import 'package:appcotizador/src/model/usuario.dart';
import 'package:appcotizador/src/model/local.dart';
import 'package:appcotizador/src/model/categoria.dart';
import 'package:appcotizador/src/model/totalcotizacion.dart';

class DbProvider {
  Future<Database> init() async {
    Directory directory =
        await getApplicationDocumentsDirectory(); //returns a directory which stores permanent files
    final path =
        join(directory.path, "bdleoncito.db"); //create path to database

    String sqlafectaciones = """
            CREATE TABLE afectaciones(
              id INTEGER,
              descripcion VARCHAR(100),
              porcentaje REAL,
              idtipoafectacion INTEGER,
              tipoafectacion VARCHAR(100),
              vigencia BIT)""";

    String sqlcanal = """
            CREATE TABLE canal(
              id INTEGER,
              descripcion VARCHAR(100),
              tipo BIT)""";

    String sqlcentropoblado = """
            CREATE TABLE centropoblado(
              id INTEGER,
              descripcion VARCHAR(100),
              idsegmentacion INTEGER,
              iddistrito INTEGER)""";

    String sqlcliente = """
            CREATE TABLE cliente(
              id INTEGER,
              referenciaorigen INTEGER,
              dni VARCHAR(8),
              ruc VARCHAR(11),
              apellidopaterno VARCHAR(100),
              apellidomaterno VARCHAR(100),
              nombres VARCHAR(100),
              fechanacimiento VARCHAR(10),
              direccion VARCHAR(200),
              telefono VARCHAR(20),
              idevaleo INTEGER,
              idevasen INTEGER,
              asignacioncme BIT,
              cmeasignada REAL,
              cmedisponible REAL,
              lineacredito REAL,
              lineadisponible REAL,
              idresultado INTEGER,
              resultado TEXT,
              excepcion BIT,
              motivoexcepcion TEXT,
              idvigencia INTEGER,
              vigencia VARCHAR(100),
              idrecurrencia INTEGER,
              recurrencia VARCHAR(100),
              idestado INTEGER,
              estado VARCHAR(100),
              idbancarizacion INTEGER,
              bancarizacion VARCHAR(100),
              idnivelriesgo INTEGER,
              nivelriesgo VARCHAR(100),
              fecharegistro VARCHAR(20),
              registrado BIT)""";

    String sqlestado = """
            CREATE TABLE estado(
              id INTEGER,
              descripcion VARCHAR(100))""";

    String sqlfactores = """
            CREATE TABLE factores(
              numero INTEGER,
              factorcuota REAL,
              idfactorcuota INTEGER,
              factortem REAL,
              idfactortem INTEGER)""";

    String sqlinicialesmoto = """
            CREATE TABLE inicialesmoto(
              id INTEGER,
              montoinicial REAL,
              idrecurrencia INTEGER,
              tipo BIT,
              vigencia BIT)""";

    String sqlinicialesrecurrente = """
            CREATE TABLE inicialesrecurrente(
              id INTEGER,
              idvigencia INTEGER,
              idrecurrencia INTEGER,
              inicial REAL
              vigencia BIT)""";

    String sqlnivelriesgo = """
            CREATE TABLE nivelriesgo(
              id INTEGER,
              descripcion VARCHAR(100))""";

    String sqlrecurrencia = """
            CREATE TABLE recurrencia(
              id INTEGER,
              descripcion VARCHAR(100))""";

    String sqlvigencia = """
            CREATE TABLE vigencia(
              id INTEGER,
              descripcion VARCHAR(100))""";

    String sqlproducto = """
            CREATE TABLE producto(
              id INTEGER,
              segmentaciongeografica VARCHAR(50),
              cantidad REAL,
              codigomodelo INTEGER,
              nombremodelo VARCHAR(200),
              codigomarca INTEGER,
              nombremarca VARCHAR(200),
              codigolinea INTEGER,
              nombrelinea VARCHAR(200),
              precio REAL,
              regalo BIT,
              tipocotizacion VARCHAR(50),
              lineadescuento INTEGER,
              dsctonivel REAL,
              dsctosegmento REAL,
              dsctolinea REAL,
              dsctoplazo REAL,
              dsctoexcepcion REAL,
              dsctocombo REAL,
              dsctototal REAL,
              preciototal REAL,
              preciodscto REAL,
              precioreal REAL,
              vigencia BIT,
              idproductoregalo INTEGER,
              regalocant REAL,
              detalledscto TEXT)""";

    String sqlparametroscotizacion = """
            CREATE TABLE parametroscotizacion(
              id INTEGER,
              nivelriesgo INTEGER,
              tipovivienda VARCHAR(100),
              edadminima INTEGER,
              edadmaxima INTEGER,
              situacionlaboral VARCHAR(100),
              estabilidadlaboral VARCHAR(100),
              porcentajeinicial REAL,
              plazoinicial INTEGER,
              observacion TEXT,
              tipocotizacion VARCHAR(100),
              giroactividad VARCHAR(100))""";

    String sqlusuario = """ 
            CREATE TABLE usuario(
            id INTEGER,
            usuario VARCHAR(10),
            clave VARCHAR(20),
            nombre VARCHAR(200),
            cargo VARCHAR(200),
            token TEXT,
            sincronizar BIT)""";

    String sqllocal = """
            CREATE TABLE local(
            codigo INTEGER,
            descripcion VARCHAR(50),
            tipo VARCHAR(30),
            estado BIT)""";

    String sqlcategoria = """
          CREATE TABLE categoria(
          id INTEGER,
          codigo VARCHAR(10),
          descripcion VARCHAR(100),
          categoria VARCHAR(50),
          superior INTEGER)""";

    String sqltotal = """
          CREATE TABLE totalcotizacion(
          fecha VARCHAR(10),
          cantidad INTEGER,
          codigolocal INTEGER)""";

    String sqlcotizacion = """
          CREATE TABLE cotizacion(
            id INTEGER,
            dni VARCHAR(8),
            enviada INTEGER,
            combo INTEGER,
            edad INTEGER,
            idvivienda INT,
            idsituacionlaboral VARCHAR(50),
            idestabilidadlaboral VARCHAR(50),
            idgiroactividad VARCHAR(50),
            idzona  VARCHAR(50),
            cuotas INTEGER,
            excepcion BIT,
            porcentajeinicial REAL,
            plazoinicial INTEGER,
            pagoinicial  REAL,
            preciocompra  REAL,
            descuentopromo  REAL,
            precioreal  REAL,
            montocuota  REAL,
            montocredito  REAL,
            totalcredito  REAL,
            observacion TEXT,
            negociacion BIT,
            idresultado INTEGER,
            idcontacto INTEGER,
            idpersonalregistro INTEGER,
            asesorvta INTEGER,
            proceso INTEGER,
            direccion VARCHAR(200),
            telefono VARCHAR(9),
            codigoubigeo VARCHAR(20),
            factor REAL,
            factortem REAL,
            tea REAL,
            tem REAL,
            tceaimp REAL,
            tcemimp REAL,
            tcea REAL,
            tcem REAL,
            igv REAL,
            inicialorig REAL,
            medio INTEGER,
            fecharegistro VARCHAR(20)
          )""";

    String sqldetallecotizacion = """
          CREATE TABLE detallecotizacion(
            id INTEGER,
            cantidad REAL,
            idproducto INTEGER,
            descripcion TEXT,
            preciounitario REAL,
            preciototal REAL,
            dsctoperfilcrediticio REAL,
            dsctosegmentacion REAL,
            dsctolineaproducto REAL,
            dsctoplazo REAL,
            dsctocombo REAL,
            dsctoexcepcion REAL,
            descuentototal REAL,
            precioreal REAL,
            idcotizacion INTEGER
          )""";

    String sqlcronograma = """
          CREATE TABLE cronograma(
            id INTEGER,
            nrocuota INTEGER,
            amortizacion REAL,
            interes REAL,
            gasto REAL,
            monto REAL,
            igv REAL,
            saldo REAL,
            facturar REAL,
            idcotizacion INTEGER          
          )""";

    // var tables = [sqlusuario, sqllocal, sqlcategoria, sqltotal];
    // databaseExists(path).then((value) => () async {
    //   if(!value){
    return await openDatabase(
        //open the database or create a database if there isn't any
        path,
        version: 1, onCreate: (Database db, int version) async {
      await db.execute(sqlusuario);
      await db.execute(sqllocal);

      await db.execute(sqlafectaciones);
      await db.execute(sqlcanal);
      await db.execute(sqlcategoria);
      await db.execute(sqlcentropoblado);
      await db.execute(sqlcliente);
      await db.execute(sqlestado);
      await db.execute(sqlfactores);
      await db.execute(sqlinicialesmoto);
      await db.execute(sqlinicialesrecurrente);
      await db.execute(sqlnivelriesgo);
      await db.execute(sqlparametroscotizacion);
      await db.execute(sqlproducto);
      await db.execute(sqlrecurrencia);
      await db.execute(sqlvigencia);
      await db.execute(sqltotal);
      await db.execute(sqlcotizacion);
      await db.execute(sqldetallecotizacion);
      await db.execute(sqlcronograma);
      // BASE DE DATOS CREADA
    });
    // }
    // });
  }

  Future<int> deleteUsuario() async {
    final db = await init(); //open database
    int result = await db.delete("usuario");
    return result;
  }

  Future<int> otroUsuario(String usuarioaux) async {
    final prefs = new Sesion();
    final db = await init(); //open database
    int result = 0;
    final maps = await db.query("usuario");
    if (maps.toList().length > 0) {
      if (maps[0]['usuario'] != usuarioaux) {
        List<Cotizacion> dataax = await fetchCotizacion(true);
        prefs.pendientes = dataax.length;
        if (prefs.pendientes > 0) {
          result = -1;
        }
      }
    }
    return result;
  }

  Future<int> updateCotizacionSync(int id) async {
    final db = await init(); //open database
    int result = 0;
    result = await db.rawUpdate('''
          UPDATE cotizacion 
          SET enviada =  1
          WHERE id = ?
          ''', [id]);
    return result;
  }

  Future<int> updateClienteSync(String dni) async {
    final db = await init(); //open database
    int result = 0;
    result = await db.rawUpdate('''
          UPDATE cliente 
          SET registrado =  1
          WHERE dni = ?
          ''', [dni]);
    return result;
  }

  Future<int> updateUsuarioSync(int syncc) async {
    final db = await init(); //open database
    int result = 0;
    result = await db.rawUpdate('''
          UPDATE usuario 
          SET sincronizar =  ?
          ''', [syncc]);
    return result;
  }

  Future<int> loginUsuario(Usuario item) async {
    final db = await init(); //open database
    int result = 0;
    final maps = await db.query("usuario");
    if (maps.toList().length > 0) {
      // TABLA USUARIO LLENA
      if (maps[0]['usuario'] != item.usuario) {
        // INGRESO DE OTRO USUARIO
        await deleteUsuario();
        result = await db.insert("usuario", item.toMap());
      } else {
        //ACTUALIZA DATOS DE USUARIO
        result = await db.rawUpdate('''
          UPDATE usuario 
          SET clave = ? , token = ?, nombre = ?, cargo = ? 
          WHERE usuario = ?
          ''', [item.clave, item.token, item.nombre, item.cargo, item.usuario]);
      }
    } else {
      // INGRESO DE NUEVO USUARIO
      await deleteUsuario();
      result = await db.insert("usuario", item.toMap());
    }

    return result;
  }

  Future<int> insertLocal(Local item) async {
    final db = await init(); //open database
    return db.insert(
      "local",
      item.toMap(),
      conflictAlgorithm: ConflictAlgorithm.ignore,
    );
  }

  Future<int> insertCotizacion(Cotizacion item) async {
    final db = await init(); //open database
    return db.insert(
      "cotizacion",
      item.toMap(),
      conflictAlgorithm: ConflictAlgorithm.ignore,
    );
  }

  Future<int> insertDetalleCotizacion(DetalleCotizacion item) async {
    final db = await init(); //open database
    return db.insert(
      "detallecotizacion",
      item.toMap(),
      conflictAlgorithm: ConflictAlgorithm.ignore,
    );
  }

  Future<int> insertCronograma(Cronograma item) async {
    final db = await init(); //open database
    return db.insert(
      "cronograma",
      item.toMap(),
      conflictAlgorithm: ConflictAlgorithm.ignore,
    );
  }

  Future maxIdCotizacion() async {
    final db = await init();
    return await db.rawQuery("SELECT MAX(id) idmax FROM cotizacion");
  }

  Future maxIdDetalleCotizacion() async {
    final db = await init();
    return await db.rawQuery("SELECT MAX(id) idmax FROM detallecotizacion");
  }

  Future maxIdCronograma() async {
    final db = await init();
    return await db.rawQuery("SELECT MAX(id) idmax FROM cronograma");
  }

  Future<int> insertCategoria(StringBuffer lista) async {
    final db = await init(); //open database
    await db.transaction((txn) async {
      var batch = txn.batch();
      batch.rawInsert(
          "INSERT INTO categoria(id, codigo, descripcion, categoria, superior) "
          "VALUES ${lista.toString()}");
      await batch.commit(noResult: true);
    });
    return 1;
  }

  Future<int> insertAfectaciones(Afectaciones item) async {
    final db = await init(); //open database
    return db.rawInsert(
        "INSERT INTO afectaciones(id,descripcion,porcentaje,idtipoafectacion,tipoafectacion,vigencia) VALUES(${item.id},'${item.descripcion}',${item.porcentaje},${item.idtipoafectacion},'${item.tipoafectacion}',${item.vigencia})");
  }

  Future<int> insertCanal(Canal item) async {
    final db = await init(); //open database
    return db.insert(
      "canal",
      item.toMap(),
      conflictAlgorithm: ConflictAlgorithm.ignore,
    );
  }

  Future<int> insertCentroPoblado(CentroPoblado item) async {
    final db = await init(); //open database
    await db.transaction((txn) async {
      var batch = txn.batch();
      batch.insert("centropoblado", item.toMap());
      await batch.commit(noResult: true);
    });
    return item.id;
  }

  Future<int> insertCliente(Cliente item) async {
    final db = await init(); //open database
    return db.insert(
      "cliente",
      item.toMap(),
      conflictAlgorithm: ConflictAlgorithm.ignore,
    );
  }

  // Future<int> insertClienteBatch(Cliente item, bool state) async{
  //   try{
  //     final db = await init();
  //     Batch batch = db.batch();
  //     batch.insert("cliente", item.toMap());
  //       batch.commit();

  //   }catch(e, _) { print(e.toString());}
  //   return 1;
  // }

  // Future<int> insertProductosBatch(Producto item, bool state) async{
  //   try{
  //     final db = await init();
  //     Batch batch = db.batch();
  //     batch.insert("producto", item.toMap());
  //     batch.commit();

  //   }catch(e, _) { print(e.toString());}
  //   return 1;
  // }

  Future<int> insertProductosBatch(List<Producto> data) async {
    try {
      final db = await init();
      Batch batch = db.batch();
      var buffer = new StringBuffer();
      data.forEach((c) {
        if (buffer.isNotEmpty) {
          buffer.write(",\n");
        }
        buffer.write("(");
        buffer.write(c.id);
        buffer.write(", '");
        buffer.write(c.segmentaciongeografica);
        buffer.write("', ");
        buffer.write(c.cantidad);
        buffer.write(", ");
        buffer.write(c.codigomodelo);
        buffer.write(", '");
        buffer.write(c.nombremodelo);
        buffer.write("', ");
        buffer.write(c.codigomarca);
        buffer.write(", '");
        buffer.write(c.nombremarca);
        buffer.write("', ");
        buffer.write(c.codigolinea);
        buffer.write(", '");
        buffer.write(c.nombrelinea);
        buffer.write("', ");
        buffer.write(c.precio);
        buffer.write(", ");
        buffer.write(c.regalo);
        buffer.write(", '");
        buffer.write(c.tipocotizacion);
        buffer.write("', ");
        buffer.write(c.lineadescuento);
        buffer.write(", ");
        buffer.write(c.dsctonivel);
        buffer.write(", ");
        buffer.write(c.dsctosegmento);
        buffer.write(", ");
        buffer.write(c.dsctolinea);
        buffer.write(", ");
        buffer.write(c.dsctoplazo);
        buffer.write(", ");
        buffer.write(c.dsctoexcepcion);
        buffer.write(", ");
        buffer.write(c.dsctocombo);
        buffer.write(", ");
        buffer.write(c.dsctototal);
        buffer.write(", ");
        buffer.write(c.preciototal);
        buffer.write(", ");
        buffer.write(c.preciodscto);
        buffer.write(", ");
        buffer.write(c.precioreal);
        buffer.write(", ");
        buffer.write(c.vigencia);
        buffer.write(", ");
        buffer.write(c.idproductoregalo);
        buffer.write(", ");
        buffer.write(c.regalocant);
        buffer.write(", '");
        buffer.write(c.detalledscto);
        buffer.write("')");
      });
      batch.rawInsert("INSERT Into producto (id,segmentaciongeografica, "
          " cantidad, "
          " codigomodelo, "
          " nombremodelo, "
          " codigomarca, "
          " nombremarca, "
          " codigolinea, "
          " nombrelinea, "
          " precio, "
          " regalo, "
          " tipocotizacion, "
          " lineadescuento, "
          " dsctonivel, "
          " dsctosegmento, "
          " dsctolinea, "
          " dsctoplazo, "
          " dsctoexcepcion, "
          " dsctocombo, "
          " dsctototal, "
          " preciototal, "
          " preciodscto, "
          " precioreal, "
          " vigencia, "
          " idproductoregalo, "
          " regalocant, "
          " detalledscto)"
          " VALUES ${buffer.toString()}");
      batch.commit();
    } catch (e, _) {
      print(e.toString());
    }
    return 1;
  }

  Future<int> insertCategoriaBatch(List<Categoria> data) async {
    try {
      final db = await init();
      Batch batch = db.batch();
      var buffer = new StringBuffer();
      data.forEach((c) {
        if (buffer.isNotEmpty) {
          buffer.write(",\n");
        }
        buffer.write("(");
        buffer.write(c.id);
        buffer.write(", '");
        buffer.write(c.codigo);
        buffer.write("', '");
        buffer.write(c.descripcion);
        buffer.write("', '");
        buffer.write(c.categoria);
        buffer.write("', ");
        buffer.write(c.superior);
        buffer.write(")");
      });
      batch.rawInsert(
          "INSERT Into categoria (id,codigo,descripcion,categoria,superior)"
          " VALUES ${buffer.toString()}");
      batch.commit();
    } catch (e, _) {
      print(e.toString());
    }
    return 1;
  }

  Future<int> insertCentroPobladoBatch(List<CentroPoblado> data) async {
    try {
      final db = await init();
      Batch batch = db.batch();
      var buffer = new StringBuffer();
      data.forEach((c) {
        if (buffer.isNotEmpty) {
          buffer.write(",\n");
        }
        buffer.write("(");
        buffer.write(c.id);
        buffer.write(", '");
        buffer.write(c.descripcion);
        buffer.write("', ");
        buffer.write(c.idsegmentacion);
        buffer.write(", ");
        buffer.write(c.iddistrito);
        buffer.write(")");
      });
      batch.rawInsert(
          "INSERT Into centropoblado (id,descripcion,idsegmentacion,iddistrito)"
          " VALUES ${buffer.toString()}");
      batch.commit();
    } catch (e, _) {
      print(e.toString());
    }
    return 1;
  }

  Future<int> insertClienteBatch(List<Cliente> data) async {
    try {
      final db = await init();
      Batch batch = db.batch();
      var buffer = new StringBuffer();
      data.forEach((c) {
        if (buffer.isNotEmpty) {
          buffer.write(",\n");
        }
        buffer.write("(");
        buffer.write(c.id);
        buffer.write(", ");
        buffer.write(c.referenciaorigen);
        buffer.write(", '");
        buffer.write(c.dni);
        buffer.write("', '");
        buffer.write(c.ruc);
        buffer.write("', '");
        buffer.write(c.apellidopaterno);
        buffer.write("', '");
        buffer.write(c.apellidomaterno);
        buffer.write("', '");
        buffer.write(c.nombres);
        buffer.write("', '");
        buffer.write(c.fechanacimiento);
        buffer.write("', '");
        buffer.write(c.direccion);
        buffer.write("', '");
        buffer.write(c.telefono);
        buffer.write("', ");
        buffer.write(c.idevaleo);
        buffer.write(", ");
        buffer.write(c.idevasen);
        buffer.write(", ");
        buffer.write(c.asignacioncme);
        buffer.write(", ");
        buffer.write(c.cmeasignada);
        buffer.write(", ");
        buffer.write(c.cmedisponible);
        buffer.write(", ");
        buffer.write(c.lineacredito);
        buffer.write(", ");
        buffer.write(c.lineadisponible);
        buffer.write(", ");
        buffer.write(c.idresultado);
        buffer.write(", '");
        buffer.write(c.resultado);
        buffer.write("', ");
        buffer.write(c.excepcion);
        buffer.write(", '");
        buffer.write(c.motivoexcepcion);
        buffer.write("', ");
        buffer.write(c.idvigencia);
        buffer.write(", '");
        buffer.write(c.vigencia);
        buffer.write("', ");
        buffer.write(c.idrecurrencia);
        buffer.write(", '");
        buffer.write(c.recurrencia);
        buffer.write("', ");
        buffer.write(c.idestado);
        buffer.write(", '");
        buffer.write(c.estado);
        buffer.write("', ");
        buffer.write(c.idbancarizacion);
        buffer.write(", '");
        buffer.write(c.bancarizacion);
        buffer.write("', ");
        buffer.write(c.idnivelriesgo);
        buffer.write(", '");
        buffer.write(c.nivelriesgo);
        buffer.write("', '");
        buffer.write(c.fecharegistro);
        buffer.write("', ");
        buffer.write(c.registrado);
        buffer.write(")");
      });
      batch.rawInsert("INSERT Into cliente (id, referenciaorigen, "
          " dni,"
          " ruc,"
          " apellidopaterno,"
          " apellidomaterno,"
          " nombres,"
          " fechanacimiento,"
          " direccion,"
          " telefono,"
          " idevaleo,"
          " idevasen,"
          " asignacioncme,"
          " cmeasignada,"
          " cmedisponible,"
          " lineacredito,"
          " lineadisponible,"
          " idresultado,"
          " resultado,"
          " excepcion,"
          " motivoexcepcion,"
          " idvigencia,"
          " vigencia,"
          " idrecurrencia,"
          " recurrencia,"
          " idestado,"
          " estado,"
          " idbancarizacion,"
          " bancarizacion,"
          " idnivelriesgo,"
          " nivelriesgo,"
          " fecharegistro,"
          " registrado )"
          " VALUES ${buffer.toString()}");
      batch.commit();
    } catch (e, _) {
      print(e.toString());
    }
    return 1;
  }

  // createCliente(Cliente item) async {
  //   final db = await init();
  //   final res = await db.insert('Employee', item.toJson());

  //   return res;
  // }

  Future<int> insertEstado(Estado item) async {
    final db = await init(); //open database
    return db.insert(
      "estado",
      item.toMap(),
      conflictAlgorithm: ConflictAlgorithm.ignore,
    );
  }

  Future<int> insertFactores(Factores item) async {
    final db = await init(); //open database
    return db.insert(
      "factores",
      item.toMap(),
      conflictAlgorithm: ConflictAlgorithm.ignore,
    );
  }

  Future<int> insertInicialesMoto(InicialesMoto item) async {
    final db = await init(); //open database
    return db.insert(
      "inicialesmoto",
      item.toMap(),
      conflictAlgorithm: ConflictAlgorithm.ignore,
    );
  }

  Future<int> insertInicialesRecurrente(InicialesRecurrente item) async {
    final db = await init(); //open database
    return db.insert(
      "inicialesrecurrente",
      item.toMap(),
      conflictAlgorithm: ConflictAlgorithm.ignore,
    );
  }

  Future<int> insertNivelRiesgo(NivelRiesgo item) async {
    final db = await init(); //open database
    return db.insert(
      "nivelriesgo",
      item.toMap(),
      conflictAlgorithm: ConflictAlgorithm.ignore,
    );
  }

  Future<int> insertParametrosCotizacion(ParametrosCotizacion item) async {
    final db = await init(); //open database

    // db.rawInsert("INSERT INTO parametroscotizacion(id,nivelriesgo,tipovivienda,edadminima,edadmaxima,situacionlaboral,estabilidadlaboral,porcentajeinicial,plazoinicial,observacion,tipocotizacion,giroactividad) VALUES(${item.id},${item.nivelriesgo},'${item.tipovivienda}',${item.edadminima},${item.edadmaxima},'${item.situacionlaboral}','${item.estabilidadlaboral}','${item.porcentajeinicial}',${item.plazoinicial},'${item.observacion}','${item.tipocotizacion}','${item.giroactividad}')");

    return db.insert(
      "parametroscotizacion",
      item.toMap(),
      conflictAlgorithm: ConflictAlgorithm.ignore,
    );
  }

  Future<int> insertProducto(Producto item) async {
    final db = await init(); //open database
    return db.insert(
      "producto",
      item.toMap(),
      conflictAlgorithm: ConflictAlgorithm.ignore,
    );
  }

  Future<int> insertRecurrencia(Recurrencia item) async {
    final db = await init(); //open database
    return db.insert(
      "recurrencia",
      item.toMap(),
      conflictAlgorithm: ConflictAlgorithm.ignore,
    );
  }

  Future<int> insertVigencia(Vigencia item) async {
    final db = await init(); //open database
    return db.insert(
      "vigencia",
      item.toMap(),
      conflictAlgorithm: ConflictAlgorithm.ignore,
    );
  }

  Future<int> insertTotalCotizacion(TotalCotizacion item) async {
    final db = await init(); //open database
    return db.insert(
      "totalcotizacion",
      item.toMap(),
      conflictAlgorithm: ConflictAlgorithm.ignore,
    );
  }

  Future<int> deleteLocal() async {
    final db = await init(); //open database
    int result = await db.delete("local");
    return result;
  }

  Future<int> deleteCotizaciones() async {
    final db = await init(); //open database
    await db.delete("cotizacion");
    await db.delete("detallecotizacion");
    await db.delete("cronograma");
    return 1;
  }

  Future<int> deleteAfectaciones() async {
    final db = await init(); //open database
    int result = await db.delete("afectaciones");
    return result;
  }

  Future<int> deleteCanal() async {
    final db = await init(); //open database
    int result = await db.delete("canal");
    return result;
  }

  Future<int> deleteCategoria() async {
    final db = await init(); //open database
    int result = await db.delete("categoria");
    return result;
  }

  Future<int> deleteCentroPoblado() async {
    final db = await init(); //open database
    int result = await db.delete("centropoblado");
    return result;
  }

  Future<int> deleteCliente() async {
    final db = await init(); //open database
    int result = await db.delete("cliente");
    return result;
  }

  Future<int> deleteClienteDni(String dni) async {
    final db = await init(); //open database
    int result =
        await db.rawDelete("DELETE FROM cliente WHERE dni = ?", ['$dni']);
    return result;
  }

  Future<double> getComboMontoMin() async {
    final db = await init(); //open database
    var result = await db.rawQuery(
        "SELECT codigo FROM categoria WHERE categoria = 'COTIZACION VENCIMIENTO' AND descripcion = 'COMBOMAX'");
    return double.parse(result[0]['codigo']);
  }

  Future<int> deleteEstado() async {
    final db = await init(); //open database
    int result = await db.delete("estado");
    return result;
  }

  Future<int> deleteFactores() async {
    final db = await init(); //open database
    int result = await db.delete("factores");
    return result;
  }

  Future<int> deleteInicialesMoto() async {
    final db = await init(); //open database
    int result = await db.delete("inicialesmoto");
    return result;
  }

  Future<int> deleteInicialesRecurrente() async {
    final db = await init(); //open database
    int result = await db.delete("inicialesrecurrente");
    return result;
  }

  Future<int> deleteNivelRiesgo() async {
    final db = await init(); //open database
    int result = await db.delete("nivelriesgo");
    return result;
  }

  Future<int> deleteParametrosCotizacion() async {
    final db = await init(); //open database
    int result = await db.delete("parametroscotizacion");
    return result;
  }

  Future<int> deleteProducto() async {
    final db = await init(); //open database
    int result = await db.delete("producto");
    return result;
  }

  Future<int> deleteRecurrencia() async {
    final db = await init(); //open database
    int result = await db.delete("recurrencia");
    return result;
  }

  Future<int> deleteVigencia() async {
    final db = await init(); //open database
    int result = await db.delete("vigencia");
    return result;
  }

  Future<int> deleteTotal() async {
    final db = await init(); //open database
    int result = await db.delete("totalcotizacion");
    return result;
  }

  Future<List<Categoria>> fetchCategoria() async {
    //returns the memos as a list (array)

    final db = await init();
    final maps = await db
        .query("categoria"); //query all the rows in a table as an array of maps

    return List.generate(maps.length, (i) {
      //create a list of memos
      return Categoria(
        id: maps[i]['id'],
        codigo: maps[i]['codigo'],
        descripcion: maps[i]['descripcion'],
        categoria: maps[i]['categoria'],
        superior: maps[i]['superior'],
      );
    });
  }

  Future<List<Categoria>> fetchCategoriaSuperior(int codigo) async {
    //returns the memos as a list (array)

    final db = await init();
    final maps = await db.rawQuery(
        "SELECT * FROM categoria WHERE superior = $codigo"); //query all the rows in a table as an array of maps

    return List.generate(maps.length, (i) {
      //create a list of memos
      return Categoria(
        id: maps[i]['id'],
        codigo: maps[i]['codigo'],
        descripcion: maps[i]['descripcion'],
        categoria: maps[i]['categoria'],
        superior: maps[i]['superior'],
      );
    });
  }

  Future<List<CentroPoblado>> fetchCentroPobladoDistrito(int codigo) async {
    //returns the memos as a list (array)

    final db = await init();
    var maps = await db.rawQuery(
        "SELECT * FROM producto"); //query all the rows in a table as an array of maps
    maps = await db.rawQuery(
        "SELECT * FROM centropoblado WHERE iddistrito = $codigo"); //query all the rows in a table as an array of maps

    return List.generate(maps.length, (i) {
      //create a list of memos
      return CentroPoblado(
        id: maps[i]['id'],
        descripcion: maps[i]['descripcion'],
        idsegmentacion: maps[i]['idsegmentacion'],
        iddistrito: maps[i]['iddistrito'],
      );
    });
  }

  Future<List<Afectaciones>> fetchAfectaciones() async {
    //returns the memos as a list (array)

    final db = await init();
    final maps = await db.query(
        "afectaciones"); //query all the rows in a table as an array of maps

    return List.generate(maps.length, (i) {
      //create a list of memos
      return Afectaciones(
        id: maps[i]['id'],
        descripcion: maps[i]['descripcion'],
        porcentaje: maps[i]['porcentaje'],
        idtipoafectacion: maps[i]['idtipoafectacion'],
        tipoafectacion: maps[i]['tipoafectacion'],
        vigencia: maps[i]['vigencia'],
      );
    });
  }

  Future<List<Canal>> fetchCanal() async {
    //returns the memos as a list (array)

    final db = await init();
    final maps = await db
        .query("canal"); //query all the rows in a table as an array of maps

    return List.generate(maps.length, (i) {
      //create a list of memos
      return Canal(
        id: maps[i]['id'],
        descripcion: maps[i]['descripcion'],
        tipo: maps[i]['tipo'],
      );
    });
  }

  Future<List<CentroPoblado>> fetchCentroPoblado() async {
    //returns the memos as a list (array)

    final db = await init();
    final maps = await db.query(
        "centropoblado"); //query all the rows in a table as an array of maps

    return List.generate(maps.length, (i) {
      //create a list of memos
      return CentroPoblado(
        id: maps[i]['id'],
        descripcion: maps[i]['descripcion'],
        idsegmentacion: maps[i]['idsegmentacion'],
        iddistrito: maps[i]['iddistrito'],
      );
    });
  }

  Future<List<Cliente>> fetchCliente() async {
    //returns the memos as a list (array)

    final db = await init();
    final maps = await db
        .query("cliente"); //query all the rows in a table as an array of maps

    return List.generate(maps.length, (i) {
      //create a list of memos
      return Cliente(
        id: maps[i]['id'],
        referenciaorigen: maps[i]['referenciaorigen'],
        dni: maps[i]['dni'],
        ruc: maps[i]['ruc'],
        apellidopaterno: maps[i]['apellidopaterno'],
        apellidomaterno: maps[i]['apellidomaterno'],
        nombres: maps[i]['nombres'],
        fechanacimiento: maps[i]['fechanacimiento'],
        direccion: maps[i]['direccion'],
        telefono: maps[i]['telefono'],
        idevaleo: maps[i]['idevaleo'],
        idevasen: maps[i]['idevasen'],
        asignacioncme: maps[i]['asignacioncme'],
        cmeasignada: maps[i]['cmeasignada'],
        cmedisponible: maps[i]['cmedisponible'],
        lineacredito: maps[i]['lineacredito'],
        lineadisponible: maps[i]['lineadisponible'],
        idresultado: maps[i]['idresultado'],
        resultado: maps[i]['resultado'],
        excepcion: maps[i]['excepcion'],
        motivoexcepcion: maps[i]['motivoexcepcion'],
        idvigencia: maps[i]['idvigencia'],
        vigencia: maps[i]['vigencia'],
        idrecurrencia: maps[i]['idrecurrencia'],
        recurrencia: maps[i]['recurrencia'],
        idestado: maps[i]['idestado'],
        estado: maps[i]['estado'],
        idbancarizacion: maps[i]['idbancarizacion'],
        bancarizacion: maps[i]['bancarizacion'],
        idnivelriesgo: maps[i]['idnivelriesgo'],
        nivelriesgo: maps[i]['nivelriesgo'],
        fecharegistro: maps[0]['fecharegistro'],
        registrado: maps[i]['registrado'],
      );
    });
  }

  Future<List<Cliente>> fetchClienteNuevos() async {
    //returns the memos as a list (array)

    final db = await init();
    final maps = await db.rawQuery(
        "SELECT * FROM cliente WHERE registrado = 0"); //query all the rows in a table as an array of maps

    return List.generate(maps.length, (i) {
      //create a list of memos
      return Cliente(
        id: maps[i]['id'],
        referenciaorigen: maps[i]['referenciaorigen'],
        dni: maps[i]['dni'],
        ruc: maps[i]['ruc'],
        apellidopaterno: maps[i]['apellidopaterno'],
        apellidomaterno: maps[i]['apellidomaterno'],
        nombres: maps[i]['nombres'],
        fechanacimiento: maps[i]['fechanacimiento'],
        direccion: maps[i]['direccion'],
        telefono: maps[i]['telefono'],
        idevaleo: maps[i]['idevaleo'],
        idevasen: maps[i]['idevasen'],
        asignacioncme: maps[i]['asignacioncme'],
        cmeasignada: maps[i]['cmeasignada'],
        cmedisponible: maps[i]['cmedisponible'],
        lineacredito: maps[i]['lineacredito'],
        lineadisponible: maps[i]['lineadisponible'],
        idresultado: maps[i]['idresultado'],
        resultado: maps[i]['resultado'],
        excepcion: maps[i]['excepcion'],
        motivoexcepcion: maps[i]['motivoexcepcion'],
        idvigencia: maps[i]['idvigencia'],
        vigencia: maps[i]['vigencia'],
        idrecurrencia: maps[i]['idrecurrencia'],
        recurrencia: maps[i]['recurrencia'],
        idestado: maps[i]['idestado'],
        estado: maps[i]['estado'],
        idbancarizacion: maps[i]['idbancarizacion'],
        bancarizacion: maps[i]['bancarizacion'],
        idnivelriesgo: maps[i]['idnivelriesgo'],
        nivelriesgo: maps[i]['nivelriesgo'],
        fecharegistro: maps[0]['fecharegistro'],
        registrado: maps[i]['registrado'],
      );
    });
  }

  Future<List<Estado>> fetchEstado() async {
    //returns the memos as a list (array)

    final db = await init();
    final maps = await db
        .query("estado"); //query all the rows in a table as an array of maps

    return List.generate(maps.length, (i) {
      //create a list of memos
      return Estado(
        id: maps[i]['id'],
        descripcion: maps[i]['descripcion'],
      );
    });
  }

  Future<List<Recurrencia>> fetchRecurrencia() async {
    //returns the memos as a list (array)

    final db = await init();
    final maps = await db.query(
        "recurrencia"); //query all the rows in a table as an array of maps

    return List.generate(maps.length, (i) {
      //create a list of memos
      return Recurrencia(
        id: maps[i]['id'],
        descripcion: maps[i]['descripcion'],
      );
    });
  }

  Future<List<Vigencia>> fetchVigencia() async {
    //returns the memos as a list (array)

    final db = await init();
    final maps = await db
        .query("vigencia"); //query all the rows in a table as an array of maps

    return List.generate(maps.length, (i) {
      //create a list of memos
      return Vigencia(
        id: maps[i]['id'],
        descripcion: maps[i]['descripcion'],
      );
    });
  }

  Future<List<NivelRiesgo>> fetchNivelRiesgo() async {
    //returns the memos as a list (array)

    final db = await init();
    final maps = await db.query(
        "nivelriesgo"); //query all the rows in a table as an array of maps

    return List.generate(maps.length, (i) {
      //create a list of memos
      return NivelRiesgo(
        id: maps[i]['id'],
        descripcion: maps[i]['descripcion'],
      );
    });
  }

  Future<List<Factores>> fetchFactores() async {
    //returns the memos as a list (array)

    final db = await init();
    final maps = await db
        .query("factores"); //query all the rows in a table as an array of maps

    return List.generate(maps.length, (i) {
      //create a list of memos
      return Factores(
        numero: maps[i]['numero'],
        factorcuota: maps[i]['factorcuota'],
        idfactorcuota: maps[i]['idfactorcuota'],
        factortem: maps[i]['factortem'],
        idfactortem: maps[i]['idfactortem'],
      );
    });
  }

  Future<List<InicialesMoto>> fetchInicialesMoto() async {
    //returns the memos as a list (array)

    final db = await init();
    final maps = await db.query(
        "inicialesmoto"); //query all the rows in a table as an array of maps

    return List.generate(maps.length, (i) {
      //create a list of memos
      return InicialesMoto(
        id: maps[i]['id'],
        montoinicial: maps[i]['montoinicial'],
        idrecurrencia: maps[i]['idrecurrencia'],
        tipo: maps[i]['tipo'],
        vigencia: maps[i]['vigencia'],
      );
    });
  }

  Future<List<InicialesRecurrente>> fetchInicialesRecurrente() async {
    //returns the memos as a list (array)

    final db = await init();
    final maps = await db.query(
        "inicialesrecurrente"); //query all the rows in a table as an array of maps

    return List.generate(maps.length, (i) {
      //create a list of memos
      return InicialesRecurrente(
        id: maps[i]['id'],
        idvigencia: maps[i]['idvigencia'],
        idrecurrencia: maps[i]['idrecurrencia'],
        inicial: maps[i]['inicial'],
        vigencia: maps[i]['vigencia'],
      );
    });
  }

  Future<List<TotalCotizacion>> fetchTotalCotizacion() async {
    //returns the memos as a list (array)

    final db = await init();
    final maps = await db.query(
        "totalcotizacion"); //query all the rows in a table as an array of maps

    return List.generate(maps.length, (i) {
      //create a list of memos
      return TotalCotizacion(
        fecha: maps[i]['fecha'],
        cantidad: maps[i]['cantidad'],
        codigolocal: maps[i]['codigolocal'],
      );
    });
  }

  Future<List<ParametrosCotizacion>> fetchParametrosCotizacion() async {
    //returns the memos as a list (array)

    final db = await init();
    final maps = await db.query(
        "parametroscotizacion"); //query all the rows in a table as an array of maps

    return List.generate(maps.length, (i) {
      //create a list of memos
      return ParametrosCotizacion(
        id: maps[i]['id'],
        nivelriesgo: maps[i]['nivelriesgo'],
        tipovivienda: maps[i]['tipovivienda'],
        edadminima: maps[i]['edadminima'],
        edadmaxima: maps[i]['edadmaxima'],
        situacionlaboral: maps[i]['situacionlaboral'],
        estabilidadlaboral: maps[i]['estabilidadlaboral'],
        porcentajeinicial:
            double.parse(maps[i]['porcentajeinicial'].toString()),
        plazoinicial: maps[i]['plazoinicial'],
        observacion: maps[i]['observacion'],
        tipocotizacion: maps[i]['tipocotizacion'],
        giroactividad: maps[i]['giroactividad'],
      );
    });
  }

  Future<List<Producto>> fetchProducto() async {
    //returns the memos as a list (array)

    final db = await init();
    final maps = await db
        .query("producto"); //query all the rows in a table as an array of maps

    return List.generate(maps.length, (i) {
      //create a list of memos
      return Producto(
          id: maps[i]['id'],
          segmentaciongeografica: maps[i]['segmentaciongeografica'],
          cantidad: maps[i]['cantidad'],
          codigomodelo: maps[i]['codigomodelo'],
          nombremodelo: maps[i]['nombremodelo'],
          codigomarca: maps[i]['codigomarca'],
          nombremarca: maps[i]['nombremarca'],
          codigolinea: maps[i]['codigolinea'],
          nombrelinea: maps[i]['nombrelinea'],
          precio: maps[i]['precio'],
          regalo: maps[i]['regalo'],
          tipocotizacion: maps[i]['tipocotizacion'],
          lineadescuento: maps[i]['lineadescuento'],
          dsctonivel: maps[i]['dsctonivel'],
          dsctosegmento: maps[i]['dsctosegmento'],
          dsctolinea: maps[i]['dsctolinea'],
          dsctoplazo: maps[i]['dsctoplazo'],
          dsctoexcepcion: maps[i]['dsctoexcepcion'],
          dsctocombo: maps[i]['dsctocombo'],
          dsctototal: maps[i]['dsctototal'],
          preciototal: maps[i]['preciototal'],
          preciodscto: maps[i]['preciodscto'],
          precioreal: maps[i]['precioreal'],
          vigencia: maps[i]['vigencia'],
          idproductoregalo: maps[i]['idproductoregalo'],
          regalocant: maps[i]['regalocant'],
          detalledscto: maps[i]['detalledscto']);
    });
  }

  Future<List<Afectaciones>> fetchCuotas(bool excepcion, bool combo) async {
    //returns the memos as a list (array)

    final db = await init();
    List<Afectaciones> afct = await fetchAfectaciones();
    var maps;
    if (!excepcion && !combo) {
      maps = await db.rawQuery(
          "SELECT * FROM afectaciones WHERE tipoafectacion = 'PLAZO' AND vigencia = 1"); //query all the rows in a table as an array of maps
    }
    if (!excepcion && combo) {
      maps = await db.rawQuery(
          "SELECT * FROM afectaciones WHERE tipoafectacion = 'PLAZO' AND vigencia = 1 AND descripcion NOT IN('1','2','3','4','5')"); //query all the rows in a table as an array of maps
    }
    if (excepcion && !combo) {
      maps = await db.rawQuery(
          "SELECT * FROM afectaciones WHERE tipoafectacion = 'PLAZO' AND descripcion NOT IN('11','12')"); //query all the rows in a table as an array of maps
    }
    if (excepcion && combo) {
      maps = await db.rawQuery(
          "SELECT * FROM afectaciones WHERE tipoafectacion = 'PLAZO' AND descripcion NOT IN('1','2','3','4','5','11','12')"); //query all the rows in a table as an array of maps
    }

    return List.generate(maps.length, (i) {
      //create a list of memos
      return Afectaciones(
        id: maps[i]['id'],
        descripcion: maps[i]['descripcion'],
        porcentaje: maps[i]['porcentaje'],
        idtipoafectacion: maps[i]['idtipoafectacion'],
        tipoafectacion: maps[i]['tipoafectacion'],
        vigencia: maps[i]['vigencia'],
      );
    });
  }

  Future<List<Producto>> fetchProductoFilter(String tipocotizacion) async {
    //returns the memos as a list (array)
    final db = await init();
    final maps = await db.rawQuery(
        "SELECT * FROM producto WHERE tipocotizacion = ?", [
      '$tipocotizacion'
    ]); //query all the rows in a table as an array of maps

    return List.generate(maps.length, (i) {
      //create a list of memos
      return Producto(
          id: maps[i]['id'],
          segmentaciongeografica: maps[i]['segmentaciongeografica'],
          cantidad: maps[i]['cantidad'],
          codigomodelo: maps[i]['codigomodelo'],
          nombremodelo: maps[i]['nombremodelo'],
          codigomarca: maps[i]['codigomarca'],
          nombremarca: maps[i]['nombremarca'],
          codigolinea: maps[i]['codigolinea'],
          nombrelinea: maps[i]['nombrelinea'],
          precio: maps[i]['precio'],
          regalo: maps[i]['regalo'],
          tipocotizacion: maps[i]['tipocotizacion'],
          lineadescuento: maps[i]['lineadescuento'],
          dsctonivel: maps[i]['dsctonivel'],
          dsctosegmento: maps[i]['dsctosegmento'],
          dsctolinea: maps[i]['dsctolinea'],
          dsctoplazo: maps[i]['dsctoplazo'],
          dsctoexcepcion: maps[i]['dsctoexcepcion'],
          dsctocombo: maps[i]['dsctocombo'],
          dsctototal: maps[i]['dsctototal'],
          preciototal: maps[i]['preciototal'],
          preciodscto: maps[i]['preciodscto'],
          precioreal: maps[i]['precioreal'],
          vigencia: maps[i]['vigencia'],
          idproductoregalo: maps[i]['idproductoregalo'],
          regalocant: maps[i]['regalocant'],
          detalledscto: maps[i]['detalledscto']);
    });
  }

  Future<bool> loginsinconexion(String susuario, String sclave) async {
    //returns the memos as a list (array)

    final db = await init();
    final maps = await db
        .query("usuario"); //query all the rows in a table as an array of maps
    bool ban = false;
    if (maps[0]['usuario'] == susuario && maps[0]['clave'] == sclave) {
      ban = true;
    }
    return ban;
  }

  Future<Usuario> fetchUsuario() async {
    //returns the memos as a list (array)

    final db = await init();
    final us = await db
        .query("usuario"); //query all the rows in a table as an array of maps

    return Usuario(
      id: us[0]['id'],
      usuario: us[0]['usuario'],
      clave: us[0]['clave'],
      nombre: us[0]['nombre'],
      cargo: us[0]['cargo'],
      token: us[0]['token'],
      sincronizar: us[0]['sincronizar'].toString(),
    );
  }

  Future<List<Local>> fetchLocal(String estado) async {
    //returns the memos as a list (array)

    final db = await init();
    final maps = await db.rawQuery(
        "SELECT * FROM local WHERE estado = $estado"); //query all the rows in a table as an array of maps

    return List.generate(maps.length, (i) {
      //create a list of memos
      return Local(
        codigo: maps[i]['codigo'],
        descripcion: maps[i]['descripcion'],
        tipo: maps[i]['tipo'],
        estado: maps[i]['estado'].toString(),
      );
    });
  }

  Future<List<ListaCotizacion>> fetchListCotizacion() async {
    final db = await init();
    var maps = await db.rawQuery(
        "SELECT CT.id, CL.dni, CL.nombres, CL.apellidopaterno, CL.apellidomaterno, CN.descripcion as canal, CT.montocredito, CT.pagoinicial, CT.totalcredito, CT.enviada, CT.fecharegistro FROM cotizacion CT "
        "INNER JOIN cliente CL ON CL.dni = CT.dni INNER JOIN canal CN ON CN.id = CT.medio ");

    return List.generate(maps.length, (i) {
      //create a list of memos
      return ListaCotizacion(
        id: maps[i]['id'],
        dni: maps[i]['dni'],
        cliente:
            "${maps[i]['nombres']} ${maps[i]['apellidopaterno']} ${maps[i]['apellidomaterno']}",
        canal: maps[i]['canal'],
        fecharegistro: maps[i]['fecharegistro'],
        montocuotas: maps[i]['montocredito'],
        montoinicial: maps[i]['pagoinicial'],
        montototal: maps[i]['totalcredito'],
        enviada: maps[i]['enviada'],
      );
    });
  }

  Future<List<ListaCotizacion>> filterListCotizacion(
      String filterCriterio) async {
    final db = await init();
    var maps = await db.rawQuery(
        "SELECT CT.id, CL.dni, CL.nombres, CL.apellidopaterno, CL.apellidomaterno, CN.descripcion as canal, CT.montocredito, CT.pagoinicial, CT.totalcredito, CT.enviada, CT.fecharegistro FROM cotizacion CT "
        "INNER JOIN cliente CL ON CL.dni = CT.dni INNER JOIN canal CN ON CN.id = CT.medio WHERE CL.nombres LIKE ? OR CL.apellidopaterno LIKE ? OR CL.apellidomaterno LIKE ? OR CL.dni LIKE ?",
        [
          '%$filterCriterio%',
          '%$filterCriterio%',
          '%$filterCriterio%',
          '%$filterCriterio%'
        ]);

    return List.generate(maps.length, (i) {
      //create a list of memos
      return ListaCotizacion(
        id: maps[i]['id'],
        dni: maps[i]['dni'],
        cliente:
            "${maps[i]['nombres']} ${maps[i]['apellidopaterno']} ${maps[i]['apellidomaterno']}",
        canal: maps[i]['canal'],
        fecharegistro: maps[i]['fecharegistro'],
        montocuotas: maps[i]['montocredito'],
        montoinicial: maps[i]['pagoinicial'],
        montototal: maps[i]['totalcredito'],
        enviada: maps[i]['enviada'],
      );
    });
  }

  Future<Cotizacion> getCotizacion(int id) async {
    //returns the memos as a list (array)

    final db = await init();
    var maps = await db.rawQuery("SELECT * FROM cotizacion WHERE id = ?",
        [id]); //query all the rows in a table as an array of maps

    //create a list of memos
    return Cotizacion(
        id: maps[0]['id'],
        dni: maps[0]['dni'],
        enviada: maps[0]['enviada'],
        combo: maps[0]['combo'],
        edad: maps[0]['edad'],
        idvivienda: maps[0]['idvivienda'],
        idsituacionlaboral: maps[0]['idsituacionlaboral'],
        idestabilidadlaboral: maps[0]['idestabilidadlaboral'],
        idgiroactividad: maps[0]['idgiroactividad'],
        idzona: maps[0]['idzona'],
        cuotas: maps[0]['cuotas'],
        excepcion: maps[0]['excepcion'],
        porcentajeinicial: maps[0]['porcentajeinicial'],
        plazoinicial: maps[0]['plazoinicial'],
        pagoinicial: maps[0]['pagoinicial'],
        preciocompra: maps[0]['preciocompra'],
        descuentopromo: maps[0]['descuentopromo'],
        precioreal: maps[0]['precioreal'],
        montocuota: maps[0]['montocuota'],
        montocredito: maps[0]['montocredito'],
        totalcredito: maps[0]['totalcredito'],
        observacion: maps[0]['observacion'],
        negociacion: maps[0]['negociacion'],
        idresultado: maps[0]['idresultado'],
        idcontacto: maps[0]['idcontacto'],
        idpersonalregistro: maps[0]['idpersonalregistro'],
        asesorvta: maps[0]['asesorvta'],
        proceso: maps[0]['proceso'],
        direccion: maps[0]['direccion'],
        telefono: maps[0]['telefono'],
        codigoubigeo: maps[0]['codigoubigeo'],
        factor: maps[0]['factor'],
        factortem: maps[0]['factortem'],
        tea: maps[0]['tea'],
        tem: maps[0]['tem'],
        tceaimp: maps[0]['tceaimp'],
        tcemimp: maps[0]['tcemimp'],
        tcea: maps[0]['tcea'],
        tcem: maps[0]['tcem'],
        igv: maps[0]['igv'],
        inicialorig: maps[0]['inicialorig'],
        medio: maps[0]['medio'],
        fecharegistro: maps[0]['fecharegistro']);
  }

  Future<List<Cotizacion>> fetchCotizacion(bool tipo) async {
    //returns the memos as a list (array)

    final db = await init();
    var maps = await db.rawQuery(
        "SELECT * FROM cotizacion"); //query all the rows in a table as an array of maps
    if (tipo) {
      maps = await db.rawQuery("SELECT * FROM cotizacion WHERE enviada = 0");
    }
    return List.generate(maps.length, (i) {
      //create a list of memos
      return Cotizacion(
          id: maps[i]['id'],
          dni: maps[i]['dni'],
          enviada: maps[i]['enviada'],
          combo: maps[i]['combo'],
          edad: maps[i]['edad'],
          idvivienda: maps[i]['idvivienda'],
          idsituacionlaboral: maps[i]['idsituacionlaboral'],
          idestabilidadlaboral: maps[i]['idestabilidadlaboral'],
          idgiroactividad: maps[i]['idgiroactividad'],
          idzona: maps[i]['idzona'],
          cuotas: maps[i]['cuotas'],
          excepcion: maps[i]['excepcion'],
          porcentajeinicial: maps[i]['porcentajeinicial'],
          plazoinicial: maps[i]['plazoinicial'],
          pagoinicial: maps[i]['pagoinicial'],
          preciocompra: maps[i]['preciocompra'],
          descuentopromo: maps[i]['descuentopromo'],
          precioreal: maps[i]['precioreal'],
          montocuota: maps[i]['montocuota'],
          montocredito: maps[i]['montocredito'],
          totalcredito: maps[i]['totalcredito'],
          observacion: maps[i]['observacion'],
          negociacion: maps[i]['negociacion'],
          idresultado: maps[i]['idresultado'],
          idcontacto: maps[i]['idcontacto'],
          idpersonalregistro: maps[i]['idpersonalregistro'],
          asesorvta: maps[i]['asesorvta'],
          proceso: maps[i]['proceso'],
          direccion: maps[i]['direccion'],
          telefono: maps[i]['telefono'],
          codigoubigeo: maps[i]['codigoubigeo'],
          factor: maps[i]['factor'],
          factortem: maps[i]['factortem'],
          tea: maps[i]['tea'],
          tem: maps[i]['tem'],
          tceaimp: maps[i]['tceaimp'],
          tcemimp: maps[i]['tcemimp'],
          tcea: maps[i]['tcea'],
          tcem: maps[i]['tcem'],
          igv: maps[i]['igv'],
          inicialorig: maps[i]['inicialorig'],
          medio: maps[i]['medio'],
          fecharegistro: maps[i]['fecharegistro']);
    });
  }

  Future<List<DetalleCotizacion>> fetchDetalleCotizacion(int id) async {
    //returns the memos as a list (array)

    final db = await init();
    var maps = await db.rawQuery(
        "SELECT * FROM detallecotizacion WHERE idcotizacion = ?",
        [id]); //query all the rows in a table as an array of maps

    return List.generate(maps.length, (i) {
      //create a list of memos
      return DetalleCotizacion(
          id: maps[i]['id'],
          cantidad: maps[i]['cantidad'],
          idproducto: maps[i]['idproducto'],
          descripcion: maps[i]['descripcion'],
          preciounitario: maps[i]['preciounitario'],
          preciototal: maps[i]['preciototal'],
          dsctoperfilcrediticio: maps[i]['dsctoperfilcrediticio'],
          dsctosegmentacion: maps[i]['dsctosegmentacion'],
          dsctolineaproducto: maps[i]['dsctolineaproducto'],
          dsctoplazo: maps[i]['dsctoplazo'],
          dsctocombo: maps[i]['dsctocombo'],
          dsctoexcepcion: maps[i]['dsctoexcepcion'],
          descuentototal: maps[i]['descuentototal'],
          precioreal: maps[i]['precioreal'],
          idcotizacion: maps[i]['idcotizacion']);
    });
  }

  Future<List<Cronograma>> fetchCronograma(int id) async {
    //returns the memos as a list (array)

    final db = await init();
    var maps = await db.rawQuery(
        "SELECT * FROM cronograma WHERE idcotizacion = ?",
        [id]); //query all the rows in a table as an array of maps

    return List.generate(maps.length, (i) {
      //create a list of memos
      return Cronograma(
          id: maps[i]['id'],
          nrocuota: maps[i]['nrocuota'],
          amortizacion: maps[i]['amortizacion'],
          interes: maps[i]['interes'],
          gasto: maps[i]['gasto'],
          monto: maps[i]['monto'],
          igv: maps[i]['igv'],
          saldo: maps[i]['saldo'],
          facturar: maps[i]['facturar'],
          idcotizacion: maps[i]['idcotizacion']);
    });
  }

  Future<Cliente> getCliente(String dni) async {
    final db = await init();
    List<Map> results = await db.query("Cliente",
        columns: [
          'id',
          'referenciaorigen',
          'dni',
          'ruc',
          'apellidopaterno',
          'apellidomaterno',
          'nombres',
          'fechanacimiento',
          'direccion',
          'telefono',
          'idevaleo',
          'idevasen',
          'asignacioncme',
          'cmeasignada',
          'cmedisponible',
          'lineacredito',
          'lineadisponible',
          'idresultado',
          'resultado',
          'excepcion',
          'motivoexcepcion',
          'idvigencia',
          'vigencia',
          'idrecurrencia',
          'recurrencia',
          'idestado',
          'estado',
          'idbancarizacion',
          'bancarizacion',
          'idnivelriesgo',
          'nivelriesgo',
          'fecharegistro',
          'registrado',
        ],
        where: 'dni = ?',
        whereArgs: [dni]);

    if (results.length > 0) {
      return new Cliente(
          id: results[0]['id'],
          referenciaorigen: results[0]['referenciaorigen'],
          dni: results[0]['dni'],
          ruc: results[0]['ruc'],
          apellidopaterno: results[0]['apellidopaterno'],
          apellidomaterno: results[0]['apellidomaterno'],
          nombres: results[0]['nombres'],
          fechanacimiento: results[0]['fechanacimiento'],
          direccion: results[0]['direccion'],
          telefono: results[0]['telefono'],
          idevaleo: results[0]['idevaleo'],
          idevasen: results[0]['idevasen'],
          asignacioncme: results[0]['asignacioncme'],
          cmeasignada: results[0]['cmeasignada'],
          cmedisponible: results[0]['cmedisponible'],
          lineacredito: results[0]['lineacredito'],
          lineadisponible: results[0]['lineadisponible'],
          idresultado: results[0]['idresultado'],
          resultado: results[0]['resultado'],
          excepcion: results[0]['excepcion'],
          motivoexcepcion: results[0]['motivoexcepcion'],
          idvigencia: results[0]['idvigencia'],
          vigencia: results[0]['vigencia'],
          idrecurrencia: results[0]['idrecurrencia'],
          recurrencia: results[0]['recurrencia'],
          idestado: results[0]['idestado'],
          estado: results[0]['estado'],
          idbancarizacion: results[0]['idbancarizacion'],
          bancarizacion: results[0]['bancarizacion'],
          idnivelriesgo: results[0]['idnivelriesgo'],
          nivelriesgo: results[0]['nivelriesgo'],
          fecharegistro: results[0]['fecharegistro'],
          registrado: results[0]['registrado']);
    }

    return null;
  }

  Future<List<Cliente>> filterClientes(String filterCriterio) async {
    final db = await init();

    var maps = await db.rawQuery(
        "SELECT * FROM cliente WHERE dni LIKE ?", ['%$filterCriterio%']);
    if (maps.length != null) {
      return List.generate(maps.length, (i) {
        //create a list of memos
        return Cliente(
          id: maps[i]['id'],
          referenciaorigen: maps[i]['referenciaorigen'],
          dni: maps[i]['dni'],
          ruc: maps[i]['ruc'],
          apellidopaterno: maps[i]['apellidopaterno'],
          apellidomaterno: maps[i]['apellidomaterno'],
          nombres: maps[i]['nombres'],
          fechanacimiento: maps[i]['fechanacimiento'],
          direccion: maps[i]['direccion'],
          telefono: maps[i]['telefono'],
          idevaleo: maps[i]['idevaleo'],
          idevasen: maps[i]['idevasen'],
          asignacioncme: maps[i]['asignacioncme'],
          cmeasignada: maps[i]['cmeasignada'],
          cmedisponible: maps[i]['cmedisponible'],
          lineacredito: maps[i]['lineacredito'],
          lineadisponible: maps[i]['lineadisponible'],
          idresultado: maps[i]['idresultado'],
          resultado: maps[i]['resultado'],
          excepcion: maps[i]['excepcion'],
          motivoexcepcion: maps[i]['motivoexcepcion'],
          idvigencia: maps[i]['idvigencia'],
          vigencia: maps[i]['vigencia'],
          idrecurrencia: maps[i]['idrecurrencia'],
          recurrencia: maps[i]['recurrencia'],
          idestado: maps[i]['idestado'],
          estado: maps[i]['estado'],
          idbancarizacion: maps[i]['idbancarizacion'],
          bancarizacion: maps[i]['bancarizacion'],
          idnivelriesgo: maps[i]['idnivelriesgo'],
          nivelriesgo: maps[i]['nivelriesgo'],
          fecharegistro: maps[0]['fecharegistro'],
          registrado: maps[i]['registrado'],
        );
      });
    }
    return new List<Cliente>();
  }

  Future<List<Producto>> filterProductos(
      String filterCriterio, String tipocotizacion) async {
    final db = await init();

    var maps = await db.rawQuery(
        "SELECT * FROM producto WHERE tipocotizacion = ? AND nombremodelo LIKE ?",
        ['$tipocotizacion', '%$filterCriterio%']);
    if (maps.length != null) {
      return List.generate(maps.length, (i) {
        //create a list of memos
        return Producto(
            id: maps[i]['id'],
            segmentaciongeografica: maps[i]['segmentaciongeografica'],
            cantidad: maps[i]['cantidad'],
            codigomodelo: maps[i]['codigomodelo'],
            nombremodelo: maps[i]['nombremodelo'],
            codigomarca: maps[i]['codigomarca'],
            nombremarca: maps[i]['nombremarca'],
            codigolinea: maps[i]['codigolinea'],
            nombrelinea: maps[i]['nombrelinea'],
            precio: maps[i]['precio'],
            regalo: maps[i]['regalo'],
            tipocotizacion: maps[i]['tipocotizacion'],
            lineadescuento: maps[i]['lineadescuento'],
            dsctonivel: maps[i]['dsctonivel'],
            dsctosegmento: maps[i]['dsctosegmento'],
            dsctolinea: maps[i]['dsctolinea'],
            dsctoplazo: maps[i]['dsctoplazo'],
            dsctoexcepcion: maps[i]['dsctoexcepcion'],
            dsctocombo: maps[i]['dsctocombo'],
            dsctototal: maps[i]['dsctototal'],
            preciototal: maps[i]['preciototal'],
            preciodscto: maps[i]['preciodscto'],
            precioreal: maps[i]['precioreal'],
            vigencia: maps[i]['vigencia'],
            idproductoregalo: maps[i]['idproductoregalo'],
            regalocant: maps[i]['regalocant'],
            detalledscto: maps[i]['detalledscto']);
      });
    }
    return new List<Producto>();
  }

  Future<ResultadoCotizacion> obtenerparametroscotizacion(
      int vtipomoto,
      String vcuotas,
      int vedad,
      String vtipovivienda,
      int vnivelriesgo,
      String vtipocotizacion,
      int vvigencia,
      int vrecurrencia,
      int vcentropoblado) async {
    final db = await init();
    int vsegmentacion = 0;
    double pporcinicial = 0.0;
    int pplazoinicial = 0;
    double pmontoinicial = 0.0;
    double ptea = 0.0;
    double ptem = 0.0;
    double ptceaimp = 0.0;
    double ptcemimp = 0.0;
    double pigv = 0.0;
    double pfactor = 0.0;
    double pfactortem = 0.0;
    double pdsctocombootros = 0.0;
    double pdsctocomboleo = 0.0;
    double pdsctosegmentacion = 0.0;
    double pdsctonivelriesgo = 0.0;

    var mapsPC = await db.rawQuery(
        "SELECT id, porcentajeinicial, plazoinicial FROM parametroscotizacion WHERE nivelriesgo = ${vnivelriesgo} AND tipovivienda = '${vtipovivienda}'"
        " AND tipocotizacion = '${vtipocotizacion}' AND (${vedad} BETWEEN edadminima AND edadmaxima) ");

    if (mapsPC.length > 0) {
      pporcinicial = mapsPC[0]['porcentajeinicial'];
      pplazoinicial = mapsPC[0]['plazoinicial'];
    }
    if (pplazoinicial == -1) {
      mapsPC = await db.rawQuery(
          "SELECT inicial FROM inicialesrecurrente WHERE idvigencia = ? AND idrecurrencia = ?",
          ['$vvigencia', '$vrecurrencia']);
      if (mapsPC.length > 0) {
        pplazoinicial = mapsPC[0]['inicial'];
      }
    }

    if (vtipocotizacion == 'MOTO') {
      mapsPC = await db.rawQuery(
          "SELECT montoinicial FROM inicialesmoto WHERE tipo = ? AND idrecurrencia = ?",
          ['$vtipomoto', '$vrecurrencia']);
      if (mapsPC.length > 0) {
        pmontoinicial = mapsPC[0]['montoinicial'];
      }
    }
    // factor cuotas
    mapsPC = await db.rawQuery(
        "SELECT porcentaje FROM afectaciones WHERE tipoafectacion = ? AND descripcion = ?",
        ['PLAZO', '$vcuotas']);
    if (mapsPC.length > 0) {
      pfactor = mapsPC[0]['porcentaje'];
    }
    // factor tem
    mapsPC = await db.rawQuery(
        "SELECT porcentaje FROM afectaciones WHERE tipoafectacion = ? AND descripcion = ?",
        ['FACTOR TEM', '$vcuotas']);
    if (mapsPC.length > 0) {
      pfactortem = mapsPC[0]['porcentaje'];
    }

    // tea
    mapsPC = await db.rawQuery(
        "SELECT porcentaje FROM afectaciones WHERE tipoafectacion = ? AND descripcion = ?",
        ['IMPUESTOS', 'TEA']);
    if (mapsPC.length > 0) {
      ptea = mapsPC[0]['porcentaje'];
    }

    // tem
    mapsPC = await db.rawQuery(
        "SELECT porcentaje FROM afectaciones WHERE tipoafectacion = ? AND descripcion = ?",
        ['IMPUESTOS', 'TEM']);
    if (mapsPC.length > 0) {
      ptem = mapsPC[0]['porcentaje'];
    }

    // tcea
    mapsPC = await db.rawQuery(
        "SELECT porcentaje FROM afectaciones WHERE tipoafectacion = ? AND descripcion = ?",
        ['IMPUESTOS', 'TCEA IMPUESTOS']);
    if (mapsPC.length > 0) {
      ptceaimp = mapsPC[0]['porcentaje'];
    }

    // tcem
    mapsPC = await db.rawQuery(
        "SELECT porcentaje FROM afectaciones WHERE tipoafectacion = ? AND descripcion = ?",
        ['IMPUESTOS', 'TCEM IMPUESTOS']);
    if (mapsPC.length > 0) {
      ptcemimp = mapsPC[0]['porcentaje'];
    }

    // igv
    mapsPC = await db.rawQuery(
        "SELECT porcentaje FROM afectaciones WHERE tipoafectacion = ? AND descripcion = ?",
        ['IMPUESTOS', 'IGV']);
    if (mapsPC.length > 0) {
      pigv = mapsPC[0]['porcentaje'];
    }

    // combo leo
    mapsPC = await db.rawQuery(
        "SELECT porcentaje FROM afectaciones WHERE tipoafectacion = ? AND descripcion = ?",
        ['COMBO', 'MELAMINA']);
    if (mapsPC.length > 0) {
      pdsctocomboleo = mapsPC[0]['porcentaje'];
    }

    // combo otros
    mapsPC = await db.rawQuery(
        "SELECT porcentaje FROM afectaciones WHERE tipoafectacion = ? AND descripcion = ?",
        ['COMBO', 'DESCUENTO']);
    if (mapsPC.length > 0) {
      pdsctocombootros = mapsPC[0]['porcentaje'];
    }

    // descuento segmentacion
    if (vcentropoblado > 0) {
      mapsPC = await db.rawQuery(
          "SELECT idsegmentacion FROM centropoblado WHERE id = ?",
          ['$vcentropoblado']);
      if (mapsPC.length > 0) {
        vsegmentacion = mapsPC[0]['idsegmentacion'];
      }
      mapsPC = await db.rawQuery(
          "SELECT porcentaje FROM afectaciones WHERE idtipoafectacion = ? AND id = ?",
          [1, '$vsegmentacion']);

      if (mapsPC.length > 0) {
        pdsctosegmentacion = mapsPC[0]['porcentaje'];
      }
    }

    // descuento nivel riesgo
    mapsPC = await db.rawQuery(
        "SELECT porcentaje FROM afectaciones WHERE tipoafectacion = ? AND id = ?",
        ['NIVEL RIESGO', '$vnivelriesgo']);
    if (mapsPC.length > 0) {
      pdsctonivelriesgo = mapsPC[0]['porcentaje'];
    }

    pporcinicial = pporcinicial == null ? 0 : pporcinicial;
    return ResultadoCotizacion(
      porcinicial: pporcinicial,
      plazoinicial: pplazoinicial,
      montoinicial: pmontoinicial,
      tea: ptea,
      tem: ptem,
      tceaimp: ptceaimp,
      tcemimp: ptcemimp,
      igv: pigv,
      factor: pfactor,
      factortem: pfactortem,
      dsctocombootros: pdsctocombootros,
      dsctocomboleo: pdsctocomboleo,
      dsctosegmentacion: pdsctosegmentacion,
      dsctonivelriesgo: pdsctonivelriesgo,
    );
  }
}
