class ResultadoCotizacion{
  final num porcinicial;
  final int plazoinicial;
  final double montoinicial;
  final double tea;
  final double tem;
  final double tceaimp;
  final double tcemimp;
  final double igv;
  final double factor;
  final double factortem;
  final double dsctocombootros;
  final double dsctocomboleo;
  final double dsctosegmentacion;
  final double dsctonivelriesgo;

  ResultadoCotizacion({
    this.porcinicial,
    this.plazoinicial,
    this.montoinicial,
    this.tea,
    this.tem,
    this.tceaimp,
    this.tcemimp,
    this.igv,
    this.factor,
    this.factortem,
    this.dsctocombootros,
    this.dsctocomboleo,
    this.dsctosegmentacion,
    this.dsctonivelriesgo
  });

  Map<String, dynamic> toMap(){
    return <String, dynamic>{"porcinicial" : porcinicial,
    "plazoinicial" : plazoinicial,
    "montoinicial" : montoinicial,
    "tea" : tea,
    "tem" : tem,
    "tceaimp" : tceaimp,
    "tcemimp" : tcemimp,
    "igv" : igv,
    "factor" : factor,
    "factortem" : factortem,
    "dsctocombootros" : dsctocombootros,
    "dsctocomboleo" : dsctocomboleo,
    "dsctosegmentacion" : dsctosegmentacion,
    "dsctonivelriesgo" : dsctonivelriesgo};
  }
}