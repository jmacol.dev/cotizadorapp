class ListaCotizacion{  
  final int id;
  final String dni;
  final String cliente;
  final String canal;
  final String fecharegistro;
  final double montocuotas;
  final double montoinicial;
  final double montototal;
  final int enviada;

  ListaCotizacion({this.id,this.dni,this.cliente, this.canal,
  this.fecharegistro,
  this.montocuotas,
  this.montoinicial, this.montototal, this.enviada});

  Map<String,dynamic> toMap(){ // used when inserting data to the database
    return <String,dynamic>{
      "id" : id,
      "dni" : dni,
      "cliente" : cliente,
      "canal" : canal,
      "fecharegistro" : fecharegistro,
      "montocuotas" : montocuotas,
      "montoinicial" : montoinicial,
      "montototal" : montototal,
      "enviada" : enviada
    };
  }
}





