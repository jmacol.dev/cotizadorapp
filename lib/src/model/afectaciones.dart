class Afectaciones{  
  final int id;
  final String descripcion;
  final double porcentaje;
  final int idtipoafectacion;
  final String tipoafectacion;
  final int vigencia;

  Afectaciones({this.id,this.descripcion,this.porcentaje,
  this.idtipoafectacion,
  this.tipoafectacion,
  this.vigencia});

  Map<String,dynamic> toMap(){ // used when inserting data to the database
    return <String,dynamic>{
      "id" : id,
      "descripcion" : descripcion,
      "porcentaje":porcentaje,
      "idtipoafectacion":idtipoafectacion,
      "tipoafectacion":tipoafectacion,
      "vigencia":vigencia,
    };
  }
}






