import 'dart:convert';

import 'package:appcotizador/src/model/usuario.dart';
import 'package:appcotizador/src/widgets/module.dart';
import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';

class NavDrawer extends StatefulWidget  {
  NavDrawer({Key key}) : super(key: key);
  @override
  NavDrawerPageState createState() => NavDrawerPageState();
  }
class NavDrawerPageState extends State<NavDrawer> {
  final prefs = new Sesion();
  @override
  Widget build(BuildContext context) {
    locales.removeWhere((element) => element.codigo == 0);
    int codlocal = prefs.local;
    Map<String, dynamic> user = jsonDecode(prefs.usuario);
    return Container(
      width: 400,
      child: Drawer(  
      child: ListView(        
        padding: EdgeInsets.zero,
        children: <Widget>[
          UserAccountsDrawerHeader(
            decoration: BoxDecoration(
              color: new Color(0xFF0062ac),                
              image: DecorationImage(
                  fit: BoxFit.fill,                    
                  image: AssetImage('assets/images/cover.jpg')),
            ),
            accountName: Text(user['nombre'], style: TextStyle(fontSize: 18)),     
              accountEmail: Text(user['cargo'], style: TextStyle(fontSize: 15)),        
            currentAccountPicture: CircleAvatar(                
              backgroundColor:
                  Theme.of(context).platform == TargetPlatform.iOS
                      ? new Color(0xFF0062ac)
                      : Colors.white,
              child: Icon(
                FontAwesomeIcons.userAlt,
                size: 40,
              ),
            ),
          ),
          Padding(     
                padding: EdgeInsets.all(0),
                child:           
                TextForm(
                show:true,
                icon: FontAwesomeIcons.building,
                item: DropdownButtonFormField(
                  
                style:  new TextStyle(fontSize: 17, color: Colors.black54),
                items: locales.map((Locales local) {
                  return new DropdownMenuItem(
                    value: local.codigo,
                    child: Row(
                      children: <Widget>[
                        Text(local.descripcion),
                      ],
                    )
                    );
                  }).toList(),
                  onChanged: (newValue) {
                    codlocal = newValue;
                    prefs.local = codlocal;
                    Navigator.pushReplacementNamed(context, 'dashboard');  
                  },
                  value: codlocal,                 
                  ),)
            ),
          ListTile(
            leading: Icon(FontAwesomeIcons.syncAlt),
            title: Text('Sincronizar', style: TextStyle(fontSize: 18)),
            onTap: () async {
              setState((){
                isLoading = true;   
                Navigator.pushReplacementNamed(context, 'dashboard');                   
              });              
            },
          ),
          ListTile(
            leading: Icon(FontAwesomeIcons.userTag),
            title: Text('Evaluar cliente', style: TextStyle(fontSize: 18)),
            onTap: () async  {
                  if(prefs.sesion && prefs.token.toString().trim().isNotEmpty){ 
                    Usuario useraux = await db.fetchUsuario();
                    if(useraux.sincronizar == '1'){
                      prefs.datosevaluar = true;
                      prefs.clienteencontrado = false;
                      Navigator.pushReplacementNamed(context, 'evaluar');
                    }else{
                      prefs.toastmessage("Debe sincronizar la data para poder iniciar su trabajo.", Colors.black87, Colors.yellow[700]);        
                    }
                  }else{
                    Navigator.pushReplacementNamed(context, 'login');
                  }  
              
            },
          ),
          ListTile(
            leading: Icon(FontAwesomeIcons.shareSquare),
            title: Text('Enviar pendientes '+ '(' + prefs.pendientes.toString() + ')', style: TextStyle(fontSize: 18)),
            onTap: () => {prefs.enviarpendientescotizaciones(context)},
          ),
          ListTile(
            leading: Icon(FontAwesomeIcons.clipboardList),
            title: Text('Mis cotizaciones', style: TextStyle(fontSize: 18)),
            onTap: () => {Navigator.pushReplacementNamed(context, 'cotizalist')  },
          ),
          ListTile(
            leading: Icon(FontAwesomeIcons.signOutAlt, color: Colors.red[600],),
            title: Text('Cerrar sesión', style: TextStyle(fontSize: 19, color: Colors.red[600]), ),
            onTap: () => {
             prefs.cerrarsesion(context)
            },
          ),
        ],
      ),
      )
    );
  }
}