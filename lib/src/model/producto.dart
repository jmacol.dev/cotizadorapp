class Producto{  

  int id;
  String segmentaciongeografica;
  double cantidad;
  int codigomodelo;
  String nombremodelo;
  int codigomarca;
  String nombremarca;
  int codigolinea;
  String nombrelinea;
  double precio;
  int regalo;
  String tipocotizacion;
  int lineadescuento;
  double dsctonivel;
  double dsctosegmento;
  double dsctolinea;
  double dsctoplazo;
  double dsctoexcepcion;
  double dsctocombo;
  double dsctototal;
  double preciototal;
  double preciodscto;
  double precioreal;
  int vigencia;
  int idproductoregalo;
  double regalocant;
  String detalledscto;

  Producto({
    this.id,
    this.segmentaciongeografica,
    this.cantidad,
    this.codigomodelo,
    this.nombremodelo,
    this.codigomarca,
    this.nombremarca,
    this.codigolinea,
    this.nombrelinea,
    this.precio,
    this.regalo,
    this.tipocotizacion,
    this.lineadescuento,
    this.dsctonivel,
    this.dsctosegmento,
    this.dsctolinea,
    this.dsctoplazo,
    this.dsctoexcepcion,
    this.dsctocombo,
    this.dsctototal,
    this.preciototal,
    this.preciodscto,
    this.precioreal,
    this.vigencia,
    this.idproductoregalo,
    this.regalocant,
    this.detalledscto
  });

  Map<String,dynamic> toMap(){ // used when inserting data to the database
    return <String,dynamic>{
      "id" : id,
      "segmentaciongeografica" : segmentaciongeografica,
      "cantidad" : cantidad,
      "codigomodelo" : codigomodelo,
      "nombremodelo" : nombremodelo,
      "codigomarca" : codigomarca,
      "nombremarca" : nombremarca,
      "codigolinea" : codigolinea,
      "nombrelinea" : nombrelinea,
      "precio" : precio,
      "regalo" : regalo,
      "tipocotizacion" : tipocotizacion,
      "lineadescuento" : lineadescuento,
      "dsctonivel" : dsctonivel,
      "dsctosegmento" : dsctosegmento,
      "dsctolinea" : dsctolinea,
      "dsctoplazo" : dsctoplazo,
      "dsctoexcepcion" : dsctoexcepcion,
      "dsctocombo" : dsctocombo,
      "dsctototal" : dsctototal,
      "preciototal" : preciototal,
      "preciodscto" : preciodscto,
      "precioreal" : precioreal,
      "vigencia" : vigencia,
      "idproductoregalo" : idproductoregalo,
      "regalocant" : regalocant,
      "detalledscto" : detalledscto
    };
  }
}


