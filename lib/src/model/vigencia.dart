class Vigencia{  
  final int id;
  final String descripcion;

  Vigencia({this.id,this.descripcion});

  Map<String,dynamic> toMap(){ // used when inserting data to the database
    return <String,dynamic>{
      "id" : id,
      "descripcion" : descripcion
    };
  }
}