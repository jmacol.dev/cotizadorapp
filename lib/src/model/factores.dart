class Factores{  
  final int numero;
  final double factorcuota;
  final int idfactorcuota;
  final double factortem;
  final int idfactortem;

  Factores({this.numero,
  this.factorcuota,
  this.idfactorcuota,
  this.factortem,
  this.idfactortem});

  Map<String,dynamic> toMap(){ // used when inserting data to the database
    return <String,dynamic>{
      "numero":numero,
      "factorcuota":factorcuota,
      "idfactorcuota":idfactorcuota,
      "factortem":factortem,
      "idfactortem":idfactortem,
    };
  }
}






