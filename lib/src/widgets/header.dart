import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';

class Header extends StatelessWidget{
  final IconData icon;
  final String titulo;
  final String subtitulo;
  final double hdif;
  const Header({
    @required this.icon,
    @required this.titulo,
    @required this.subtitulo,
    @required this.hdif
  }) ;
  @override
  Widget build(BuildContext context){
    final  Color colorBlanco = Colors.white.withOpacity(0.8);
    return Stack(     
      
      children:<Widget>[
        
          IconHeaderBackground(hdif: this.hdif),
          Positioned(
            top: 20-this.hdif.toDouble(),
            left: -60,
            child: FaIcon(
              this.icon, 
              size:250, 
              color:Colors.white.withOpacity(0.3),
              ),
            ),
            Column(
              children:<Widget>[
                SizedBox(height:100-this.hdif.toDouble(), width: double.infinity),
                Text(this.titulo, style: TextStyle(fontSize: 40, fontWeight: FontWeight.bold, color: colorBlanco)),
                SizedBox(height:10),
                Text(this.subtitulo, style: TextStyle(fontSize: 32, color: colorBlanco)),
                SizedBox(height:10),
                FaIcon(
              this.icon, 
              size:48, 
              color:Colors.white,
              ),
              ],
            ),
      ],
    );
    
  }
}

class IconHeaderBackground extends StatelessWidget {
  final double hdif;
  const IconHeaderBackground({
    this.hdif
  }) ;

  @override
  Widget build(BuildContext context) {
    return Container(
      width:  double.infinity,
      height: 300-this.hdif.toDouble() ,
       decoration: BoxDecoration(
        color: Color(0xff084B8A),
        // borderRadius: BorderRadius.only(
        //   bottomLeft: Radius.circular(100),
        // )
      ),
      
      
    );
  }
}