class InicialesMoto{  
  final int id;
  final double montoinicial;
  final int idrecurrencia;
  final int tipo;
  final int vigencia;

  InicialesMoto({this.id,
this.montoinicial,
this.idrecurrencia,
this.tipo,
this.vigencia});

  Map<String,dynamic> toMap(){ // used when inserting data to the database
    return <String,dynamic>{
      "id":id,
      "montoinicial":montoinicial,
      "idrecurrencia":idrecurrencia,
      "tipo":tipo,
      "vigencia":vigencia
    };
  }
}





