class InicialesRecurrente{
  final int id;
  final int idvigencia;
  final int idrecurrencia;
  final double inicial;
  final int vigencia;

  InicialesRecurrente({this.id,
    this.idvigencia,
    this.idrecurrencia,
    this.inicial,
    this.vigencia});

  Map<String,dynamic> toMap(){ // used when inserting data to the database
    return <String,dynamic>{
      "id":id,
      "idvigencia":idvigencia,
      "idrecurrencia":idrecurrencia,
      "inicial":inicial,
      "vigencia":vigencia
    };
  }
}





