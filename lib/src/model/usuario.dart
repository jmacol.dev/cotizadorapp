class Usuario{
  final int id;
  final String usuario;
  final String clave;
  final String nombre;
  final String cargo;
  final String token;
  final String sincronizar;

  Usuario({this.id, this.usuario,this.clave,this.nombre,this.cargo,this.token,this.sincronizar});

  Map<String,dynamic> toMap(){ // used when inserting data to the database
    return <String,dynamic>{
      "id" : id,
      "usuario" : usuario,
      "clave" : clave,
      "nombre" : nombre,
      "cargo" : cargo,
      "token" : token,
      "sincronizar" : sincronizar,
    };
  }
}