import 'package:appcotizador/src/pages/pdfcrear.dart';
import 'package:appcotizador/src/widgets/module.dart';
import 'package:pdf/pdf.dart';
import 'dart:io';
import 'package:pdf/widgets.dart';
import 'package:path_provider/path_provider.dart';
import 'package:flutter/material.dart' as material;

reportView(context, int id) async {
  final Document pdf = Document();
  prefs.datospdf(1);
  pdf.addPage(MultiPage(
      pageFormat:
      PdfPageFormat.letter.copyWith(marginBottom: 1.5 * PdfPageFormat.cm),
      crossAxisAlignment: CrossAxisAlignment.start,
      header: (Context context) {
        if (context.pageNumber == 1) {
          return null;
        }
        return Container(
            alignment: Alignment.topRight,
            margin: const EdgeInsets.only(bottom: 3.0 * PdfPageFormat.mm),
            padding: const EdgeInsets.only(bottom: 3.0 * PdfPageFormat.mm),
            decoration: const BoxDecoration(
                border:
                BoxBorder(bottom: true, width: 0.5, color: PdfColors.grey)),
            child: Text('Cotización Sede ',
                style: Theme.of(context)
                    .defaultTextStyle
                    .copyWith(color: PdfColors.grey)));
      },
      footer: (Context context) {
        return Container(
            alignment: Alignment.centerRight,
            margin: const EdgeInsets.only(top: 0.5 * PdfPageFormat.cm),
            child: Text('Cotización Sede ',
                style: Theme.of(context)
                    .defaultTextStyle
                    .copyWith(color: PdfColors.grey)),);
      },
      build: (Context context) => <Widget>[
        Header(
            level: 0,
            child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                  Text('Cotización Sede ', textScaleFactor:1.5),
                  PdfLogo()
                ])),
        Paragraph(          
            text:'CLIENTE: ${clienteauxdata.nombres} ${clienteauxdata.apellidopaterno} ${clienteauxdata.apellidomaterno}'),
        Paragraph(          
            text:'DOC. IDENTIDAD: ${clienteauxdata.dni}'),
        Paragraph(          
            text:'DIRECCIÓN: ${clienteauxdata.direccion}'),
        Paragraph(          
            text:'TELÉFONO: ${clienteauxdata.telefono}'),
        Padding(padding: const EdgeInsets.all(5)),
        Header(
            level: 0,
            child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                  Text('Lista de productos ', textScaleFactor:1.5)
                ])),
        Paragraph(          
            text:'PRECIO LISTA: ${cotizaciondata.precioreal} + ${cotizaciondata.descuentopromo} '),
        Paragraph(          
            text:'DESCUENTO: ${cotizaciondata.descuentopromo}'),
        Paragraph(          
            text:'PRECIO REAL: ${cotizaciondata.precioreal}'),       

        Padding(padding: const EdgeInsets.all(5)),
        Table.fromTextArray(
          headers: <dynamic>['DESCRIPCIÓN', 'CANTIDAD', 'PRECIO UNIT.', 'DESCUENTO', 'PRECIO REAL'],
          cellAlignment: Alignment.centerRight,
          data: <List>[
             [
               'A','B','C','D','E'
              //  '${purchases[0]['total_price']}',
              //  '${purchases[0]['quantity']}' ,
              //  '${purchases[0]['service_name_ar']}',
              //  '${purchases[0]['article_name_ar']}',
             ]
           ],),

        Padding(padding: const EdgeInsets.all(5)),
        Header(
            level: 0,
            child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                  Text('Cronograma ', textScaleFactor:1.5)
                ])),
        Table.fromTextArray(
          headers: <dynamic>['NRO', 'SALDO', 'AMORTIZ.', 'INTERÉS', 'GASTO COB.', 'IGV', 'MONTO FACT'],
          cellAlignment: Alignment.centerRight,          
          data: <List>[
             [
               'A','B','C','D','E','F','G'
              //  '${purchases[0]['total_price']}',
              //  '${purchases[0]['quantity']}' ,
              //  '${purchases[0]['service_name_ar']}',
              //  '${purchases[0]['article_name_ar']}',
             ]
           ],),
        Table.fromTextArray(
          headers: <dynamic>['INICIAL', 'PRÉSTAMO', 'CUOTAS', 'TOTAL CUOTAS', 'TOTAL COMPRA'],
          cellAlignment: Alignment.centerRight,
          data: <List>[
             [
               'A','B','C','D','E'
              //  '${purchases[0]['total_price']}',
              //  '${purchases[0]['quantity']}' ,
              //  '${purchases[0]['service_name_ar']}',
              //  '${purchases[0]['article_name_ar']}',
             ]
           ],),         
        
      ]),
      );
      
  //save PDF

  final String dir = (await getApplicationDocumentsDirectory()).path;
  final String path = '$dir/report.pdf';
  final File file = File(path);
  await file.writeAsBytes(pdf.save());
  material.Navigator.push(
                          context,
                          material.MaterialPageRoute(
                              builder: (context) => PdfViewerPage(path: path)),
                        );
  // material.Navigator.of(context).push(
  //   material.MaterialPageRoute(
  //     builder: (_) => PdfViewerPage(path: path),
  //   ),
  // );
  
}