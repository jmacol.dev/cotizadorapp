import 'package:appcotizador/src/db/dbProvider.dart';
import 'package:appcotizador/src/model/afectaciones.dart';
import 'package:appcotizador/src/model/canal.dart';
import 'package:appcotizador/src/model/categoria.dart';
import 'package:appcotizador/src/model/listacotizacion.dart';
import 'package:appcotizador/src/model/producto.dart';
import 'package:appcotizador/src/widgets/module.dart';
import 'package:appcotizador/src/model/cliente.dart';
import 'package:appcotizador/src/pages/dashboard.dart';
import 'package:appcotizador/src/pages/datosevaluacion.dart';
import 'package:appcotizador/src/widgets/header.dart';
import 'package:flutter/material.dart';
import 'package:flutter/scheduler.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:appcotizador/src/widgets/menu.dart';
import 'dart:convert'; 
import 'package:intl/intl.dart';
// import 'package:sweetalert/sweetalert.dart';
String mensajeval = "";
bool showmsj = false;

class CotizaList extends StatefulWidget  {
  CotizaList({Key key}) : super(key: key);

  @override
  CotizaListPageState createState() => CotizaListPageState();
  }
class CotizaListPageState extends State<CotizaList>  {
  TextEditingController controller = new TextEditingController();
  final globalKey = new GlobalKey<ScaffoldState>();  
  DbProvider db = DbProvider();
  final formatter = DateFormat("yyyy-MM-dd");  
  @override
  Widget build(BuildContext context){
  final prefs = new Sesion();  
  listacotizacionx();
  return Scaffold(
    // body: Center(child:  Text('App Cotizador'),)
    key: globalKey,
    drawer: NavDrawer(),
        appBar: AppBar(          
          backgroundColor: Color(0xff084B8A),
          title: Text('', style:TextStyle(fontSize: 36)),
          elevation: 0,
          
      ),
     body: Stack(
            children: <Widget>[
              Header(
                icon: FontAwesomeIcons.clipboardList, 
                titulo: 'Grupo Leoncito',
                subtitulo: 'Mis cotizaciones', hdif:80),
                Visibility (
                      visible: listCotizacion.length > 0 ? false : true,
                      child:
                      Center(
                        child: 
                        Container(
                          margin: EdgeInsets.only(top:320, left: 0, right: 0),
                        padding: new EdgeInsets.all(10.0),
                        width: MediaQuery.of(context).size.width * 0.9,       
                        decoration:
                            BoxDecoration(border: Border.all(color: Colors.redAccent), borderRadius: BorderRadius.all(Radius.circular(10.0))),
                        child: FittedBox(
                          child: Text(
                          "No hay cotizaciones registradas del ${DateFormat("dd-MM-yyyy").format(DateTime.now()).toString()}", textAlign:  TextAlign.center,
                          style: TextStyle(color: Colors.redAccent),
                        ),
                      ),
                      )
                      ),
                      ),
            Container(
              margin: EdgeInsets.only(top:210, left: 0, right: 0),
            color: Color(0xff084B8A),
            child: new Padding(
              padding: const EdgeInsets.all(8.0),
              child: new Card(
                child: new ListTile(
                  leading: new Icon(Icons.search),
                  title: new TextField(
                    controller: controller,
                    decoration: new InputDecoration(
                        hintText: 'Buscar por nombre o dni', border: InputBorder.none),
                    onChanged: (text) {
                      setState(() {
                          filtrarcotizacionx(text);
                      });
                    },
                   // onChanged: onSearchTextChanged,
                  ),
                  trailing: new IconButton(icon: new Icon(Icons.cancel), 
                  onPressed: () {
                    setState(() {
                    controller.clear();
                    listacotizacionx();
                    });
                   // onSearchTextChanged('');
                  },
                  
                  ),
                )))),
              Container(
              margin: EdgeInsets.only(top:290, left: 20, right: 20),
              
              child: 
                
                new ListView(
                      children : 
                        
                      listCotizacion.map(buildItem).toList(),

                      

                  ))
              
            ]),
      floatingActionButton: FloatingActionButton(
        onPressed: () async {
          Navigator.push(
                context,
                new MaterialPageRoute(
                    builder: (context) => Dashboard()));
          // Add your onPressed code here!
        },
        child: Icon(FontAwesomeIcons.elementor),
        backgroundColor: Colors.redAccent,
      ),  
    );

    
  }
  
  
}



Widget buildItem(ListaCotizacion item) {
  IconData icon  = item.enviada == 1 ? Icons.send : Icons.autorenew;
  return new 
  Card(
      elevation: 8.0,
      margin: new EdgeInsets.symmetric(horizontal: 2.0, vertical: 6.0),
      child: Container(
        decoration: BoxDecoration(color: Color(0xff72B223)),
        child: 
  ListTile(
        contentPadding: EdgeInsets.symmetric(horizontal: 20.0, vertical: 10.0),
        leading: Container(
          padding: EdgeInsets.only(right: 12.0),
          decoration: new BoxDecoration(
              border: new Border(
                  right: new BorderSide(width: 1.0, color: Colors.blueAccent))),
          child: Icon(icon, color: Colors.blueGrey[600], size: 50,),
        ),
        title: Text(
          "${item.dni} - ${item.cliente} (${item.canal})",
          style: TextStyle(color: Color(0xff003B7C), fontWeight: FontWeight.bold, fontSize: 17),
        ),
        // subtitle: Text("Intermediate", style: TextStyle(color: Colors.white)),

        subtitle: Row(
          children: <Widget>[
            // Icon(Icons.linear_scale, color: Colors.yellowAccent),
            Text('Total Cuotas: S/ ${item.montocuotas.toStringAsFixed(2)}'
            '\nInicial: S/ ${item.montoinicial.toStringAsFixed(2)}'
            '\nTotal Venta: S/ ${item.montototal.toStringAsFixed(2)}', style: TextStyle(color: Colors.blueGrey[800], fontSize: 18))
          ],
        ),
        onTap: (){
        },
        trailing:
            Icon(Icons.keyboard_arrow_right, color: Colors.yellowAccent, size: 50.0)),
            
      ));
}

listacotizacionx() async {
    List<ListaCotizacion> _futureOfList = await db.fetchListCotizacion();
     listCotizacion = new List<ListaCotizacion>();
    _futureOfList.forEach((element) {
      listCotizacion.add(element);
    });
    
  }

 filtrarcotizacionx(String filtercriterio) async {
    List<ListaCotizacion> _futureOfList = await db.filterListCotizacion(filtercriterio);
     listCotizacion = new List<ListaCotizacion>();
    _futureOfList.forEach((element) {
      listCotizacion.add(element);
    });
    
  }
    



