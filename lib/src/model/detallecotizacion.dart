class DetalleCotizacion{
final int id;
final double cantidad;
final int idproducto;
final String descripcion;
final double preciounitario;
final double preciototal;
final double dsctoperfilcrediticio;
final double dsctosegmentacion;
final double dsctolineaproducto;
final double dsctoplazo;
final double dsctocombo;
final double dsctoexcepcion;
final double descuentototal;
final double precioreal;
final int idcotizacion;
  
  DetalleCotizacion({
      this.id,
      this.cantidad,
      this.idproducto,
      this.descripcion,
      this.preciounitario,
      this.preciototal,
      this.dsctoperfilcrediticio,
      this.dsctosegmentacion,
      this.dsctolineaproducto,
      this.dsctoplazo,
      this.dsctocombo,
      this.dsctoexcepcion,
      this.descuentototal,
      this.precioreal,
      this.idcotizacion
  });

    Map<String,dynamic> toMap(){ // used when inserting data to the database
    return <String,dynamic>{
      "id" :id,
      "cantidad" :cantidad,
      "idproducto" :idproducto,
      "descripcion" :descripcion,
      "preciounitario" :preciounitario,
      "preciototal" :preciototal,
      "dsctoperfilcrediticio" :dsctoperfilcrediticio,
      "dsctosegmentacion" :dsctosegmentacion,
      "dsctolineaproducto" :dsctolineaproducto,
      "dsctoplazo" :dsctoplazo,
      "dsctocombo" :dsctocombo,
      "dsctoexcepcion" :dsctoexcepcion,
      "descuentototal" :descuentototal,
      "precioreal" :precioreal,
      "idcotizacion" :idcotizacion
    };
  }
}