import 'package:appcotizador/src/db/dbProvider.dart';
import 'package:appcotizador/src/model/afectaciones.dart';
import 'package:appcotizador/src/model/canal.dart';
import 'package:appcotizador/src/model/categoria.dart';
import 'package:appcotizador/src/model/cotizacion.dart';
import 'package:appcotizador/src/model/producto.dart';
import 'package:appcotizador/src/widgets/module.dart';
import 'package:appcotizador/src/model/cliente.dart';
import 'package:appcotizador/src/model/usuario.dart';
import 'package:appcotizador/src/pages/dashboard.dart';
import 'package:appcotizador/src/pages/datosevaluacion.dart';
import 'package:appcotizador/src/widgets/header.dart';
import 'package:flutter/material.dart';
import 'package:flutter/scheduler.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:appcotizador/src/widgets/menu.dart';
import 'dart:convert';
import 'package:intl/intl.dart';
import 'package:http/http.dart';

// import 'package:sweetalert/sweetalert.dart';
String mensajeval = "";
bool showmsj = false;

class EvaluarCliente extends StatefulWidget {
  EvaluarCliente({Key key}) : super(key: key);

  @override
  EvaluarClientePageState createState() => EvaluarClientePageState();
}

class EvaluarClientePageState extends State<EvaluarCliente> {
  final TextEditingController dniCtrl = new TextEditingController();
  final TextEditingController apepaternoCtrl = new TextEditingController();
  final TextEditingController apematernoCtrl = new TextEditingController();
  final TextEditingController nombresCtrl = new TextEditingController();
  final TextEditingController fechanacCtrl = new TextEditingController();
  final globalKey = new GlobalKey<ScaffoldState>();
  DbProvider db = DbProvider();
  final formatter = DateFormat("yyyy-MM-dd");
  Cliente clienteaux = new Cliente();
  @override
  Widget build(BuildContext context) {
    final prefs = new Sesion();
    return Scaffold(
      // body: Center(child:  Text('App Cotizador'),)
      key: globalKey,
      drawer: NavDrawer(),
      appBar: AppBar(
        backgroundColor: Color(0xff084B8A),
        title: Text('', style: TextStyle(fontSize: 36)),
        elevation: 0,
      ),
      body: Stack(children: <Widget>[
        Container(
          margin: EdgeInsets.only(top: 250, left: 70, right: 70),
          child: SingleChildScrollView(
            child: Column(children: <Widget>[
              Padding(
                  padding: EdgeInsets.all(0),
                  child: TextForm(
                      show: true,
                      icon: FontAwesomeIcons.idCard,
                      item: TextFormField(
                        style: new TextStyle(fontSize: 20),
                        maxLength: 8,
                        enabled: prefs.datosevaluar,
                        onChanged: (newValue) {
                          setState(() {
                            if (dniCtrl.text.trim().isEmpty) {
                              prefs.toastmessage("Igrese DNI de cliente.",
                                  Colors.redAccent, Colors.white);
                            }
                          });
                        },
                        controller: dniCtrl,
                        decoration: new InputDecoration(
                          labelText: 'Ingrese DNI',
                        ),
                        // validator: validaUsuario,
                      ))),
              Padding(
                  padding: EdgeInsets.all(0),
                  child: TextForm(
                      show: !prefs.clienteencontrado && !prefs.datosevaluar,
                      icon: null,
                      item: TextFormField(
                        style: new TextStyle(fontSize: 20),
                        maxLength: 50,
                        onChanged: (newValue) {
                          setState(() {
                            if (apepaternoCtrl.text.trim().isEmpty) {
                              prefs.toastmessage("Igrese apellido paterno.",
                                  Colors.redAccent, Colors.white);
                            }
                          });
                        },
                        controller: apepaternoCtrl,
                        decoration: new InputDecoration(
                          labelText: 'Ingrese apellido paterno',
                        ),
                        // validator: validaClave,
                      ))),
              Padding(
                  padding: EdgeInsets.all(0),
                  child: TextForm(
                      show: !prefs.clienteencontrado && !prefs.datosevaluar,
                      icon: null,
                      item: TextFormField(
                        style: new TextStyle(fontSize: 20),
                        maxLength: 50,
                        onChanged: (newValue) {
                          setState(() {
                            if (apematernoCtrl.text.trim().isEmpty) {
                              prefs.toastmessage("Igrese apellido materno.",
                                  Colors.redAccent, Colors.white);
                            }
                          });
                        },
                        controller: apematernoCtrl,
                        decoration: new InputDecoration(
                          labelText: 'Ingrese apellido materno',
                        ),
                        // validator: validaClave,
                      ))),
              Padding(
                  padding: EdgeInsets.all(0),
                  child: TextForm(
                      show: !prefs.clienteencontrado && !prefs.datosevaluar,
                      icon: null,
                      item: TextFormField(
                        style: new TextStyle(fontSize: 20),
                        maxLength: 50,
                        onChanged: (newValue) {
                          setState(() {
                            if (nombresCtrl.text.trim().isEmpty) {
                              prefs.toastmessage("Igrese nombres.",
                                  Colors.redAccent, Colors.white);
                            }
                          });
                        },
                        controller: nombresCtrl,
                        decoration: new InputDecoration(
                          labelText: 'Ingrese nombres',
                        ),
                        // validator: validaClave,
                      ))),
              Padding(
                  padding: EdgeInsets.all(0),
                  child: TextForm(
                      show: !prefs.clienteencontrado && !prefs.datosevaluar,
                      icon: null,
                      item: TextFormField(
                        style: new TextStyle(fontSize: 20),
                        controller: fechanacCtrl,
                        maxLength: 10,
                        decoration: new InputDecoration(
                          labelText: 'Ingrese fecha de nacimiento',
                        ),
                        onTap: () async {
                          DateTime date = DateTime(1900);
                          FocusScope.of(context).requestFocus(new FocusNode());

                          date = await showDatePicker(
                              context: context,
                              initialDate: DateTime.now(),
                              firstDate: DateTime(1900),
                              lastDate: DateTime(2100));

                          fechanacCtrl.text = formatter.format(date);
                        },
                      ))),
              Container(
                  alignment: Alignment.bottomLeft,
                  width: MediaQuery.of(context).size.width,
                  child: Row(
                    children: <Widget>[
                      GestureDetector(
                          onTap: () {
                            validarevaluacion(context);
                          },
                          child: Container(
                            margin: new EdgeInsets.all(0.0),
                            alignment: Alignment.center,
                            color: Color(0xff084B8A),
                            width: MediaQuery.of(context).size.width * 0.6,
                            child: Text("EVALUAR CLIENTE",
                                style: TextStyle(
                                    color: Colors.white,
                                    fontSize: 24,
                                    fontWeight: FontWeight.w500)),
                            padding: EdgeInsets.only(top: 16, bottom: 16),
                          )),
                      SizedBox(
                          height: 60.0,
                          width: MediaQuery.of(context).size.width * 0.15,
                          child: RaisedButton(
                              color: Colors.deepOrangeAccent,
                              textColor: Colors.yellow[50],
                              padding: EdgeInsets.fromLTRB(10, 10, 10, 10),
                              splashColor: Colors.grey,
                              child: Icon(FontAwesomeIcons.times, size: 40),
                              onPressed: () async {
                                setState(() {
                                  dniCtrl.text = "";
                                  prefs.clienteencontrado = false;
                                  prefs.datosevaluar = true;
                                });
                              })),
                    ],
                  ))
            ]),
          ),
        ),
        Visibility(
            visible: isLoading,
            child: new Container(
              child: new Stack(
                children: <Widget>[
                  new Container(
                    alignment: AlignmentDirectional.center,
                    decoration: new BoxDecoration(
                      color: Colors.white70,
                    ),
                    child: new Container(
                      decoration: new BoxDecoration(
                          color: Colors.black87,
                          borderRadius: new BorderRadius.circular(10.0)),
                      width: 300.0,
                      height: 200.0,
                      alignment: AlignmentDirectional.center,
                      child: new Column(
                        crossAxisAlignment: CrossAxisAlignment.center,
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: <Widget>[
                          new Center(
                            child: new SizedBox(
                              height: 50.0,
                              width: 50.0,
                              child: new CircularProgressIndicator(
                                value: null,
                                strokeWidth: 7.0,
                              ),
                            ),
                          ),
                          new Container(
                            margin: const EdgeInsets.only(top: 25.0),
                            child: new Center(
                              child: new Text(
                                "Evaluando cliente...",
                                style: new TextStyle(
                                    color: Colors.white, fontSize: 30),
                              ),
                            ),
                          ),
                        ],
                      ),
                    ),
                  ),
                ],
              ),
            )),
        Header(
            icon: FontAwesomeIcons.userTag,
            titulo: 'Grupo Leoncito',
            subtitulo: 'Evaluar Cliente',
            hdif: 80),
      ]),
      floatingActionButton: FloatingActionButton(
        onPressed: () async {
          // await db.deleteCotizaciones();
          // prefs.pendientes = (await db.fetchCotizacion(true)).length;
          Navigator.push(context,
              new MaterialPageRoute(builder: (context) => Dashboard()));
          // Add your onPressed code here!
        },
        child: Icon(FontAwesomeIcons.elementor),
        backgroundColor: Colors.redAccent,
      ),
    );
  }

  bool validarregistro() {
    bool state = true;
    if (apepaternoCtrl.text.trim().isEmpty) {
      prefs.toastmessage(
          "Igrese apellido paterno.", Colors.redAccent, Colors.white);
      state = false;
    } else if (apematernoCtrl.text.trim().isEmpty) {
      prefs.toastmessage(
          "Igrese apellido materno.", Colors.redAccent, Colors.white);
      state = false;
    } else if (nombresCtrl.text.trim().isEmpty) {
      prefs.toastmessage("Igrese nombre(s).", Colors.redAccent, Colors.white);
      state = false;
    } else if (fechanacCtrl.text.trim().isEmpty) {
      prefs.toastmessage(
          "Igrese fechanacimiento.", Colors.redAccent, Colors.white);
      state = false;
    }

    return state;
  }

  estadovariables(String msj, bool state) async {
    setState(() {
      showmsj = state;
      mensajeval = msj;
    });

    await Future.delayed(const Duration(milliseconds: 2500), () async {
      if (mounted) {
        setState(() {
          showmsj = false;
          mensajeval = "";
        });
      }
    });
  }

  validarevaluacion(BuildContext context) async {
    try {
      if (dniCtrl.text.trim().isEmpty) {
        prefs.toastmessage(
            "Igrese documento de identidad.", Colors.redAccent, Colors.white);
        return false;
      }
      var stateaux = await conectnet();
      if (prefs.datosevaluar) {
        prefs.dnicliente = this.dniCtrl.text;
        var clienteaux = await db.getCliente(this.dniCtrl.text);
        if (clienteaux == null) {
          if (!stateaux) {
            //  cliente nuevo llenar campos si no hay internet
            prefs.toastmessage(
                "No hay conexión a internet. Ingrese datos del contacto",
                Colors.black87,
                Colors.yellow[700]);
            setState(() {
              prefs.datosevaluar = false;
              this.dniCtrl.text = prefs.dnicliente;
            });
          } else {
            //  cliente nuevo consultar api si hay internet
            consultarapicliente();
          }
          //  Navigator.pushReplacementNamed(context, 'evaluar');
        } else {
          final fechareg = DateTime.parse(clienteaux.fecharegistro);
          final fechactual = DateTime.now();
          final difdias = fechactual.difference(fechareg).inDays;
          if (difdias > 30) {
            if (stateaux) {
              // fecha de registro > 30 días - con acceso a internet
              await db.deleteClienteDni(dniCtrl.text);
              consultarapicliente();
            } else {
              // fecha de registro >30 días - sin acceso a internet
              cargardatoscliente(clienteaux);
            }
          } else {
            // fecha de registro <= 30 días
            cargardatoscliente(clienteaux);
          }
        }
      } else {
        if (validarregistro()) {
          prefs.datosevaluar = true;
          prefs.clienteencontrado = true;
          final mdCliente = Cliente(
            id: 0,
            referenciaorigen: 0,
            dni: dniCtrl.text,
            ruc: "",
            apellidopaterno: apepaternoCtrl.text,
            apellidomaterno: apematernoCtrl.text,
            nombres: nombresCtrl.text,
            fechanacimiento: fechanacCtrl.text,
            direccion: "",
            telefono: "",
            idevaleo: 0,
            idevasen: 0,
            asignacioncme: 0,
            cmeasignada: 0.0,
            cmedisponible: 0.0,
            lineacredito: 0.0,
            lineadisponible: 0.0,
            idresultado: 0,
            resultado: "CLIENTE NUEVO",
            excepcion: 0,
            motivoexcepcion: "",
            idvigencia: 0,
            vigencia: "",
            idrecurrencia: 6,
            recurrencia: "NUEVO",
            idestado: 0,
            estado: "",
            idbancarizacion: 0,
            bancarizacion: "SIN EVALUAR",
            idnivelriesgo: 0,
            nivelriesgo: "",
            registrado: 0,
          );

          await db.insertCliente(mdCliente);
          var clienteaux = await db.getCliente(this.dniCtrl.text);
          clienteauxdata = clienteaux;
          edadaux = calcularedad(clienteaux.fechanacimiento);
          cargarparametros();
          Navigator.push(context,
              MaterialPageRoute(builder: (context) => DatosEvaluacion()));
        }
      }
    } catch (e_) {
      setState(() {
        isLoading = false;
      });
      prefs.toastmessage(e_.toString(), Colors.redAccent, Colors.white);
    }
  }

  consultarapicliente() async {
    setState(() {
      isLoading = true;
    });
    prefs.logueonuevo();
    Usuario auxUser = await db.fetchUsuario();
    prefs.toastmessage(
        "Evaluando cliente, espere mientras carga la información.",
        Colors.greenAccent,
        Colors.black);
    var dataclie = jsonEncode({
      "dni": this.dniCtrl.text,
      "tipodoc": "D",
      "fechanacimiento": null,
      "nombre": null,
      "paterno": null,
      "materno": null,
      "nombrecompleto": null,
      "personalregistro": auxUser.id,
      "token": auxUser.token
    });
    Response response =
        await URLS().postData('evaluar-cliente-sync', dataclie, true);
    var jsonauxclie = jsonDecode(response.body);

    if (jsonauxclie['estado']) {
      var datosclie = jsonauxclie['data'][0].split('|');
      final mdCliente = Cliente(
        id: int.parse(datosclie[0].toString()),
        referenciaorigen: int.parse(datosclie[1].toString()),
        dni: datosclie[2].toString(),
        ruc: datosclie[3].toString(),
        apellidopaterno: datosclie[4].toString(),
        apellidomaterno: datosclie[5].toString(),
        nombres: datosclie[6].toString().replaceAll("'", ""),
        fechanacimiento: datosclie[7].toString(),
        direccion: datosclie[8].toString(),
        telefono: datosclie[9].toString(),
        idevaleo: int.parse(datosclie[10].toString()),
        idevasen: int.parse(datosclie[12].toString()),
        asignacioncme: int.parse(datosclie[11].toString()),
        cmeasignada:
            double.parse(double.parse(datosclie[13]).toStringAsFixed(2)),
        cmedisponible:
            double.parse(double.parse(datosclie[14]).toStringAsFixed(2)),
        lineacredito:
            double.parse(double.parse(datosclie[15]).toStringAsFixed(2)),
        lineadisponible:
            double.parse(double.parse(datosclie[16]).toStringAsFixed(2)),
        idresultado: int.parse(datosclie[17].toString()),
        resultado: datosclie[18].toString(),
        excepcion: int.parse(datosclie[19].toString()),
        motivoexcepcion: datosclie[20].toString(),
        idvigencia: int.parse(datosclie[21].toString()),
        vigencia: datosclie[22].toString(),
        idrecurrencia: int.parse(datosclie[23].toString()),
        recurrencia: datosclie[24].toString(),
        idestado: int.parse(datosclie[25].toString()),
        estado: datosclie[26].toString(),
        idbancarizacion: int.parse(datosclie[27].toString()),
        bancarizacion: datosclie[28].toString(),
        idnivelriesgo: int.parse(datosclie[29].toString()),
        nivelriesgo: datosclie[30].toString(),
        fecharegistro: datosclie[31].toString(),
        registrado: int.parse(datosclie[32].toString()),
      );
      await db.insertCliente(mdCliente);
      clienteaux = await db.getCliente(this.dniCtrl.text);
      cargardatoscliente(clienteaux);
    } else {
      setState(() {
        prefs.datosevaluar = false;
        this.dniCtrl.text = prefs.dnicliente;
      });
      prefs.toastmessage(
          jsonauxclie['descripcion'], Colors.redAccent, Colors.white);
    }
    setState(() {
      isLoading = false;
    });
  }

  cargardatoscliente(Cliente clienteaux) {
    prefs.datosevaluar = true;
    prefs.clienteencontrado = true;
    edadaux = calcularedad(clienteaux.fechanacimiento);
    clienteauxdata = clienteaux;
    cargarparametros();
    Navigator.push(
        context, MaterialPageRoute(builder: (context) => DatosEvaluacion()));
  }

  Future<List<Producto>> productosData() async {
    return await db.fetchProductoFilter("PRODUCTO ESTANDAR");
  }

  Future<List<Categoria>> categoriasuperior(int codigo) async {
    List<Categoria> dataCat = await db.fetchCategoriaSuperior(codigo);
    return dataCat;
  }

  cargarparametros() {
    Future<List<Producto>> _futureOfListProd = productosData();
    dataProducto = new List<Producto>();
    dataSelProducto = new List<Producto>();
    selectedProductos = new List<Producto>();
    // dataSelProducto = new List<Producto>();
    _futureOfListProd.then((value) => {
          value.toList().forEach((element) {
            dataProducto.add(element);
          })
        });

    cbodepartamento = new List<Combos>();
    cbodepartamento.add(new Combos(0, 'DEPARTAMENTO'));
    Future<List<Categoria>> _futureOfListDpto = categoriasuperior(357);

    // dataSelProducto = new List<Producto>();
    _futureOfListDpto.then((value) => {
          value.toList().forEach((element) {
            cbodepartamento.add(new Combos(element.id, element.descripcion));
          })
        });

    cbodepartamento = [new Combos(0, 'DEPARTAMENTO')];
    cboprovincia = [new Combos(0, 'PROVINCIA')];
    cbodistrito = [new Combos(0, 'DISTRITO')];
    cbocentropoblado = [new Combos(0, 'CENTRO POBLADO')];
    cbocuotas = [new Combos(0, 'CUOTAS')];
    Future<List<Afectaciones>> _futureOfListCtas =
        prefs.cargarcuotas(false, false);
    // dataSelProducto = new List<Producto>();
    _futureOfListCtas.then((value) => {
          value.toList().forEach((element) {
            cbocuotas.add(new Combos(
                int.parse(element.descripcion), element.descripcion));
          })
        });

    canaldata = [new Combos(0, 'CANAL')];
    Future<List<Canal>> _futureOfListCn = prefs.cargarcanal();

    // dataSelProducto = new List<Producto>();

    _futureOfListCn.then((value) => {
          value.toList().forEach((element) {
            canaldata.add(new Combos(element.id, element.descripcion));
          })
        });
    prefs.limpiarcotizacion();
  }

  int calcularedad(String fechanac) {
    // error_log($fechanacimiento);
    var fecha = formatter.format(DateTime.now()).toString().split('-');
    var fechanacaux = fechanac.split('-');
    int anios = (int.parse(fecha[0]) - int.parse(fechanacaux[0]));
    int meses = (int.parse(fecha[1]) - int.parse(fechanacaux[1]));
    int dias = (int.parse(fecha[2]) - int.parse(fechanacaux[2]));
    if (meses < 0) {
      anios -= 1;
    } else if (meses == 0) {
      if (dias < 0) anios -= 1;
    }
    return anios;
  }
}

String validaUsuario(String value) {
  String pattern = r'(^[a-zA-Z ]*$)';
  RegExp regExp = new RegExp(pattern);
  if (value.trim().length == 0) {
    return "El usuario es requerido.";
  } else if (!regExp.hasMatch(value)) {
    return "El usuario debe de ser a-z y A-Z.";
  }
  return null;
}

String validaClave(String value) {
  if (value.trim().length == 0) {
    return "La contraseña es requerida.";
  }
  return null;
}
