class Cronograma{
  final int id;
  final int nrocuota;
  final double amortizacion;
  final double interes;
  final double gasto;
  final double monto;
  final double igv;
  final double saldo;
  final double facturar;
  final int idcotizacion;
  
  Cronograma({
      this.id,
      this.nrocuota,
      this.amortizacion, 
      this.interes,
      this.gasto,
      this.monto,
      this.igv,
      this.saldo,
      this.facturar,
      this.idcotizacion,
  });

    Map<String,dynamic> toMap(){ // used when inserting data to the database
    return <String,dynamic>{
      "id": id,
      "nrocuota": nrocuota,
      "amortizacion": amortizacion,
      "interes": interes,
      "gasto": gasto,
      "monto": monto,
      "igv": igv,
      "saldo": saldo,
      "facturar": facturar,
      "idcotizacion": idcotizacion,
    };
  }
}