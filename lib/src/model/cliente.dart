import 'dart:convert';

class Cliente{  
  int id;
  int referenciaorigen;
  String dni;
  String ruc;
  String apellidopaterno;
  String apellidomaterno;
  String nombres;
  String fechanacimiento;
  String direccion;
  String telefono;
  int idevaleo;
  int idevasen;
  int asignacioncme;
  double cmeasignada;
  double cmedisponible;
  double lineacredito;
  double lineadisponible;
  int idresultado;
  String resultado;
  int excepcion;
  String motivoexcepcion;
  int idvigencia;
  String vigencia;
  int idrecurrencia;
  String recurrencia;
  int idestado;
  String estado;
  int idbancarizacion;
  String bancarizacion;
  int idnivelriesgo;
  String nivelriesgo;
  String fecharegistro;
  int registrado;

  Cliente({this.id,
  this.referenciaorigen,
  this.dni,
  this.ruc,
  this.apellidopaterno,
  this.apellidomaterno,
  this.nombres,
  this.fechanacimiento,
  this.direccion,
  this.telefono,
  this.idevaleo,
  this.idevasen,
  this.asignacioncme,
  this.cmeasignada,
  this.cmedisponible,
  this.lineacredito,
  this.lineadisponible,
  this.idresultado,
  this.resultado,
  this.excepcion,
  this.motivoexcepcion,
  this.idvigencia,
  this.vigencia,
  this.idrecurrencia,
  this.recurrencia,
  this.idestado,
  this.estado,
  this.idbancarizacion,
  this.bancarizacion,
  this.idnivelriesgo,
  this.nivelriesgo,
  this.fecharegistro,
  this.registrado });

  Map<String,dynamic> toMap(){ // used when inserting data to the database
    return <String,dynamic>{
      "id" : id,
      "referenciaorigen":referenciaorigen,
      "dni":dni,
      "ruc":ruc,
      "apellidopaterno":apellidopaterno,
      "apellidomaterno":apellidomaterno,
      "nombres":nombres,
      "fechanacimiento":fechanacimiento,
      "direccion":direccion,
      "telefono":telefono,
      "idevaleo":idevaleo,
      "idevasen":idevasen,
      "asignacioncme":asignacioncme,
      "cmeasignada":cmeasignada,
      "cmedisponible":cmedisponible,
      "lineacredito":lineacredito,
      "lineadisponible":lineadisponible,
      "idresultado":idresultado,
      "resultado":resultado,
      "excepcion":excepcion,
      "motivoexcepcion":motivoexcepcion,
      "idvigencia":idvigencia,
      "vigencia":vigencia,
      "idrecurrencia":idrecurrencia,
      "recurrencia":recurrencia,
      "idestado":idestado,
      "estado":estado,
      "idbancarizacion":idbancarizacion,
      "bancarizacion":bancarizacion,
      "idnivelriesgo":idnivelriesgo,
      "nivelriesgo":nivelriesgo,
      "fecharegistro":fecharegistro,
      "registrado":registrado
    };
  }

  
}




