class TotalCotizacion{  
  final String fecha;
  final int cantidad;
  final int codigolocal;

  TotalCotizacion({this.fecha,this.cantidad,this.codigolocal});

  Map<String,dynamic> toMap(){ // used when inserting data to the database
    return <String,dynamic>{
      "fecha" : fecha,
      "cantidad" : cantidad,
      "codigolocal" : codigolocal
    };
  }
}