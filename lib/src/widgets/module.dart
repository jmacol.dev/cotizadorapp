import 'dart:convert';
import 'dart:io';
import 'dart:math';

import 'package:animate_do/animate_do.dart';
import 'package:appcotizador/src/db/dbProvider.dart';
import 'package:appcotizador/src/model/afectaciones.dart';
import 'package:appcotizador/src/model/canal.dart';
import 'package:appcotizador/src/model/categoria.dart';
import 'package:appcotizador/src/model/centropoblado.dart';
import 'package:appcotizador/src/model/cliente.dart';
import 'package:appcotizador/src/model/cotizacion.dart';
import 'package:appcotizador/src/model/cronograma.dart';
import 'package:appcotizador/src/model/detallecotizacion.dart';
import 'package:appcotizador/src/model/estado.dart';
import 'package:appcotizador/src/model/inicialesmoto.dart';
import 'package:appcotizador/src/model/inicialesrecurrente.dart';
import 'package:appcotizador/src/model/nivelriesgo.dart';
import 'package:appcotizador/src/model/listacotizacion.dart';
import 'package:appcotizador/src/model/parametroscotizacion.dart';
import 'package:appcotizador/src/model/producto.dart';
import 'package:appcotizador/src/model/recurrencia.dart';
import 'package:appcotizador/src/model/resultadocotizacion.dart';
import 'package:appcotizador/src/model/totalcotizacion.dart';
import 'package:appcotizador/src/model/usuario.dart';
import 'package:appcotizador/src/model/vigencia.dart';
import 'package:appcotizador/src/pages/dashboard.dart';
import 'package:flutter/material.dart';
// import 'package:flutter_string_encryption/flutter_string_encryption.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:path_provider/path_provider.dart';
import 'package:shared_preferences/shared_preferences.Dart';
import 'package:http/http.dart';
import 'package:connectivity/connectivity.dart';
import 'package:fluttertoast/fluttertoast.dart';
// import 'package:http/http.dart' as http;
// import 'package:json_serializable/json_serializable.dart';

final List<Locales> locales = [new Locales(0, 'Seleccione local')];
List<TipoCotizacion> tipocotizacion = [
  new TipoCotizacion('PRODUCTO ESTANDAR', 'PRODUCTO ESTANDAR'),
  new TipoCotizacion('MOTO', 'MOTO'),
  new TipoCotizacion('TELECOMUNICACIONES', 'TELECOMUNICACIONES')
];
List<Combos> cbovivienda = [
  new Combos(0, 'TIPO VIVIENDA'),
  new Combos(8, 'PROPIA'),
  new Combos(9, 'FAMILIAR'),
  new Combos(10, 'ALQUILADA')
];
List<Combos> cboguardar = [
  new Combos(100, 'TIPO REGISTRO'),
  new Combos(1, 'VENDER'),
  new Combos(10, 'AFILIAR'),
  new Combos(-1, 'GUARDAR')
];
List<Combos> cbodepartamento = [new Combos(0, 'DEPARTAMENTO')];
List<Combos> cboprovincia = [new Combos(0, 'PROVINCIA')];
List<Combos> cbodistrito = [new Combos(0, 'DISTRITO')];
List<Combos> cbocentropoblado = [new Combos(0, 'CENTRO POBLADO')];
List<Combos> cbocuotas = [new Combos(0, 'CUOTAS')];
Cliente clienteauxdata = new Cliente();
Producto productoauxdata = new Producto();
Cotizacion cotizaciondata = new Cotizacion();
List<Cronograma> cronogramaData = new List<Cronograma>();
List<DetalleCotizacion> detallecotizacionData = new List<DetalleCotizacion>();
List<Combos> canaldata = [new Combos(0, 'CANAL')];
double preciolista = 0.0;
double descuento = 0.0;
double precioreal = 0.0;
double porcentajeinicial = 0.0;
int plazoinicial = 0;
double pagoinicial = 0.0;
double prestamo = 0.0;
double totalcronograma = 0.0;
double totalventa = 0.0;
double inicialorig = 0.0;
int edadaux = 0;
List<Cliente> dataCliente = new List<Cliente>();
List<Producto> dataProducto = new List<Producto>();
List<Producto> dataSelProducto = new List<Producto>();
List<Producto> selectedProductos = new List<Producto>();
List<ListaCotizacion> listCotizacion = new List<ListaCotizacion>();
int valueguardar = 100;
int valuevivienda = 0;
int valuedepartamento = 0;
int valueprovincia = 0;
int valuedistrito = 0;
int valuecentropoblado = 0;
int valuecuotas = 0;
int valuecanal = 0;
bool showloc = false;
bool isCombo = true;
bool isDescuento = false;
bool isAmpliarCta = false;
bool isCotiza = false;
String direccionCtrl = "";
String telefonoCtrl = "";
bool isLoading = false;
Alignment alignaux = Alignment.topRight;
ResultadoCotizacion res = new ResultadoCotizacion();
final keyClave = "l30nc1t0";
final prefs = new Sesion();
DbProvider db = DbProvider();
Future<bool> conectnet() async {
  bool state = true;
  var connectivityResult = await (Connectivity().checkConnectivity());
  if (connectivityResult == ConnectivityResult.mobile) {
    // Si se conecta por datos de móvil.
    state = true;
  } else if (connectivityResult == ConnectivityResult.wifi) {
    // Si se conecta a una red wifi.
    state = true;
  } else if (connectivityResult == ConnectivityResult.none) {
    // No hay conexión.
    state = false;
  }
  return state;
}

Future<dynamic> loginapi(String usuario, String clave) async {
  final prefs = new Sesion();
  // final cryptor = new PlatformStringCryptor();
  DbProvider db = DbProvider();
  String json =
      jsonEncode({"usuario": usuario, "password": clave, "recaptcha": true});
  Response response = await URLS().postData('login', json, false);
  var data = jsonDecode(response.body);
  if (data['estado']) {
    prefs.sesion = true;
    prefs.token = data['token'];
    final mdusuario = Usuario(
        id: data['user']['id'],
        usuario: usuario,
        clave: clave,
        nombre: data['user']['nombre'],
        cargo: data['user']['cargo'],
        token: prefs.token,
        sincronizar: '0');
    prefs.usuario = jsonEncode({
      "usuario": usuario,
      "nombre": data['user']['nombre'],
      "cargo": data['user']['cargo']
    });
    await db.loginUsuario(mdusuario);
  }
  return data;
}

final items = <ItemBoton>[
  new ItemBoton(FontAwesomeIcons.syncAlt, 'Sincronizar', Color(0xff6989F5),
      Color(0xff906EF5)),
  new ItemBoton(FontAwesomeIcons.userTag, 'Evaluar Contacto', Color(0xff66A9F2),
      Color(0xff536CF6)),
  new ItemBoton(
      FontAwesomeIcons.shareSquare,
      'Enviar Pendientes (${prefs.pendientes.toString()})',
      Color(0xffF2D572),
      Color(0xffE06AA3)),
  new ItemBoton(FontAwesomeIcons.clipboardList, 'Mis Cotizaciones',
      Color(0xff317183), Color(0xff46997D)),
];

List<Widget> itemMap = items
    .map((item) => FadeInLeft(
          duration: Duration(milliseconds: 250),
          child: BotonDashboard(
            icon: item.icon,
            texto: 'Enviar Pendientes (${prefs.pendientes.toString()})',
            color1: item.color1,
            color2: item.color2,
            onPress: () async {},
          ),
        ))
    .toList();

class URLS {
  static const String BASE_URL =
      'http://bvc.grupoleoncito.com.pe/api/'; //'http://192.168.159.128:8000/api/';//

  Map<String, String> headers = {"Content-type": "application/json"};
  Future<Response> postData(String url, String json, bool sesion) async {
    return await post(BASE_URL + (sesion ? 'v1/' : 'v0/') + url,
        headers: headers, body: json);
  }

  Future<Response> getData(String url, String parametros, String token) async {
    return await get(BASE_URL + 'v1/' + url + '/?' + parametros, headers: {
      HttpHeaders.authorizationHeader: "Bearer " + token,
      HttpHeaders.contentTypeHeader: "application/json",
      'Accept': 'application/json'
    });
  }
}

class Alertas {
  final String titulo;
  final String mensaje;

  const Alertas({
    @required this.titulo,
    @required this.mensaje,
  });
  showAlertDialog(BuildContext context) {
    // set up the button
    Widget okButton = FlatButton(
      child: Text("Aceptar"),
      onPressed: () => Navigator.pop(context, false),
    );

    // set up the AlertDialog
    AlertDialog alert = AlertDialog(
      title: Text(this.titulo),
      content: Text(
        this.mensaje,
        style: TextStyle(fontSize: 20),
      ),
      actions: [
        okButton,
      ],
    );

    // show the dialog
    showDialog(
      context: context,
      builder: (BuildContext context) {
        return alert;
      },
    );
  }
}

class Sesion {
  static final Sesion _instancia = new Sesion._internal();

  factory Sesion() {
    return _instancia;
  }

  Sesion._internal();

  SharedPreferences _prefs;

  initPrefs() async {
    this._prefs = await SharedPreferences.getInstance();
  }

  // GET y SET sincroniza
  get sincroniza {
    return _prefs.getBool('sincroniza') ?? false;
  }

  set sincroniza(bool value) {
    _prefs.setBool('sincroniza', value);
  }

  // GET y SET sesion
  get sesion {
    return _prefs.getBool('sesion') ?? false;
  }

  set sesion(bool value) {
    _prefs.setBool('sesion', value);
  }

  // GET y SET datosevaluar
  get datosevaluar {
    return _prefs.getBool('datosevaluar') ?? true;
  }

  set datosevaluar(bool value) {
    _prefs.setBool('datosevaluar', value);
  }

  // GET y SET clienteencontrado
  get clienteencontrado {
    return _prefs.getBool('clienteencontrado') ?? false;
  }

  set clienteencontrado(bool value) {
    _prefs.setBool('clienteencontrado', value);
  }

  // GET y SET datoscliente
  get datoscliente {
    return _prefs.getString('datoscliente') ?? '0';
  }

  set datoscliente(String value) {
    _prefs.setString('datoscliente', value);
  }

  // GET y SET dnicliente
  get dnicliente {
    return _prefs.getString('dnicliente') ?? '';
  }

  set dnicliente(String value) {
    _prefs.setString('dnicliente', value);
  }

// GET y SET sesionlocal
  get sesionLocal {
    return _prefs.getBool('showlocal') ?? false;
  }

  set sesionLocal(bool value) {
    _prefs.setBool('showlocal', value);
  }

  // GET y SET pendientes
  get pendientes {
    return _prefs.getInt('pendientes') ?? 0;
  }

  set pendientes(int value) {
    _prefs.setInt('pendientes', value);
  }

  // GET y SET usuario
  get usuario {
    return _prefs.getString('usuario') ?? '0';
  }

  set usuario(String value) {
    _prefs.setString('usuario', value);
  }

  // GET y SET local
  get local {
    return _prefs.getInt('local') ?? 0;
  }

  set local(int value) {
    _prefs.setInt('local', value);
  }

  // GET y SET token
  get token {
    return _prefs.getString('token') ?? '';
  }

  set token(String value) {
    _prefs.setString('token', value);
  }

  get botonSesion {
    return _prefs.getString('botonsesion') ?? 'Iniciar sesión';
  }

  set botonSesion(String value) {
    _prefs.setString('botonsesion', value);
  }

  cerrarsesion(BuildContext context) async {
    _prefs.setString('token', "");
    _prefs.setBool('sesionLocal', false);
    _prefs.setString('usuario', "");
    _prefs.setInt('local', 0);
    _prefs.setBool('sesion', false);
    locales.clear();
    locales.add(new Locales(0, 'Seleccione local'));
    var usuarioaux = await db.fetchUsuario();
    prefs.toastmessage("${usuarioaux.nombre} ha finalizado la sesión.",
        Colors.black87, Colors.white);
    Navigator.pushReplacementNamed(context, 'login');
  }

  datospdf(int id) async {
    cotizaciondata = new Cotizacion();
    clienteauxdata = new Cliente();
    detallecotizacionData = new List<DetalleCotizacion>();
    cronogramaData = new List<Cronograma>();

    cotizaciondata = await db.getCotizacion(id);
    clienteauxdata = await db.getCliente(cotizaciondata.dni);
    detallecotizacionData = await db.fetchDetalleCotizacion(id);
    cronogramaData = await db.fetchCronograma(id);
  }

  limpiarcronograma(bool ban) {
    preciolista = 0.0;
    descuento = 0.0;
    precioreal = 0.0;
    porcentajeinicial = 0.0;
    plazoinicial = 0;
    pagoinicial = 0.0;
    prestamo = 0.0;
    totalcronograma = 0.0;
    totalventa = 0.0;
    cronogramaData = new List<Cronograma>();
    isCotiza = ban;
  }

  limpiarcotizacion() {
    valueguardar = 100;
    valuevivienda = 0;
    valuedepartamento = 0;
    valueprovincia = 0;
    valuedistrito = 0;
    valuecentropoblado = 0;
    valuecuotas = 0;
    valuecanal = 0;
    isCombo = false;
    isDescuento = false;
    isAmpliarCta = false;
    isCotiza = false;
    dataProducto = new List<Producto>();
    dataSelProducto = new List<Producto>();
    selectedProductos = new List<Producto>();
    limpiarcronograma(false);
  }

  bool validarparametroscotizacion() {
    alignaux = Alignment.center;
    isCotiza = false;
    bool state = true;

    if (dataSelProducto.length == 0) {
      toastmessage("Seleccione productos.", Colors.redAccent, Colors.white);
      state = false;
    } else if (valuevivienda == 0) {
      toastmessage("Seleccione tipo vivienda.", Colors.redAccent, Colors.white);
      state = false;
    } else if (valuedepartamento == 0) {
      toastmessage("Seleccione departamento.", Colors.redAccent, Colors.white);
      state = false;
    } else if (valueprovincia == 0) {
      toastmessage("Seleccione provincia.", Colors.redAccent, Colors.white);
      state = false;
    } else if (valuedistrito == 0) {
      toastmessage("Seleccione distrito.", Colors.redAccent, Colors.white);
      state = false;
    } else if (cbocentropoblado.length > 0) {
      if (valuecentropoblado == 0) {
        toastmessage(
            "Seleccione centro poblado.", Colors.redAccent, Colors.white);
        state = false;
      }
    }
    if (state) {
      if (valuecuotas == 0) {
        toastmessage("Seleccione cuotas.", Colors.redAccent, Colors.white);
        state = false;
      }
    }
    return state;
  }

  bool validarregistro() {
    bool state = true;
    if (valueguardar == 100) {
      toastmessage(
          "Seleccione tipo de registro.", Colors.redAccent, Colors.white);
      state = false;
    } else {
      if (valuecanal == 0) {
        toastmessage(
            "Seleccione canal de captura.", Colors.redAccent, Colors.white);
        state = false;
      } else {
        if (valueguardar == 0) {
          if (direccionCtrl.trim().isEmpty) {
            toastmessage("Ingrese dirección.", Colors.redAccent, Colors.white);
            state = false;
          } else if (telefonoCtrl.trim().isEmpty) {
            toastmessage(
                "Ingrese teléfono o celular.", Colors.redAccent, Colors.white);
            state = false;
          }
        }
      }
    }
    return state;
  }

  vercronograma(int cuotas, double prestamo, double montocuota, double tem,
      double fctotal, double fctem, double igv, double inicial) {
    try {
      totalcronograma = 0;
      cronogramaData = new List<Cronograma>();
      double saldo = prestamo;
      double interes = 0.0;
      double amortizacion = 0.0;
      double gastocobranza = (prestamo * fctotal) - (prestamo * fctem);
      double montoigv = 0.0;
      final cronogramaux = Cronograma(
        id: 0,
        nrocuota: 0,
        amortizacion: inicial,
        interes: 0.00,
        gasto: 0.00,
        monto: 0.00,
        igv: 0.00,
        saldo: prestamo,
        facturar: inicial,
        idcotizacion: 0,
      );
      cronogramaData.add(cronogramaux);
      for (int i = 0; i < cuotas; i++) {
        interes = (saldo * tem);
        amortizacion = (montocuota - interes - gastocobranza);
        if (i == cuotas - 1) {
          amortizacion = saldo;
        }
        saldo = saldo - amortizacion;
        montoigv = (interes + gastocobranza) * igv;
        if (i == cuotas - 1) {
          saldo = 0;
        }
        final cronogramaux = Cronograma(
          id: 0,
          nrocuota: (i + 1),
          amortizacion: amortizacion,
          interes: interes,
          gasto: gastocobranza,
          monto: montocuota,
          igv: montoigv,
          saldo: saldo,
          facturar: (amortizacion + interes + gastocobranza + montoigv),
          idcotizacion: 0,
        );
        totalcronograma += cronogramaux.facturar;
        cronogramaData.add(cronogramaux);
      }

      totalventa = totalcronograma + inicial;
    } catch (e_) {
      toastmessage(
          "Error al validar cronograma...", Colors.redAccent, Colors.white);
    }
  }

  obtenertcem(List<double> cuotasarray, double inversion) {
    List<double> auxcuotas = cuotasarray;
    double auxf = 30;
    double auxa = 0;
    double auxb = 2 * auxf / 360;
    double auxtcem = 0;
    for (int h = 0; h <= 1000; h++) {
      double valc = 0.00000000000;
      double auxc = 0.0;
      auxc = (auxa + auxb) / 2;
      for (int i = 1; i <= auxcuotas.length; i++) {
        valc = valc + auxcuotas[i - 1] / pow((1 + auxc), i);
      }
      if (valc < inversion) {
        auxb = auxc;
      } else {
        auxa = auxc;
      }
      if ((valc - inversion).abs() < 0.001) {
        auxtcem = auxc;
        h = 1000;
      }
    }
    return auxtcem;
  }

  logueonuevo() async {
    Usuario usuarioaux = await db.fetchUsuario();
    loginapi(usuarioaux.usuario, usuarioaux.clave);
    prefs.token = usuarioaux.token;
    prefs.usuario = jsonEncode({
      "usuario": usuarioaux.usuario,
      "nombre": usuarioaux.nombre,
      "cargo": usuarioaux.cargo
    });
  }

  enviarpendientescotizaciones(BuildContext context) async {
    var stateaux = await conectnet();
    if (!stateaux) {
      prefs.toastmessage("Verifique su conexión a internet.", Colors.black87,
          Colors.yellow[700]);
      return false;
    }
    logueonuevo();
    List<Cliente> clienvo = await db.fetchClienteNuevos();
    List<Cotizacion> dataax = await db.fetchCotizacion(true);
    prefs.pendientes = dataax.length;
    if (prefs.pendientes == 0) {
      if (clienvo.length == 0) {
        prefs.toastmessage(
            "No tiene registros por enviar al sistema de cotización.",
            Colors.black87,
            Colors.yellow[700]);
        return false;
      } else {
        prefs.toastmessage(
            "No ha realizado cotizaciones pero tiene contactos nuevos por evaluar.",
            Colors.black87,
            Colors.yellow[700]);
        prefs.toastmessage(
            "Enviado contactos nuevos...", Colors.black87, Colors.yellow[700]);

        prefs.enviarcontactos();
        prefs.toastmessage(
            "Sincronización finalizada.", Colors.greenAccent, Colors.black);
      }
    } else {
      if (clienvo.length > 0) {
        prefs.toastmessage(
            "Enviado contactos nuevos...", Colors.black87, Colors.yellow[700]);
        prefs.enviarcontactos();
      }
      prefs.toastmessage(
          "Enviado cotizaciones.", Colors.black87, Colors.yellow[700]);
      (await db.fetchCotizacion(true)).forEach((element) async {
        prefs.enviarcotizacionapi(element.id);
      });
      List<Cotizacion> dataax = await db.fetchCotizacion(true);
      prefs.pendientes = dataax.length;
      prefs.toastmessage(
          "Sincronización finalizada.", Colors.greenAccent, Colors.black);
      Navigator.pushReplacementNamed(context, 'dashboard');
    }
  }

  enviarcotizacionapi(int idCotizacion) async {
    Usuario auxUser = await db.fetchUsuario();
    Cotizacion element = await db.getCotizacion(idCotizacion);
    String detcot = jsonEncode((await db.fetchDetalleCotizacion(element.id))
        .map((e) => e.toMap())
        .toList());
    String cronog = jsonEncode(
        (await db.fetchCronograma(element.id)).map((e) => e.toMap()).toList());

    var data = jsonEncode({
      "combo": element.combo,
      "edad": element.edad,
      "dni": element.dni,
      "idvivienda": element.idvivienda.toString(),
      "idsituacionlaboral": "0",
      "idestabilidadlaboral": "0",
      "idgiroactividad": "0",
      "idzona": element.idzona,
      "cuotas": element.cuotas,
      "excepcion": element.excepcion,
      "porcentajeinicial": element.porcentajeinicial,
      "plazoinicial": element.plazoinicial,
      "pagoinicial": element.pagoinicial,
      "preciocompra": element.preciocompra,
      "descuentopromo": element.descuentopromo,
      "precioreal": element.precioreal,
      "montocuota": element.montocuota,
      "montocredito": element.montocredito,
      "totalcredito": element.totalcredito,
      "observacion": element.observacion,
      "negociacion": element.negociacion,
      "idresultado": element.idresultado,
      "idcontacto": element.idcontacto,
      "idpersonalregistro": auxUser.id,
      "asesorvta": auxUser.id,
      "proceso": element.proceso,
      "direccion": element.direccion,
      "telefono": element.telefono,
      "codigoubigeo": element.codigoubigeo,
      "detallecotizacion": detcot,
      "cronograma": cronog,
      "factor": element.factor,
      "factortem": element.factortem,
      "tea": element.tea,
      "tem": element.tem,
      "tceaimp": element.tceaimp,
      "tcemimp": element.tcemimp,
      "tcea": element.tcea,
      "tcem": element.tcem,
      "igv": element.igv,
      "inicialorig": element.inicialorig,
      "token": auxUser.token,
      "ws": "0",
      "fecharegistro": element.fecharegistro,
      "medio": element.medio,
    });

    Response response =
        await URLS().postData('guardar-cotizacion-sync', data, true);
    var datareg = jsonDecode(response.body);
    if (datareg['estado'] == true) {
      await db.updateCotizacionSync(idCotizacion);
    } else {
      prefs.toastmessage(
          datareg['descripcion'], Colors.redAccent, Colors.white);
      return false;
    }
  }

  enviarcontactos() async {
    Usuario auxUser = await db.fetchUsuario();
    (await db.fetchClienteNuevos()).forEach((element) async {
      var dataclie = jsonEncode({
        "dni": element.dni,
        "tipodoc": "D",
        "fechanacimiento": element.fechanacimiento,
        "nombre": element.nombres,
        "paterno": element.apellidopaterno,
        "materno": element.apellidomaterno,
        "nombrecompleto":
            "${element.apellidopaterno} ${element.apellidomaterno} ${element.nombres}",
        "personalregistro": auxUser.id,
        "token": auxUser.token
      });
      Response response =
          await URLS().postData('evaluar-cliente-sync', dataclie, true);
      var datareg = jsonDecode(response.body);
      if (datareg['estado'] == true) {
        await db.updateClienteSync(element.dni);
      } else {
        prefs.toastmessage(
            datareg['descripcion'], Colors.redAccent, Colors.white);
        return false;
      }
    });
  }

  toastmessage(String msj, Color color, Color colortext) {
    Fluttertoast.showToast(
        msg: msj,
        toastLength: Toast.LENGTH_SHORT,
        gravity: ToastGravity.BOTTOM,
        timeInSecForIosWeb: 7,
        backgroundColor: color,
        textColor: colortext,
        fontSize: 20.0);
  }

  Future<List<Afectaciones>> cargarcuotas(bool excepcion, bool combo) async {
    List<Afectaciones> dataCat = await db.fetchCuotas(excepcion, combo);
    return dataCat;
  }

  Future<List<Canal>> cargarcanal() async {
    List<Canal> dataCat = await db.fetchCanal();
    return dataCat;
  }

  Future<String> getFilePath(String nombrearchivo) async {
    Directory appDocumentsDirectory = await getApplicationDocumentsDirectory();
    String appDocumentsPath = appDocumentsDirectory.path;
    String filePath = '$appDocumentsPath/$nombrearchivo.txt';
    return filePath;
  }

  void saveFile(String nombrearchivo, String contenido) async {
    File file = File(await getFilePath(nombrearchivo));
    file.writeAsString(contenido);
  }

  Future<String> readFile(String nombrearchivo) async {
    File file = File(await getFilePath(nombrearchivo));
    String fileContent = await file.readAsString();
    return fileContent;
  }

  Future<Cliente> obtenercliente(String dni) async {
    JsonCodec codec = new JsonCodec();
    String contenido = await prefs.readFile("cliente");
    final maps = codec.decode(contenido);
    List.generate(maps.length, (i) {
      //create a list of memos
      var datosclie = utf8
          .decode(base64.decode((base64.normalize(maps[i].toString()))))
          .split('|');
      if (datosclie[2].toString() == dni) {
        return Cliente(
          id: int.parse(datosclie[0].toString()),
          referenciaorigen: int.parse(datosclie[1].toString()),
          dni: datosclie[2].toString(),
          ruc: datosclie[3].toString(),
          apellidopaterno: datosclie[4].toString(),
          apellidomaterno: datosclie[5].toString(),
          nombres: datosclie[6].toString(),
          fechanacimiento: datosclie[7].toString(),
          direccion: datosclie[8].toString(),
          telefono: datosclie[9].toString(),
          idevaleo: int.parse(datosclie[10].toString()),
          idevasen: int.parse(datosclie[12].toString()),
          asignacioncme: int.parse(datosclie[11].toString()),
          cmeasignada: double.parse(datosclie[13].toString()),
          cmedisponible: double.parse(datosclie[14].toString()),
          lineacredito: double.parse(datosclie[15].toString()),
          lineadisponible: double.parse(datosclie[16].toString()),
          idresultado: int.parse(datosclie[17].toString()),
          resultado: datosclie[18].toString(),
          excepcion: int.parse(datosclie[19].toString()),
          motivoexcepcion: datosclie[20].toString(),
          idvigencia: int.parse(datosclie[21].toString()),
          vigencia: datosclie[22].toString(),
          idrecurrencia: int.parse(datosclie[23].toString()),
          recurrencia: datosclie[24].toString(),
          idestado: int.parse(datosclie[25].toString()),
          estado: datosclie[26].toString(),
          idbancarizacion: int.parse(datosclie[27].toString()),
          bancarizacion: datosclie[28].toString(),
          idnivelriesgo: int.parse(datosclie[29].toString()),
          nivelriesgo: datosclie[30].toString(),
          registrado: int.parse(datosclie[31].toString()),
        );
      }
    });
    return null;
  }

  Future<bool> sincronizardatos(BuildContext context) async {
    bool state = false;
    try {
      var stateaux = await conectnet();
      if (!stateaux) {
        prefs.toastmessage(
            "Verifique su conexión a internet y vuelve a sincronizar.",
            Colors.black,
            Colors.white);
        return false;
      }

      if (prefs.pendientes == 0) {
        List<Cliente> clienvo = await db.fetchClienteNuevos();
        logueonuevo();
        if (clienvo.length > 0) {
          toastmessage(
              "No tiene cotizaciones por enviar pero tiene contactos nuevos que deben ser enviadas a evaluar.",
              Colors.redAccent,
              Colors.white);
          return false;
        }
        isLoading = true;
        toastmessage("Iniciando sincronización...", Colors.black, Colors.white);

        JsonCodec codec = new JsonCodec();
        Response response = await URLS().getData(
            'sincronizar-app', 'pendientes=${prefs.pendientes}', prefs.token);
        var jsonaux = codec.decode(response.body.toString());

        // DATA CANAL
        toastmessage("Cargando parametros de configuración...",
            Colors.greenAccent, Colors.black);
        List listCanal = jsonaux['canal'] as List;
        await db.deleteCanal();
        listCanal.forEach((element) async {
          final mdCanal = Canal(
              id: int.parse(element['id'].toString()),
              descripcion: element['descripcion'].toString(),
              tipo: int.parse(element['tipo'].toString()));
          await db.insertCanal(mdCanal);
        });
        // NIVEL RIESGO
        List listNivelRiesgo = jsonaux['nivelriesgo'] as List;
        await db.deleteNivelRiesgo();
        listNivelRiesgo.forEach((element) async {
          final mdNivelRiesgo = NivelRiesgo(
              id: int.parse(element['id'].toString()),
              descripcion: element['descripcion'].toString());
          await db.insertNivelRiesgo(mdNivelRiesgo);
        });

        // RECURRENCIA
        List listRecurrencia = jsonaux['recurrencia'] as List;
        await db.deleteRecurrencia();
        listRecurrencia.forEach((element) async {
          final mdRecurrencia = Recurrencia(
              id: int.parse(element['id'].toString()),
              descripcion: element['descripcion'].toString());
          await db.insertRecurrencia(mdRecurrencia);
        });

        // VIGENCIA
        List listVigencia = jsonaux['vigencia'] as List;
        await db.deleteVigencia();
        listVigencia.forEach((element) async {
          final mdVigencia = Vigencia(
              id: int.parse(element['id'].toString()),
              descripcion: element['descripcion'].toString());
          await db.insertVigencia(mdVigencia);
        });

        // ESTADO CLIENTE
        List listEstado = jsonaux['estadoclie'] as List;
        await db.deleteEstado();
        listEstado.forEach((element) async {
          final mdEstado = Estado(
              id: int.parse(element['id'].toString()),
              descripcion: element['descripcion'].toString());
          await db.insertEstado(mdEstado);
        });

        // INICIALES MOTO
        List listInicialesMoto = jsonaux['inicialesmoto'] as List;
        await db.deleteInicialesMoto();
        listInicialesMoto.forEach((element) async {
          final mdIniciales = InicialesMoto(
            id: int.parse(element['id'].toString()),
            montoinicial: double.parse(element['montoinicial'].toString()),
            idrecurrencia: int.parse(element['idrecurrencia'].toString()),
            tipo: int.parse(element['tipo'].toString()),
            vigencia: int.parse(element['vigencia'].toString()),
          );
          await db.insertInicialesMoto(mdIniciales);
        });
        // INICIALES RECURRENTE
        List listInicialesRecurrencia = jsonaux['inicialesrecurrente'] as List;
        await db.deleteInicialesRecurrente();
        listInicialesRecurrencia.forEach((element) async {
          final mdIniciales = InicialesRecurrente(
            id: int.parse(element['id'].toString()),
            idvigencia: int.parse(element['idrecurrencia'].toString()),
            idrecurrencia: int.parse(element['tipo'].toString()),
            inicial: double.parse(element['montoinicial'].toString()),
            vigencia: int.parse(element['vigencia'].toString()),
          );
          await db.insertInicialesRecurrente(mdIniciales);
        });

        //  AFECTACIONES
        List listAfectaciones = jsonaux['afectaciones'] as List;
        await db.deleteAfectaciones();
        listAfectaciones.forEach((element) async {
          final mdAfectaciones = Afectaciones(
              id: int.parse(element['id'].toString()),
              descripcion: element['descripcion'].toString(),
              porcentaje: double.parse(element['porcentaje'].toString()),
              idtipoafectacion:
                  int.parse(element['idtipoafectacion'].toString()),
              tipoafectacion: element['tipoafectacion'].toString(),
              vigencia: int.parse(element['vigencia'].toString()));

          await db.insertAfectaciones(mdAfectaciones);
        });

        // TOTAL COTIZACIONES
        List listTotal = jsonaux['totalcotizaciones'] as List;
        await db.deleteTotal();
        if (listTotal.length > 0) {
          listTotal.forEach((element) async {
            final mdTotal = TotalCotizacion(
                fecha: element['fecha'].toString(),
                cantidad: int.parse(element['cantidad'].toString()),
                codigolocal: int.parse(element['codigolocal'].toString()));
            await db.insertTotalCotizacion(mdTotal);
          });
        }
        // DATA PARAMETROS COTIZACIÓN
        List listParametrosCotizacion = jsonaux['parametroscotizacion'] as List;
        await db.deleteParametrosCotizacion();
        listParametrosCotizacion.forEach((element) async {
          final mdParametrosCotizacion = ParametrosCotizacion(
              id: element['id'],
              nivelriesgo: element['nivelriesgo'],
              tipovivienda: element['tipovivienda'],
              edadminima: element['edadminima'],
              edadmaxima: element['edadmaxima'],
              situacionlaboral: element['situacionlaboral'],
              estabilidadlaboral: element['estabilidadlaboral'],
              porcentajeinicial: double.parse(element['porcentajeinicial']),
              plazoinicial: element['plazoinicial'],
              observacion: element['observacion'],
              tipocotizacion: element['tipocotizacion'],
              giroactividad: element['giroactividad']);
          await db.insertParametrosCotizacion(mdParametrosCotizacion);
        });

        // DATA CATEGORIAS
        List<Categoria> dataCategoria = new List<Categoria>();
        toastmessage(
            "Cargando categorías...", Colors.greenAccent, Colors.black);
        await db.deleteCategoria();
        response = await URLS()
            .getData('sincronizarcategoria-app', 'row=0', prefs.token);
        var jsonauxcat = codec.decode(response.body);
        for (int i = 0; i < jsonauxcat.length; i++) {
          dataCategoria = new List<Categoria>();
          jsonauxcat[i].forEach((element) async {
            // var datoscat = utf8.decode(base64.decode((element.toString()))).split('|');
            var datoscat = element.split('|');
            final mdCategoria = Categoria(
                id: int.parse(datoscat[0].toString()),
                codigo: datoscat[1].toString(),
                descripcion: datoscat[2].toString(),
                categoria: datoscat[3].toString(),
                superior: int.parse(datoscat[4].toString()));
            dataCategoria.add(mdCategoria);
          });
          await db.insertCategoriaBatch(dataCategoria);
        }

        // DATA CENTROS POBLADOS
        List<CentroPoblado> dataCentroPoblado = new List<CentroPoblado>();
        toastmessage("Cargando ubigeo...", Colors.greenAccent, Colors.black);
        await db.deleteCentroPoblado();

        response = await URLS()
            .getData('sincronizarcentropoblado-app', 'row=0', prefs.token);
        var jsonauxubig = codec.decode(response.body);
        for (int i = 0; i < jsonauxubig.length; i++) {
          dataCentroPoblado = new List<CentroPoblado>();
          jsonauxubig[i].forEach((element) async {
            // var datosubig = utf8.decode(base64.decode((base64.normalize(element.toString())))).split('|');
            var datosubig = element.split('|');
            final mdUbigeo = CentroPoblado(
                id: int.parse(datosubig[0].toString()),
                descripcion: datosubig[1].toString(),
                idsegmentacion: int.parse(datosubig[2].toString()),
                iddistrito: int.parse(datosubig[3].toString()));
            dataCentroPoblado.add(mdUbigeo);
          });
          await db.insertCentroPobladoBatch(dataCentroPoblado);
          // await db.insertCentroPoblado(mdUbigeo);
        }

        // DATA CLIENTES
        toastmessage("Cargando clientes...", Colors.greenAccent, Colors.black);
        await db.deleteCliente();
        response = await URLS()
            .getData('sincronizarclientes-app', 'row=500}', prefs.token);
        var jsonauxclie = codec.decode(response.body);
        for (int i = 0; i < jsonauxclie.length; i++) {
          dataCliente = new List<Cliente>();
          jsonauxclie[i].forEach((element) async {
            // var datosclie = utf8.decode(base64.decode((base64.normalize(element.toString())))).split('|');
            var datosclie = element.split('|');
            final mdCliente = Cliente(
              id: int.parse(datosclie[0].toString()),
              referenciaorigen: int.parse(datosclie[1].toString()),
              dni: datosclie[2].toString(),
              ruc: datosclie[3].toString(),
              apellidopaterno: datosclie[4].toString(),
              apellidomaterno: datosclie[5].toString(),
              nombres: datosclie[6].toString().replaceAll("'", ""),
              fechanacimiento: datosclie[7].toString(),
              direccion: datosclie[8].toString(),
              telefono: datosclie[9].toString(),
              idevaleo: int.parse(datosclie[10].toString()),
              idevasen: int.parse(datosclie[12].toString()),
              asignacioncme: int.parse(datosclie[11].toString()),
              cmeasignada:
                  double.parse(double.parse(datosclie[13]).toStringAsFixed(2)),
              cmedisponible:
                  double.parse(double.parse(datosclie[14]).toStringAsFixed(2)),
              lineacredito:
                  double.parse(double.parse(datosclie[15]).toStringAsFixed(2)),
              lineadisponible:
                  double.parse(double.parse(datosclie[16]).toStringAsFixed(2)),
              idresultado: int.parse(datosclie[17].toString()),
              resultado: datosclie[18].toString(),
              excepcion: int.parse(datosclie[19].toString()),
              motivoexcepcion: datosclie[20].toString(),
              idvigencia: int.parse(datosclie[21].toString()),
              vigencia: datosclie[22].toString(),
              idrecurrencia: int.parse(datosclie[23].toString()),
              recurrencia: datosclie[24].toString(),
              idestado: int.parse(datosclie[25].toString()),
              estado: datosclie[26].toString(),
              idbancarizacion: int.parse(datosclie[27].toString()),
              bancarizacion: datosclie[28].toString(),
              idnivelriesgo: int.parse(datosclie[29].toString()),
              nivelriesgo: datosclie[30].toString(),
              fecharegistro: datosclie[31].toString(),
              registrado: int.parse(datosclie[32].toString()),
            );
            dataCliente.add(mdCliente);
          });
          await db.insertClienteBatch(dataCliente);
        }
        // DATA PRODUCTOS
        toastmessage("Cargando productos...", Colors.greenAccent, Colors.black);
        await db.deleteProducto();

        response = await URLS()
            .getData('sincronizarproductos-app', 'row=500', prefs.token);
        var jsonauxprod = codec.decode(response.body);
        for (int i = 0; i < jsonauxprod.length; i++) {
          dataProducto = new List<Producto>();
          jsonauxprod[i].forEach((element) async {
            // var datosprod = utf8.decode(base64.decode((base64.normalize(element.toString())))).split('|');
            var datosprod = element.split('|');
            final mdProducto = Producto(
                id: int.parse(datosprod[0].toString()),
                segmentaciongeografica: datosprod[1].toString(),
                cantidad: double.parse(datosprod[2].toString()),
                codigomodelo: int.parse(datosprod[3].toString()),
                nombremodelo: datosprod[4].toString(),
                codigomarca: int.parse(datosprod[5].toString()),
                nombremarca: datosprod[6].toString(),
                codigolinea: int.parse(datosprod[7].toString()),
                nombrelinea: datosprod[8].toString(),
                precio: double.parse(datosprod[9].toString()),
                regalo: int.parse(datosprod[10].toString()),
                tipocotizacion: datosprod[11].toString(),
                lineadescuento: int.parse(datosprod[12].toString()),
                dsctonivel: double.parse(datosprod[13].toString()),
                dsctosegmento: double.parse(datosprod[14].toString()),
                dsctolinea: double.parse(datosprod[15].toString()),
                dsctoplazo: double.parse(datosprod[16].toString()),
                dsctoexcepcion: double.parse(datosprod[17].toString()),
                dsctocombo: double.parse(datosprod[18].toString()),
                dsctototal: double.parse(datosprod[19].toString()),
                preciototal: double.parse(datosprod[20].toString()),
                preciodscto: double.parse(datosprod[21].toString()),
                precioreal: double.parse(datosprod[22].toString()),
                vigencia: int.parse(datosprod[23].toString()),
                idproductoregalo: int.parse(datosprod[24].toString()),
                regalocant: double.parse(datosprod[25].toString()),
                detalledscto: datosprod[26].toString());
            dataProducto.add(mdProducto);
          });

          await db.insertProductosBatch(dataProducto);
        }
        state = true;
      } else {
        state = false;
        toastmessage(
            "Para sincronizar debe enviar todos sus registros pendientes",
            Colors.redAccent,
            Colors.white);
        await db.deleteCliente();
        return false;
      }
      state = true;
    } catch (e, _) {
      state = false;
      await db.updateUsuarioSync(0);
      isLoading = false;
      toastmessage("Hubo problemas al sincronizar. Vuelva a intentarlo.",
          Colors.redAccent, Colors.white);
      debugPrint(e.toString());
    }
    return state;
  }
}

class ItemBoton {
  final IconData icon;
  final String texto;
  final Color color1;
  final Color color2;

  ItemBoton(this.icon, this.texto, this.color1, this.color2);
}

class Locales {
  final int codigo;
  final String descripcion;

  Locales(this.codigo, this.descripcion);
}

class TipoCotizacion {
  final String codigo;
  final String descripcion;

  TipoCotizacion(this.codigo, this.descripcion);
}

class Combos {
  final int codigo;
  final String descripcion;

  Combos(this.codigo, this.descripcion);
}

class BotonDashboard extends StatelessWidget {
  final IconData icon;
  final String texto;
  final Color color1;
  final Color color2;
  final Function onPress;

  BotonDashboard(
      {this.icon = FontAwesomeIcons.circle,
      this.texto,
      this.color1 = Colors.grey,
      this.color2 = Colors.blueGrey,
      this.onPress});

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () async {
        try {
          this.onPress();
        } catch (e, _) {
          prefs.toastmessage(e.toString(), Colors.redAccent, Colors.white);
          debugPrint(e.toString());
        }
      },
      child: Stack(
        children: <Widget>[
          _BotonDashboardBackground(this.icon, this.color1, this.color2),
          Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              SizedBox(height: 140, width: 60),
              FaIcon(this.icon, color: Colors.white, size: 40),
              SizedBox(width: 20),
              Expanded(
                  child: Text(this.texto,
                      style: TextStyle(color: Colors.white, fontSize: 24))),
              FaIcon(FontAwesomeIcons.chevronRight, color: Colors.white),
              SizedBox(width: 50),
            ],
          )
        ],
      ),
    );
  }

  verpendientes() async {
    List<Cotizacion> dataax = await db.fetchCotizacion(true);
    prefs.pendientes = dataax.length;
  }
}

class _BotonDashboardBackground extends StatelessWidget {
  final IconData icon;
  final Color color1;
  final Color color2;

  const _BotonDashboardBackground(this.icon, this.color1, this.color2);

  @override
  Widget build(BuildContext context) {
    return Container(
      child: ClipRRect(
        borderRadius: BorderRadius.circular(15),
        child: Stack(
          children: <Widget>[
            Positioned(
                right: -10,
                top: -10,
                child: FaIcon(this.icon,
                    size: 150, color: Colors.white.withOpacity(0.2)))
          ],
        ),
      ),
      width: double.infinity,
      height: 100,
      margin: EdgeInsets.all(20),
      decoration: BoxDecoration(
          boxShadow: <BoxShadow>[
            BoxShadow(
                color: Colors.black.withOpacity(0.2),
                offset: Offset(4, 6),
                blurRadius: 10),
          ],
          borderRadius: BorderRadius.circular(15),
          gradient: LinearGradient(colors: <Color>[
            this.color1,
            this.color2,
          ])),
    );
  }
}

class TextForm extends StatelessWidget {
  final IconData icon;
  final dynamic item;
  final bool show;
  const TextForm(
      {@required this.show, @required this.icon, @required this.item});
  @override
  Widget build(BuildContext context) {
    return Visibility(
        visible: show,
        child: Padding(
          padding: EdgeInsets.symmetric(vertical: 5),
          child: Card(
              color: Colors.transparent,
              borderOnForeground: false,
              elevation: 0,
              shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(0),
                side: BorderSide(
                  color: Colors.transparent,
                  width: 0,
                ),
              ),
              child: ListTile(
                  leading: Icon(
                    icon,
                    size: 36,
                    color: Color(0xffA5C3AF),
                  ),
                  title: item)),
        ));
  }
}
