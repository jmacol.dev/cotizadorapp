class Estado{  
  final int id;
  final String descripcion;

  Estado({this.id,this.descripcion});

  Map<String,dynamic> toMap(){ // used when inserting data to the database
    return <String,dynamic>{
      "id" : id,
      "descripcion" : descripcion
    };
  }
}