class ParametrosCotizacion{  
  final int id;
  final int nivelriesgo;
  final String tipovivienda;
  final int edadminima;
  final int edadmaxima;
  final String situacionlaboral;
  final String estabilidadlaboral;
  final double porcentajeinicial;
  final int plazoinicial;
  final String observacion;
  final String tipocotizacion;
  final String giroactividad;

  ParametrosCotizacion({this.id,
  this.nivelriesgo,
  this.tipovivienda,
  this.edadminima,
  this.edadmaxima,
  this.situacionlaboral,
  this.estabilidadlaboral,
  this.porcentajeinicial,
  this.plazoinicial,
  this.observacion,
  this.tipocotizacion,
  this.giroactividad});

  Map<String,dynamic> toMap(){ // used when inserting data to the database
    return <String,dynamic>{
      "id" : id,
      "nivelriesgo" :nivelriesgo,
      "tipovivienda" :tipovivienda,
      "edadminima" :edadminima,
      "edadmaxima" :edadmaxima,
      "situacionlaboral" :situacionlaboral,
      "estabilidadlaboral" :estabilidadlaboral,
      "porcentajeinicial" :porcentajeinicial,
      "plazoinicial" :plazoinicial,
      "observacion" :observacion,
      "tipocotizacion" :tipocotizacion,
      "giroactividad" :giroactividad,
    };
  }
}

