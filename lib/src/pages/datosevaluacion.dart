import 'dart:async';
import 'dart:math';

import 'package:appcotizador/src/db/dbProvider.dart';
import 'package:appcotizador/src/model/afectaciones.dart';
import 'package:appcotizador/src/model/usuario.dart';
import 'package:appcotizador/src/model/categoria.dart';
import 'package:appcotizador/src/model/centropoblado.dart';
import 'package:appcotizador/src/model/cliente.dart';
import 'package:appcotizador/src/model/cotizacion.dart';
import 'package:appcotizador/src/model/cronograma.dart';
import 'package:appcotizador/src/model/detallecotizacion.dart';
import 'package:appcotizador/src/model/producto.dart';
import 'package:appcotizador/src/model/resultadocotizacion.dart';
import 'package:appcotizador/src/pages/cotizacionpdf.dart';
import 'package:appcotizador/src/pages/evaluarcliente.dart';
import 'package:appcotizador/src/widgets/header.dart';
import 'package:flutter/material.dart';
import 'package:flutter/scheduler.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:appcotizador/src/widgets/module.dart';
import 'package:appcotizador/src/widgets/menu.dart';
import 'dart:convert';
import 'package:select_dialog/select_dialog.dart';
import 'package:intl/intl.dart';
import 'package:http/http.dart';

// import 'package:sweetalert/sweetalert.dart';
String mensajeval = "";
bool showmsj = false;

class DatosEvaluacion extends StatefulWidget {
  DatosEvaluacion({Key key}) : super(key: key);
  @override
  DatosEvaluacionPageState createState() => DatosEvaluacionPageState();
}

class DatosEvaluacionPageState extends State<DatosEvaluacion> {
  TextEditingController pagoInicialCtrl = new TextEditingController();

  final globalKey = new GlobalKey<ScaffoldState>();
  final formatter = DateFormat("yyyy-MM-dd");
  var selectedValue = {};
  Cliente clienteaux;
  Producto productoaux;
  int edad;
  var fechanacclie = clienteauxdata.fechanacimiento.split('-');
  int codlocal = 32;
  String valuetipocot = "PRODUCTO ESTANDAR";
  bool sort = false;
  String ex2 = "Seleccione Producto";
  int page = 0;

  @override
  void initState() {
    super.initState();
    pagoInicialCtrl.text = "0.00";
  }

  @override
  Widget build(BuildContext context) {
    alignaux = Alignment.center;
    final prefs = new Sesion();
    final formatter = DateFormat("yyyy-MM-dd");
    direccionCtrl =
        clienteauxdata.direccion == 'NULL' ? '' : clienteauxdata.direccion;
    telefonoCtrl =
        clienteauxdata.telefono == 'NULL' ? '' : clienteauxdata.telefono;
    // if(!prefs.clienteencontrado){
    //   SchedulerBinding.instance.addPostFrameCallback((_) {
    //     Navigator.push(
    //           context,
    //           new MaterialPageRoute(
    //               builder: (context) => EvaluarCliente()));
    //   });
    // }

    if (clienteauxdata.idvigencia == 0 && clienteauxdata.idrecurrencia == 0) {
      isDescuento = true;
    }

    // cbocuotas = [ new Combos( 0, 'CUOTAS' )];
    // Future<List<Afectaciones>> _futureOfListCtas =  prefs.cargarcuotas(false,false);
    // // dataSelProducto = new List<Producto>();
    //  _futureOfListCtas.then((value) => {
    //    value.toList().forEach((element) {
    //      cbocuotas.add(new Combos( int.parse(element.descripcion), element.descripcion));
    //    })

    //  });

    // Future<List<Cliente>> _futureOfList =  clientesData();
    // dataCliente = new List<Cliente>();

    //  _futureOfList.then((value) => {
    //    value.toList().forEach((element) {
    //      dataCliente.add(element);
    //    })

    //  });

    return DefaultTabController(
      length: 3,
      initialIndex: page,
      child: Scaffold(
        appBar: AppBar(
          backgroundColor: Color(0xff084B8A),
          bottom: TabBar(
            labelStyle: TextStyle(fontSize: 18.0),
            tabs: [
              Tab(
                  icon: Icon(FontAwesomeIcons.userTag),
                  text: "Datos Evaluación"),
              Tab(
                  icon: Icon(FontAwesomeIcons.boxTissue),
                  text: "Generar Cotización"),
              Tab(icon: Icon(FontAwesomeIcons.listAlt), text: "Cronograma"),
            ],
          ),
          title: Text('Cotización'),
        ),
        drawer: NavDrawer(),
        body: TabBarView(
          children: [
            Align(
              alignment: Alignment.topRight,
              child: SingleChildScrollView(
                  child: Column(children: <Widget>[
                // Container(
                //   width: double.infinity,
                //   height: 45,

                //   decoration: BoxDecoration(
                //     color: Color(0xff084B8A),
                //   ),
                //   child:Center(
                //     child: Text("DATOS DE CLIENTE",style: new TextStyle(
                //     fontWeight: FontWeight.bold,
                //     fontSize: 24,
                //     color: Colors.white,
                //   ),textAlign: TextAlign.center)),
                // ),

                Card(
                    borderOnForeground: true,
                    shadowColor: Color(0xffB5BECE),
                    elevation: 5,
                    color: Color(0xff9393A3),
                    child: Container(
                      padding: new EdgeInsets.all(10.0),
                      child: Column(
                        mainAxisSize: MainAxisSize.min,
                        children: <Widget>[
                          ListTile(
                            leading: Icon(FontAwesomeIcons.userCheck, size: 50),
                            title: Text(
                              "${clienteauxdata.nombres} ${clienteauxdata.apellidopaterno} ${clienteauxdata.apellidomaterno}",
                              style: new TextStyle(
                                  fontSize: 19,
                                  color: Color(0xff084B8A),
                                  fontWeight: FontWeight.bold),
                              textAlign: TextAlign.start,
                            ),
                            subtitle: Text(
                              "${clienteauxdata.dni} (${edadaux.toString()} años)",
                              style: new TextStyle(
                                fontSize: 18,
                              ),
                              textAlign: TextAlign.start,
                            ),
                          ),
                          Row(
                            mainAxisAlignment: MainAxisAlignment.end,
                            children: <Widget>[
                              Text(
                                "FECHA NACIMIENTO: ",
                                style: new TextStyle(
                                  fontWeight: FontWeight.bold,
                                  fontSize: 18,
                                ),
                                textAlign: TextAlign.start,
                              ),
                              Text(
                                "${fechanacclie[2]}-${fechanacclie[1]}-${fechanacclie[0]}",
                                style: new TextStyle(
                                  fontSize: 18,
                                ),
                                textAlign: TextAlign.start,
                              ),
                              SizedBox(
                                width: 20,
                                height: 30,
                              ),
                            ],
                          ),
                        ],
                      ),
                    )),
                Container(
                  width: double.infinity,
                  padding: new EdgeInsets.all(5.0),
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.start,
                    children: <Widget>[
                      Container(
                        width: double.infinity,
                        height: 45,
                        decoration: BoxDecoration(
                          border: Border.all(color: Color(0xffdc143c)),
                          color: Color(0xffdc143c),
                        ),
                        child: Center(
                            child: Text("RESULTADO EVALUACIÓN",
                                style: new TextStyle(
                                  fontWeight: FontWeight.bold,
                                  fontSize: 24,
                                  color: Colors.white,
                                ),
                                textAlign: TextAlign.center)),
                      ),
                      Container(
                        width: double.infinity,
                        padding: new EdgeInsets.all(10.0),
                        decoration: BoxDecoration(
                          border:
                              Border.all(color: Color(0xffdc143c), width: 5),
                        ),
                        child: Center(
                          child: Text(
                            "${clienteauxdata.resultado}",
                            style: new TextStyle(
                              fontSize: 18,
                            ),
                            maxLines: 10,
                            textAlign: TextAlign.justify,
                          ),
                        ),
                      ),
                    ],
                  ),
                ),

                Container(
                  width: double.infinity,
                  padding: new EdgeInsets.all(5.0),
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.start,
                    children: <Widget>[
                      Container(
                        width: double.infinity,
                        height: 45,
                        decoration: BoxDecoration(
                          border: Border.all(color: Color(0xff148B2E)),
                          color: Color(0xff148B2E),
                        ),
                        child: Center(
                            child: Text("CALIFICACIÓN",
                                style: new TextStyle(
                                  fontWeight: FontWeight.bold,
                                  fontSize: 24,
                                  color: Colors.white,
                                ),
                                textAlign: TextAlign.center)),
                      ),
                      Container(
                        width: double.infinity,
                        padding: new EdgeInsets.all(10.0),
                        decoration: BoxDecoration(
                          border:
                              Border.all(color: Color(0xff148B2E), width: 5),
                        ),
                        child: Column(
                          children: <Widget>[
                            Row(children: <Widget>[
                              Text(
                                "TIPO CLIENTE: ",
                                style: new TextStyle(
                                  fontWeight: FontWeight.bold,
                                  fontSize: 17,
                                ),
                                textAlign: TextAlign.start,
                              ),
                              Text(
                                "${clienteauxdata.recurrencia} ",
                                style: new TextStyle(
                                  fontSize: 17,
                                ),
                                textAlign: TextAlign.end,
                              ),
                              SizedBox(
                                width: 10,
                              ),
                              Text(
                                "${clienteauxdata.estado} ${clienteauxdata.vigencia}",
                                style: new TextStyle(
                                    fontSize: 17, color: Color(0xffdc143c)),
                                textAlign: TextAlign.end,
                              )
                            ]),
                            SizedBox(
                              height: 10,
                            ),
                            Row(children: <Widget>[
                              Text(
                                "CME ASIGNADA: ",
                                style: new TextStyle(
                                  fontWeight: FontWeight.bold,
                                  fontSize: 17,
                                ),
                                textAlign: TextAlign.start,
                              ),
                              Text(
                                "S/ ${clienteauxdata.cmeasignada.toStringAsFixed(2)}",
                                style: new TextStyle(
                                  fontSize: 17,
                                ),
                                textAlign: TextAlign.start,
                              )
                            ]),
                            SizedBox(
                              height: 10,
                            ),
                            Row(children: <Widget>[
                              Text(
                                "CME DISPONIBLE: ",
                                style: new TextStyle(
                                  fontWeight: FontWeight.bold,
                                  fontSize: 17,
                                ),
                                textAlign: TextAlign.start,
                              ),
                              Text(
                                "S/ ${clienteauxdata.cmedisponible.toStringAsFixed(2)}",
                                style: new TextStyle(
                                  fontSize: 17,
                                ),
                                textAlign: TextAlign.start,
                              )
                            ]),
                            SizedBox(
                              height: 10,
                            ),
                            Row(children: <Widget>[
                              Text(
                                "LINEA DE CRÉDITO: ",
                                style: new TextStyle(
                                  fontWeight: FontWeight.bold,
                                  fontSize: 17,
                                ),
                                textAlign: TextAlign.start,
                              ),
                              Text(
                                "S/ ${clienteauxdata.lineacredito.toStringAsFixed(2)}",
                                style: new TextStyle(
                                  fontSize: 17,
                                ),
                                textAlign: TextAlign.start,
                              )
                            ]),
                            SizedBox(
                              height: 5,
                            ),
                            Divider(
                              height: 5,
                            ),
                            SizedBox(
                              height: 5,
                            ),
                            Row(children: <Widget>[
                              Text(
                                "BANCARIZACIÓN: ",
                                style: new TextStyle(
                                  fontWeight: FontWeight.bold,
                                  fontSize: 17,
                                ),
                                textAlign: TextAlign.start,
                              ),
                              Text(
                                "${clienteauxdata.bancarizacion}",
                                style: new TextStyle(
                                  fontSize: 17,
                                ),
                                textAlign: TextAlign.start,
                              )
                            ]),
                            SizedBox(
                              height: 10,
                            ),
                            Row(children: <Widget>[
                              Text(
                                "DEUDA IMPAGA: ",
                                style: new TextStyle(
                                  fontWeight: FontWeight.bold,
                                  fontSize: 17,
                                ),
                                textAlign: TextAlign.start,
                              ),
                              Text(
                                "S/ 0.00",
                                style: new TextStyle(
                                  fontSize: 17,
                                ),
                                textAlign: TextAlign.start,
                              )
                            ]),
                            SizedBox(
                              height: 10,
                            ),
                            Row(children: <Widget>[
                              Text(
                                "NIVEL RIESGO: ",
                                style: new TextStyle(
                                  fontWeight: FontWeight.bold,
                                  fontSize: 17,
                                ),
                                textAlign: TextAlign.start,
                              ),
                              Text(
                                "${clienteauxdata.nivelriesgo}",
                                style: new TextStyle(
                                  fontSize: 17,
                                ),
                                textAlign: TextAlign.start,
                              )
                            ])
                          ],
                        ),
                      ),
                      SizedBox(
                        width: 20,
                        height: 30,
                      ),
                    ],
                  ),
                ),
              ])),
            ),
            Align(
              alignment: Alignment.topRight,
              child: SingleChildScrollView(
                  child: Column(children: <Widget>[
                // Container(
                //   width: double.infinity,
                //   height: 45,

                //   decoration: BoxDecoration(
                //     color: Color(0xff084B8A),
                //   ),
                //   child:Center(
                //     child: Text("DATOS DE CLIENTE",style: new TextStyle(
                //     fontWeight: FontWeight.bold,
                //     fontSize: 24,
                //     color: Colors.white,
                //   ),textAlign: TextAlign.center)),
                // ),
                Container(
                    width: double.infinity,
                    margin: EdgeInsets.only(
                        top: 20, left: 15, right: 15, bottom: 20),
                    alignment: Alignment.topRight,
                    decoration: BoxDecoration(color: Colors.white),
                    child: Column(children: <Widget>[
                      Container(
                        padding: EdgeInsets.all(5.0),
                        child: Align(
                          alignment: Alignment.center,
                          child: Row(children: <Widget>[
                            Switch(
                              activeTrackColor: Colors.lightGreenAccent,
                              activeColor: Colors.green,
                              value: isCombo,
                              onChanged: (value) {
                                setState(() {
                                  isCombo = value;
                                  cuotaslistar();
                                  if (valuetipocot != "PRODUCTO ESTANDAR") {
                                    isCombo = false;
                                  }
                                  alignaux = Alignment.center;
                                  prefs.limpiarcronograma(false);
                                });
                              },
                            ),
                            SizedBox(
                              height: 10.0,
                            ),
                            Text(
                              'Combo',
                              style: TextStyle(
                                  color: Colors.black, fontSize: 20.0),
                            ),
                            SizedBox(
                              width: 20.0,
                            ),
                            Expanded(
                              // child:Text('')
                              child: DropdownButtonFormField(
                                style: new TextStyle(
                                    fontSize: 17, color: Colors.black54),
                                items: tipocotizacion
                                    .toSet()
                                    .toList()
                                    .map((TipoCotizacion tipocot) {
                                  return new DropdownMenuItem(
                                      value: tipocot.codigo,
                                      child: Row(
                                        children: <Widget>[
                                          Text(tipocot.descripcion),
                                        ],
                                      ));
                                }).toList(),
                                onChanged: (newValue) async {
                                  setState(() {
                                    if (dataSelProducto
                                            .where((element) =>
                                                element.codigolinea != 63 &&
                                                element.codigolinea != 18)
                                            .toList()
                                            .length >
                                        0) {
                                      if (newValue != 'PRODUCTO ESTANDAR') {
                                        setState(() {
                                          valuetipocot = 'PRODUCTO ESTANDAR';
                                        });
                                        prefs.toastmessage(
                                            "No puede cambiar TIPO COTIZACIÓN ya que tiene agregado productos.",
                                            Colors.redAccent,
                                            Colors.white);
                                        return false;
                                      }
                                    }
                                    if (dataSelProducto
                                            .where((element) =>
                                                element.codigolinea == 63 ||
                                                element.codigolinea == 18)
                                            .toList()
                                            .length >
                                        0) {
                                      if (newValue == 'PRODUCTO ESTANDAR') {
                                        valuetipocot = 'MOTO';
                                        prefs.toastmessage(
                                            "No puede cambiar TIPO COTIZACIÓN ya que tiene agregado productos.",
                                            Colors.redAccent,
                                            Colors.white);
                                        return false;
                                      }
                                    }
                                    if (valuetipocot != "PRODUCTO ESTANDAR") {
                                      isCombo = false;
                                    }
                                    valuetipocot = newValue;
                                    Future<List<Producto>> _futureOfListProd =
                                        productosData();
                                    dataProducto = new List<Producto>();
                                    // dataSelProducto = new List<Producto>();
                                    _futureOfListProd.then((value) => {
                                          value.toList().forEach((element) {
                                            dataProducto.add(element);
                                          })
                                        });
                                  });
                                },
                                value: valuetipocot,
                              ),
                            ),
                            SizedBox(height: 5.0, width: 20),
                            SizedBox(
                                height: 40.0,
                                width: 50,
                                child: RaisedButton(
                                    color: Colors.green[800],
                                    textColor: Colors.yellow[50],
                                    padding:
                                        EdgeInsets.fromLTRB(10, 10, 10, 10),
                                    splashColor: Colors.grey,
                                    child: Icon(Icons.add),
                                    onPressed: () async {
                                      setState(() {
                                        selectedProductos.clear();
                                      });
                                      SelectDialog.showModal<Producto>(
                                        context,
                                        label: "Lista de productos",
                                        items: dataProducto,
                                        itemBuilder: (BuildContext context,
                                            Producto item, bool isSelected) {
                                          return Container(
                                            decoration: !isSelected
                                                ? null
                                                : BoxDecoration(
                                                    borderRadius:
                                                        BorderRadius.circular(
                                                            5),
                                                    color: Colors.white,
                                                    border: Border.all(
                                                      color: Theme.of(context)
                                                          .primaryColor,
                                                    ),
                                                  ),
                                            child: ListTile(
                                              // leading: CircleAvatar(
                                              //   backgroundImage: NetworkImage(item.avatar),
                                              // ),
                                              selected: isSelected,
                                              title: Text(
                                                  '${item.nombremarca} ${item.nombremodelo}'),
                                              subtitle: Text(item.nombrelinea),
                                            ),
                                          );
                                        },
                                        onFind: (String filter) async =>
                                            await db.filterProductos(
                                                filter, valuetipocot),
                                        onChange: (Producto selected) {
                                          setState(() {
                                            if (dataSelProducto
                                                    .where((element) =>
                                                        element.id ==
                                                        selected.id)
                                                    .toList()
                                                    .length ==
                                                0) {
                                              selected.cantidad = 1;
                                              selected.preciototal =
                                                  double.parse(
                                                      (selected.precio *
                                                              selected.cantidad)
                                                          .toStringAsFixed(2));
                                              selected.precioreal =
                                                  selected.preciototal;
                                              dataSelProducto.add(selected);
                                            } else {
                                              prefs.toastmessage(
                                                  "Producto ya ha sido agregado.",
                                                  Colors.redAccent,
                                                  Colors.white);
                                            }
                                            productoaux = selected;
                                            ex2 = selected.codigomodelo
                                                .toString();
                                          });
                                        },
                                      );
                                    })),
                            SizedBox(height: 5.0, width: 20),
                            SizedBox(
                                height: 40.0,
                                width: 50,
                                child: RaisedButton(
                                    color: Colors.red[600],
                                    textColor: Colors.yellow[50],
                                    padding:
                                        EdgeInsets.fromLTRB(10, 10, 10, 10),
                                    splashColor: Colors.grey,
                                    child: Icon(Icons.delete),
                                    onPressed: () async {
                                      setState(() {
                                        if (selectedProductos.length > 0) {
                                          selectedProductos.forEach((element) {
                                            if (dataSelProducto
                                                .contains(element)) {
                                              dataSelProducto.remove(element);
                                            }
                                          });
                                          selectedProductos.clear();
                                          prefs.limpiarcronograma(false);
                                        } else {
                                          prefs.toastmessage(
                                              "Seleccione productos a eliminar.",
                                              Colors.redAccent,
                                              Colors.white);
                                        }
                                      });
                                    })),
                          ]),
                        ),
                      ),
                      Container(
                          padding: EdgeInsets.all(10.0),
                          // width: MediaQuery.of(context).size.width,
                          width: double.infinity,
                          child: Align(
                              alignment: Alignment.bottomRight,
                              child: Row(
                                  mainAxisSize: MainAxisSize.max,
                                  children: <Widget>[
                                    // Expanded(
                                    //   child: Container(
                                    //   width: MediaQuery.of(context).size.width * 0.8,
                                    //   height: 45,
                                    //   decoration: BoxDecoration(
                                    //     border: Border.all(color: Color(0xffdc143c)),
                                    //     color: Colors.blue[800],
                                    //   ),
                                    //   child:Center(
                                    //     widthFactor: 500,
                                    //     child: Text("PRODUCTOS",style: new TextStyle(
                                    //     fontWeight: FontWeight.bold,
                                    //     fontSize: 24,
                                    //     color: Colors.white,
                                    //   ),textAlign: TextAlign.center)),
                                    // )),
                                    // SizedBox(height: 5.0, width: 10),
                                  ]))),
                      Container(
                        padding: EdgeInsets.all(10.0),
                        child: SingleChildScrollView(
                          scrollDirection: Axis.horizontal,
                          child: DataTable(
                            columns: [
                              DataColumn(
                                  label: Text("CANTIDAD",
                                      style: TextStyle(
                                          fontSize: 18,
                                          fontWeight: FontWeight.bold)),
                                  numeric: true),
                              DataColumn(
                                  label: Text("PRODUCTO",
                                      style: TextStyle(
                                          fontSize: 18,
                                          fontWeight: FontWeight.bold)),
                                  numeric: false),
                              DataColumn(
                                label: Text("PRECIO UNITARIO",
                                    style: TextStyle(
                                        fontSize: 18,
                                        fontWeight: FontWeight.bold)),
                                numeric: true,
                              ),
                              DataColumn(
                                label: Text("PRECIO TOTAL",
                                    style: TextStyle(
                                        fontSize: 18,
                                        fontWeight: FontWeight.bold)),
                                numeric: true,
                              ),
                              DataColumn(
                                label: Text("% DSCTO",
                                    style: TextStyle(
                                        fontSize: 18,
                                        fontWeight: FontWeight.bold)),
                                numeric: true,
                              ),
                              DataColumn(
                                label: Text("DESCUENTO",
                                    style: TextStyle(
                                        fontSize: 18,
                                        fontWeight: FontWeight.bold)),
                                numeric: true,
                              ),
                              DataColumn(
                                label: Text("PRECIO CONTADO",
                                    style: TextStyle(
                                        fontSize: 18,
                                        fontWeight: FontWeight.bold)),
                                numeric: true,
                              ),
                            ],
                            rows: dataSelProducto
                                .map(
                                  (producto) => DataRow(
                                      selected:
                                          selectedProductos.contains(producto),
                                      onSelectChanged: (b) {
                                        onSelectedRow(b, producto);
                                      },
                                      cells: [
                                        DataCell(
                                          TextFormField(
                                            textInputAction:
                                                TextInputAction.done,
                                            keyboardType:
                                                TextInputType.numberWithOptions(
                                                    decimal: true),
                                            initialValue: producto.cantidad
                                                .toStringAsFixed(2),
                                            onChanged: (text) {
                                              setState(() {
                                                producto.cantidad = double
                                                    .parse(double.parse(text)
                                                        .toStringAsFixed(2));
                                                producto.preciototal =
                                                    double.parse((producto
                                                                .precio *
                                                            producto.cantidad)
                                                        .toStringAsFixed(2));
                                                producto.precioreal =
                                                    producto.preciototal;
                                              });
                                            },
                                          ),
                                        ),
                                        DataCell(
                                          Text(
                                              '${producto.nombremarca} ${producto.nombremodelo}',
                                              style: TextStyle(fontSize: 17)),
                                        ),
                                        DataCell(
                                          Text(
                                              'S/ ${producto.precio.toStringAsFixed(2)}',
                                              style: TextStyle(fontSize: 17)),
                                        ),
                                        DataCell(
                                          Text(
                                              'S/ ${producto.preciototal.toStringAsFixed(2)}',
                                              style: TextStyle(fontSize: 17)),
                                        ),
                                        DataCell(
                                          Text(
                                              '${(producto.dsctototal * 100).toStringAsFixed(2)}%',
                                              style: TextStyle(fontSize: 17)),
                                        ),
                                        DataCell(
                                          Text(
                                              'S/ ${producto.preciodscto.toStringAsFixed(2)}',
                                              style: TextStyle(fontSize: 17)),
                                        ),
                                        DataCell(
                                          Text(
                                              'S/ ${producto.precioreal.toStringAsFixed(2)}',
                                              style: TextStyle(fontSize: 17)),
                                        ),
                                      ]),
                                )
                                .toList(),
                          ),
                        ),
                      ),
                      Container(
                        width: double.infinity,
                        height: 45,
                        decoration: BoxDecoration(
                          border: Border.all(color: Color(0xff555B65)),
                          color: Color(0xff555B65),
                        ),
                        child: Center(
                            child: Text("PARÁMETROS DE COTIZACÓN",
                                style: new TextStyle(
                                  fontWeight: FontWeight.bold,
                                  fontSize: 24,
                                  color: Colors.white,
                                ),
                                textAlign: TextAlign.center)),
                      ),
                      Container(
                          width: double.infinity,
                          padding: new EdgeInsets.all(10.0),
                          decoration: BoxDecoration(
                            border:
                                Border.all(color: Color(0xff555B65), width: 5),
                          ),
                          child: Column(children: <Widget>[
                            Container(
                                padding: EdgeInsets.all(5.0),
                                child: Align(
                                    alignment: Alignment.center,
                                    child: Row(children: <Widget>[
                                      // TIPO DE VIVIENDA
                                      Expanded(
                                        // child:Text('')
                                        child: DropdownButtonFormField(
                                          style: new TextStyle(
                                              fontSize: 17,
                                              color: Colors.black54),
                                          items: cbovivienda
                                              .toSet()
                                              .toList()
                                              .map((Combos data) {
                                            return new DropdownMenuItem(
                                                value: data.codigo,
                                                child: Row(
                                                  children: <Widget>[
                                                    Text(data.descripcion),
                                                  ],
                                                ));
                                          }).toList(),
                                          onChanged: (newValue) async {
                                            setState(() {
                                              alignaux = Alignment.center;
                                              prefs.limpiarcronograma(false);
                                              valuevivienda = newValue;
                                            });
                                          },
                                          value: valuevivienda,
                                        ),
                                      ),
                                      SizedBox(width: 10),
                                      // DESCUENTO 0
                                      Switch(
                                        activeTrackColor: Colors.redAccent,
                                        activeColor: Colors.redAccent[400],
                                        value: isDescuento,
                                        onChanged: (value) {
                                          setState(() {
                                            alignaux = Alignment.center;
                                            prefs.limpiarcronograma(false);
                                            isDescuento = value;
                                            if (clienteauxdata.idvigencia ==
                                                    0 &&
                                                clienteauxdata.idrecurrencia ==
                                                    0) {
                                              isDescuento = true;
                                            }
                                            prefs.limpiarcronograma(false);
                                          });
                                        },
                                      ),
                                      SizedBox(
                                        height: 10.0,
                                      ),
                                      Text(
                                        'Descuento 0.00%',
                                        style: TextStyle(
                                            color: Colors.black,
                                            fontSize: 20.0),
                                      ),
                                      //
                                    ]))),
                            Container(
                                padding: EdgeInsets.all(5.0),
                                child: Align(
                                    alignment: Alignment.center,
                                    child: Row(children: <Widget>[
                                      // DEPARTAMENTO
                                      Expanded(
                                        // child:Text('')
                                        child: DropdownButtonFormField(
                                          style: new TextStyle(
                                              fontSize: 17,
                                              color: Colors.black54),
                                          items: cbodepartamento
                                              .toSet()
                                              .toList()
                                              .map((Combos data) {
                                            return new DropdownMenuItem(
                                                value: data.codigo,
                                                child: Row(
                                                  children: <Widget>[
                                                    Text(data.descripcion),
                                                  ],
                                                ));
                                          }).toList(),
                                          onChanged: (newValue) async {
                                            setState(() {
                                              alignaux = Alignment.center;
                                              prefs.limpiarcronograma(false);
                                              valuedepartamento = newValue;
                                              valuedistrito = 0;
                                              valueprovincia = 0;
                                              valuecentropoblado = 0;
                                              cbodistrito = new List<Combos>();
                                              cbodistrito.add(
                                                  new Combos(0, 'DISTRITO'));
                                              cbocentropoblado =
                                                  new List<Combos>();
                                              cbocentropoblado.add(new Combos(
                                                  0, 'CENTRO POBLADO'));
                                              cboprovincia = new List<Combos>();
                                              cboprovincia.add(
                                                  new Combos(0, 'PROVINCIA'));
                                              if (valuedepartamento > 0) {
                                                Future<List<Categoria>>
                                                    _futureOfList =
                                                    categoriasuperior(
                                                        valuedepartamento);

                                                // dataSelProducto = new List<Producto>();
                                                _futureOfList.then((value) => {
                                                      value
                                                          .toList()
                                                          .forEach((element) {
                                                        cboprovincia.add(
                                                            new Combos(
                                                                element.id,
                                                                element
                                                                    .descripcion));
                                                      })
                                                    });
                                              }
                                            });
                                          },
                                          value: valuedepartamento,
                                        ),
                                      ),
                                      SizedBox(width: 10),
                                      // PROVINCIA
                                      Expanded(
                                        // child:Text('')

                                        child: DropdownButtonFormField(
                                          style: new TextStyle(
                                              fontSize: 17,
                                              color: Colors.black54),
                                          items: cboprovincia
                                              .toSet()
                                              .toList()
                                              .map((Combos data) {
                                            return new DropdownMenuItem(
                                                value: data.codigo,
                                                child: Row(
                                                  children: <Widget>[
                                                    Text(data.descripcion),
                                                  ],
                                                ));
                                          }).toList(),
                                          onChanged: (newValue) async {
                                            setState(() {
                                              alignaux = Alignment.center;
                                              prefs.limpiarcronograma(false);
                                              valueprovincia = newValue;
                                              valuedistrito = 0;
                                              valuecentropoblado = 0;
                                              cbocentropoblado =
                                                  new List<Combos>();
                                              cbocentropoblado.add(new Combos(
                                                  0, 'CENTRO POBLADO'));
                                              cbodistrito = new List<Combos>();
                                              cbodistrito.add(
                                                  new Combos(0, 'DISTRITO'));
                                              if (valueprovincia > 0) {
                                                Future<List<Categoria>>
                                                    _futureOfList =
                                                    categoriasuperior(
                                                        valueprovincia);

                                                // dataSelProducto = new List<Producto>();
                                                _futureOfList.then((value) => {
                                                      value
                                                          .toList()
                                                          .forEach((element) {
                                                        cbodistrito.add(new Combos(
                                                            element.id,
                                                            element
                                                                .descripcion));
                                                      })
                                                    });
                                              }
                                            });
                                          },
                                          value: valueprovincia,
                                        ),
                                      ),
                                      //
                                    ]))),
                            Container(
                                padding: EdgeInsets.all(5.0),
                                child: Align(
                                    alignment: Alignment.center,
                                    child: Row(children: <Widget>[
                                      // DISTRITO
                                      Expanded(
                                        child: DropdownButtonFormField(
                                          style: new TextStyle(
                                              fontSize: 17,
                                              color: Colors.black54),
                                          items: cbodistrito
                                              .toSet()
                                              .toList()
                                              .map((Combos data) {
                                            return new DropdownMenuItem(
                                                value: data.codigo,
                                                child: Row(
                                                  children: <Widget>[
                                                    Text(data.descripcion),
                                                  ],
                                                ));
                                          }).toList(),
                                          onChanged: (newValue) async {
                                            setState(() {
                                              alignaux = Alignment.center;
                                              prefs.limpiarcronograma(false);
                                              valuecentropoblado = 0;
                                              valuedistrito = newValue;
                                              cbocentropoblado =
                                                  new List<Combos>();
                                              cbocentropoblado.add(new Combos(
                                                  0, 'CENTRO POBLADO'));
                                              if (valueprovincia > 0) {
                                                Future<List<CentroPoblado>>
                                                    _futureOfList =
                                                    centropobladodistrito(
                                                        valuedistrito);

                                                // dataSelProducto = new List<Producto>();
                                                _futureOfList.then((value) => {
                                                      if (value.length > 0)
                                                        {
                                                          value
                                                              .toList()
                                                              .forEach(
                                                                  (element) {
                                                            cbocentropoblado
                                                                .add(new Combos(
                                                                    element.id,
                                                                    element
                                                                        .descripcion));
                                                          })
                                                        }
                                                    });
                                              }
                                            });
                                          },
                                          value: valuedistrito,
                                        ),
                                      ),
                                      SizedBox(width: 10),
                                      // CENTRO POBLADO
                                      Expanded(
                                        // child:Text('')
                                        child: DropdownButtonFormField(
                                          style: new TextStyle(
                                              fontSize: 17,
                                              color: Colors.black54),
                                          items: cbocentropoblado
                                              .toSet()
                                              .toList()
                                              .map((Combos data) {
                                            return new DropdownMenuItem(
                                                value: data.codigo,
                                                child: Row(
                                                  children: <Widget>[
                                                    Text(data.descripcion),
                                                  ],
                                                ));
                                          }).toList(),
                                          onChanged: (newValue) async {
                                            setState(() {
                                              alignaux = Alignment.center;
                                              prefs.limpiarcronograma(false);
                                              valuecentropoblado = newValue;
                                            });
                                          },
                                          value: valuecentropoblado,
                                        ),
                                      ),
                                      //
                                    ]))),
                            Container(
                                padding: EdgeInsets.all(5.0),
                                child: Align(
                                    alignment: Alignment.center,
                                    child: Row(children: <Widget>[
                                      // CUOTAS
                                      Expanded(
                                        // child:Text('')
                                        child: DropdownButtonFormField(
                                          style: new TextStyle(
                                              fontSize: 17,
                                              color: Colors.black54),
                                          items: cbocuotas
                                              .toSet()
                                              .toList()
                                              .map((Combos data) {
                                            return new DropdownMenuItem(
                                                value: data.codigo,
                                                child: Row(
                                                  children: <Widget>[
                                                    Text(data.descripcion),
                                                  ],
                                                ));
                                          }).toList(),
                                          onChanged: (newValue) async {
                                            setState(() {
                                              alignaux = Alignment.center;
                                              prefs.limpiarcronograma(false);
                                              valuecuotas = newValue;
                                            });
                                          },
                                          value: valuecuotas,
                                        ),
                                      ),
                                      SizedBox(width: 10),
                                      // AMPLIAR CUOTAS
                                      Switch(
                                        activeTrackColor: Colors.redAccent,
                                        activeColor: Colors.redAccent[400],
                                        value: isAmpliarCta,
                                        onChanged: (value) {
                                          setState(() {
                                            alignaux = Alignment.center;
                                            prefs.limpiarcronograma(false);
                                            isAmpliarCta = value;
                                            cuotaslistar();
                                          });
                                        },
                                      ),
                                      SizedBox(
                                        height: 10.0,
                                      ),
                                      Text(
                                        'Ampliar cuotas',
                                        style: TextStyle(
                                            color: Colors.black,
                                            fontSize: 20.0),
                                      ),
                                      //
                                    ]))),
                            Container(
                                padding: EdgeInsets.all(10.0),
                                child: Align(
                                    alignment: Alignment.bottomRight,
                                    child: Row(children: <Widget>[
                                      Expanded(
                                          child: RaisedButton(
                                              color: Color(0xff3F5D8E),
                                              textColor: Colors.yellow[50],
                                              padding: EdgeInsets.fromLTRB(
                                                  10, 10, 10, 10),
                                              splashColor: Colors.grey,
                                              child: Text(
                                                "Cotizar",
                                                style:
                                                    TextStyle(fontSize: 20.0),
                                              ),
                                              onPressed: () async {
                                                // prefs.limpiarcronograma(false);
                                                validarcotizacion(-1);
                                              })),
                                    ])))
                          ]))
                    ])),
              ])),
            ),
            Align(
                alignment: alignaux,
                child: SingleChildScrollView(
                    child: Column(children: <Widget>[
                  Visibility(
                    visible: isCotiza,
                    child: Container(
                      padding: EdgeInsets.all(10.0),
                      child: SingleChildScrollView(
                        scrollDirection: Axis.horizontal,
                        child: DataTable(
                          columns: [
                            DataColumn(
                                label: Text("N° CUOTA",
                                    style: TextStyle(
                                        fontSize: 18,
                                        fontWeight: FontWeight.bold))),
                            DataColumn(
                                label: Text("SALDO",
                                    style: TextStyle(
                                        fontSize: 18,
                                        fontWeight: FontWeight.bold)),
                                numeric: true),
                            DataColumn(
                              label: Text("AMORTIZACIÓN",
                                  style: TextStyle(
                                      fontSize: 18,
                                      fontWeight: FontWeight.bold)),
                              numeric: true,
                            ),
                            DataColumn(
                              label: Text("INTERES",
                                  style: TextStyle(
                                      fontSize: 18,
                                      fontWeight: FontWeight.bold)),
                              numeric: true,
                            ),
                            DataColumn(
                              label: Text("GASTO COBRANZA",
                                  style: TextStyle(
                                      fontSize: 18,
                                      fontWeight: FontWeight.bold)),
                              numeric: true,
                            ),
                            DataColumn(
                              label: Text("IGV",
                                  style: TextStyle(
                                      fontSize: 18,
                                      fontWeight: FontWeight.bold)),
                              numeric: true,
                            ),
                            DataColumn(
                              label: Text("MONTO CUOTA",
                                  style: TextStyle(
                                      fontSize: 18,
                                      fontWeight: FontWeight.bold)),
                              numeric: true,
                            ),
                          ],
                          rows: cronogramaData
                              .map(
                                (cuota) => DataRow(cells: [
                                  DataCell(
                                    Text(
                                        cuota.nrocuota.toString() == "0"
                                            ? 'INICIAL'
                                            : cuota.nrocuota.toString(),
                                        style: TextStyle(fontSize: 17)),
                                  ),
                                  DataCell(
                                    Text('S/ ${cuota.saldo.toStringAsFixed(2)}',
                                        style: TextStyle(fontSize: 17)),
                                  ),
                                  DataCell(
                                    Text(
                                        'S/ ${cuota.amortizacion.toStringAsFixed(2)}',
                                        style: TextStyle(fontSize: 17)),
                                  ),
                                  DataCell(
                                    Text(
                                        'S/ ${cuota.interes.toStringAsFixed(2)}',
                                        style: TextStyle(fontSize: 17)),
                                  ),
                                  DataCell(
                                    Text('S/ ${cuota.gasto.toStringAsFixed(2)}',
                                        style: TextStyle(fontSize: 17)),
                                  ),
                                  DataCell(
                                    Text('S/ ${cuota.igv.toStringAsFixed(2)}',
                                        style: TextStyle(fontSize: 17)),
                                  ),
                                  DataCell(
                                    Text(
                                        'S/ ${cuota.facturar.toStringAsFixed(2)}',
                                        style: TextStyle(fontSize: 17)),
                                  ),
                                ]),
                              )
                              .toList(),
                        ),
                      ),
                    ),
                  ),
                  Visibility(
                    visible: isCotiza,
                    child: Container(
                      padding: EdgeInsets.all(5.0),
                      child: Align(
                        alignment: Alignment.center,
                        child: Row(children: <Widget>[
                          Expanded(
                              // child:Text('')
                              child: TextForm(
                                  show: true,
                                  icon: FontAwesomeIcons.handHoldingUsd,
                                  item: TextFormField(
                                    textInputAction: TextInputAction.done,
                                    keyboardType:
                                        TextInputType.numberWithOptions(
                                            decimal: true),
                                    style: new TextStyle(fontSize: 18),
                                    maxLength: 10,
                                    // initialValue:
                                    //     pagoinicial.toStringAsFixed(2) ??
                                    //         "0.00",
                                    controller: pagoInicialCtrl,
                                    autocorrect: false,
                                    onChanged: (newValue) {
                                      prefs.limpiarcronograma(true);
                                      setState(() {
                                        print(newValue);
                                        if (newValue.isEmpty) {
                                          newValue = "0";
                                        } else if (double.parse(newValue) >=
                                                1 &&
                                            newValue.substring(
                                                    0, newValue.length - 1) ==
                                                "0" &&
                                            newValue.length == 2) {
                                          newValue =
                                              int.parse(newValue).toString();
                                        }
                                        pagoInicialCtrl.text = newValue;
                                        pagoinicial =
                                            double.parse(newValue.toString());
                                        pagoInicialCtrl.selection =
                                            TextSelection.fromPosition(
                                                TextPosition(
                                                    offset: pagoInicialCtrl
                                                        .text.length));
                                      });
                                    },
                                    decoration: new InputDecoration(
                                      labelText: 'Inicial',
                                    ),
                                  ))),
                          SizedBox(
                            height: 10.0,
                          ),
                          SizedBox(
                              height: 40.0,
                              width: 50,
                              child: RaisedButton(
                                  color: Colors.blueAccent,
                                  textColor: Colors.yellow[50],
                                  padding: EdgeInsets.fromLTRB(10, 10, 10, 10),
                                  splashColor: Colors.blueAccent,
                                  child: Icon(Icons.refresh),
                                  onPressed: () async {
                                    setState(() {
                                      validarcotizacion(pagoinicial);
                                    });
                                  })),
                          SizedBox(
                            width: 20.0,
                          ),
                          Expanded(
                            // child:Text('')
                            child: DropdownButtonFormField(
                              style: new TextStyle(
                                  fontSize: 17, color: Colors.black54),
                              items: canaldata
                                  .toSet()
                                  .toList()
                                  .map((Combos canalx) {
                                return new DropdownMenuItem(
                                    value: canalx.codigo,
                                    child: Row(
                                      children: <Widget>[
                                        Text(canalx.descripcion),
                                      ],
                                    ));
                              }).toList(),
                              onChanged: (newValue) async {
                                setState(() {
                                  valuecanal = newValue;
                                });
                              },
                              value: valuecanal,
                            ),
                          )
                        ]),
                      ),
                    ),
                  ),
                  // RESULTADOS DE COTIZACIÓN
                  Visibility(
                    visible: isCotiza,
                    child: Container(
                      width: double.infinity,
                      padding: new EdgeInsets.all(5.0),
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.start,
                        children: <Widget>[
                          Container(
                            width: double.infinity,
                            height: 45,
                            decoration: BoxDecoration(
                              border: Border.all(color: Color(0xff555B65)),
                              color: Color(0xff555B65),
                            ),
                            child: Center(
                                child: Text("RESULTADO DE COTIZACIÓN",
                                    style: new TextStyle(
                                      fontWeight: FontWeight.bold,
                                      fontSize: 24,
                                      color: Colors.white,
                                    ),
                                    textAlign: TextAlign.center)),
                          ),
                          Container(
                              width: double.infinity,
                              padding: new EdgeInsets.all(10.0),
                              decoration: BoxDecoration(
                                border: Border.all(
                                    color: Color(0xff555B65), width: 5),
                              ),
                              child: Row(children: <Widget>[
                                Column(children: <Widget>[
                                  Container(
                                      width: MediaQuery.of(context).size.width *
                                          0.5,
                                      child: Column(children: <Widget>[
                                        Row(children: <Widget>[
                                          Text(
                                            "PRECIO LISTA: ",
                                            style: new TextStyle(
                                              fontWeight: FontWeight.bold,
                                              fontSize: 17,
                                            ),
                                            textAlign: TextAlign.start,
                                          ),
                                          Text(
                                            "S/ ${preciolista.toStringAsFixed(2)} ",
                                            style: new TextStyle(
                                              fontSize: 17,
                                            ),
                                            textAlign: TextAlign.end,
                                          )
                                        ]),
                                        SizedBox(
                                          height: 10,
                                        ),
                                        Row(children: <Widget>[
                                          Text(
                                            "DESCUENTO: ",
                                            style: new TextStyle(
                                              fontWeight: FontWeight.bold,
                                              fontSize: 17,
                                            ),
                                            textAlign: TextAlign.start,
                                          ),
                                          Text(
                                            "S/ ${descuento.toStringAsFixed(2)}",
                                            style: new TextStyle(
                                              fontSize: 17,
                                            ),
                                            textAlign: TextAlign.start,
                                          )
                                        ]),
                                        SizedBox(
                                          height: 10,
                                        ),
                                        Row(children: <Widget>[
                                          Text(
                                            "PRECIO CONTADO: ",
                                            style: new TextStyle(
                                              fontWeight: FontWeight.bold,
                                              fontSize: 17,
                                            ),
                                            textAlign: TextAlign.start,
                                          ),
                                          Text(
                                            "S/ ${precioreal.toStringAsFixed(2)}",
                                            style: new TextStyle(
                                              fontSize: 17,
                                            ),
                                            textAlign: TextAlign.start,
                                          )
                                        ]),
                                        SizedBox(
                                          height: 10,
                                        ),
                                        Row(children: <Widget>[
                                          Text(
                                            "PORCENTAJE INICIAL: ",
                                            style: new TextStyle(
                                              fontWeight: FontWeight.bold,
                                              fontSize: 17,
                                            ),
                                            textAlign: TextAlign.start,
                                          ),
                                          Text(
                                            "${porcentajeinicial.toStringAsFixed(2)}%",
                                            style: new TextStyle(
                                              fontSize: 17,
                                            ),
                                            textAlign: TextAlign.start,
                                          )
                                        ]),
                                        SizedBox(
                                          height: 10,
                                        ),
                                        Row(children: <Widget>[
                                          Text(
                                            "PLAZO INICIAL: ",
                                            style: new TextStyle(
                                              fontWeight: FontWeight.bold,
                                              fontSize: 17,
                                            ),
                                            textAlign: TextAlign.start,
                                          ),
                                          Text(
                                            "${plazoinicial.toString()}",
                                            style: new TextStyle(
                                              fontSize: 17,
                                            ),
                                            textAlign: TextAlign.start,
                                          )
                                        ]),
                                      ]))
                                ]),
                                Column(children: <Widget>[
                                  Container(
                                      width: MediaQuery.of(context).size.width *
                                          0.4,
                                      child: Column(children: <Widget>[
                                        Row(children: <Widget>[
                                          Text(
                                            "PAGO INICIAL: ",
                                            style: new TextStyle(
                                              fontWeight: FontWeight.bold,
                                              fontSize: 17,
                                            ),
                                            textAlign: TextAlign.start,
                                          ),
                                          Text(
                                            "S/ ${pagoinicial.toStringAsFixed(2)}",
                                            style: new TextStyle(
                                              fontSize: 17,
                                              color: Colors.redAccent,
                                            ),
                                            textAlign: TextAlign.start,
                                          )
                                        ]),
                                        SizedBox(
                                          height: 10,
                                        ),
                                        Row(children: <Widget>[
                                          Text(
                                            "PRÉSTAMO: ",
                                            style: new TextStyle(
                                              fontWeight: FontWeight.bold,
                                              fontSize: 17,
                                            ),
                                            textAlign: TextAlign.start,
                                          ),
                                          Text(
                                            "S/ ${prestamo.toStringAsFixed(2)}",
                                            style: new TextStyle(
                                              fontSize: 17,
                                            ),
                                            textAlign: TextAlign.start,
                                          )
                                        ]),
                                        SizedBox(
                                          height: 10,
                                        ),
                                        Row(children: <Widget>[
                                          Text(
                                            "CUOTAS: ",
                                            style: new TextStyle(
                                              fontWeight: FontWeight.bold,
                                              fontSize: 17,
                                            ),
                                            textAlign: TextAlign.start,
                                          ),
                                          Text(
                                            "${valuecuotas.toString()}",
                                            style: new TextStyle(
                                              fontSize: 17,
                                            ),
                                            textAlign: TextAlign.start,
                                          )
                                        ]),
                                        SizedBox(
                                          height: 10,
                                        ),
                                        Row(children: <Widget>[
                                          Text(
                                            "TOTAL CUOTAS: ",
                                            style: new TextStyle(
                                              fontWeight: FontWeight.bold,
                                              fontSize: 17,
                                            ),
                                            textAlign: TextAlign.start,
                                          ),
                                          Text(
                                            "S/ ${totalcronograma.toStringAsFixed(2)}",
                                            style: new TextStyle(
                                              fontSize: 17,
                                              color: Colors.redAccent,
                                            ),
                                            textAlign: TextAlign.start,
                                          )
                                        ]),
                                        SizedBox(
                                          height: 10,
                                        ),
                                        Row(children: <Widget>[
                                          Text(
                                            "TOTAL VENTA: ",
                                            style: new TextStyle(
                                              fontWeight: FontWeight.bold,
                                              fontSize: 17,
                                            ),
                                            textAlign: TextAlign.start,
                                          ),
                                          Text(
                                            "S/ ${totalventa.toStringAsFixed(2)}",
                                            style: new TextStyle(
                                              fontSize: 17,
                                              color: Colors.redAccent,
                                            ),
                                            textAlign: TextAlign.start,
                                          )
                                        ])
                                      ]))
                                ]),
                                Column(children: <Widget>[
                                  SizedBox(
                                    height: 5,
                                  ),
                                  Divider(
                                    height: 5,
                                  ),
                                  SizedBox(
                                    height: 10,
                                  ),
                                ]),
                              ])

                              //   child:Column(
                              //     children: <Widget>[
                              //
                              //       SizedBox(height: 5,),
                              //       Divider(height: 5,),
                              //       SizedBox(height: 5,),
                              //

                              //       SizedBox(height: 5,),
                              //       Divider(height: 5,),
                              //       SizedBox(height: 5,),
                              //

                              //     ],
                              // ),

                              ),
                          SizedBox(
                            width: 20,
                            height: 30,
                          ),
                        ],
                      ),
                    ),
                  ),
                  Visibility(
                      visible: isCotiza,
                      child: Center(
                          child: TextForm(
                              show: true,
                              icon: FontAwesomeIcons.locationArrow,
                              item: TextFormField(
                                style: new TextStyle(fontSize: 17),
                                maxLength: 200,
                                initialValue: clienteauxdata.direccion == 'NULL'
                                    ? ''
                                    : clienteauxdata.direccion,
                                decoration: new InputDecoration(
                                  labelText: 'Dirección',
                                ),
                                onChanged: (newValue) {
                                  setState(() {
                                    direccionCtrl = newValue;
                                    clienteauxdata.direccion = newValue;
                                  });
                                },
                                validator: validaClave,
                              )))),
                  Visibility(
                    visible: isCotiza,
                    child: Container(
                      padding: EdgeInsets.all(5.0),
                      child: Align(
                        alignment: Alignment.center,
                        child: Row(children: <Widget>[
                          Expanded(
                              // child:Text('')
                              child: TextForm(
                                  show: true,
                                  icon: Icons.phone,
                                  item: TextFormField(
                                    style: new TextStyle(fontSize: 17),
                                    maxLength: 9,
                                    initialValue:
                                        clienteauxdata.telefono == 'NULL'
                                            ? ''
                                            : clienteauxdata.telefono,
                                    decoration: new InputDecoration(
                                      labelText: 'Teléfono/Celular',
                                    ),
                                    onChanged: (newValue) {
                                      setState(() {
                                        telefonoCtrl = newValue;
                                        clienteauxdata.telefono = newValue;
                                      });
                                    },
                                    validator: validaClave,
                                  ))),
                          SizedBox(
                            height: 10.0,
                          ),
                          Expanded(
                            // child:Text('')
                            child: DropdownButtonFormField(
                              style: new TextStyle(
                                  fontSize: 17, color: Colors.black54),
                              items: cboguardar
                                  .toSet()
                                  .toList()
                                  .map((Combos canalx) {
                                return new DropdownMenuItem(
                                    value: canalx.codigo,
                                    child: Row(
                                      children: <Widget>[
                                        Text(canalx.descripcion),
                                      ],
                                    ));
                              }).toList(),
                              onChanged: (newValue) async {
                                setState(() {
                                  valueguardar = newValue;
                                });
                              },
                              value: valueguardar,
                            ),
                          )
                        ]),
                      ),
                    ),
                  ),

                  Visibility(
                    visible: isCotiza,
                    child: Container(
                      padding: EdgeInsets.all(5.0),
                      child: Align(
                        alignment: Alignment.center,
                        child: Row(children: <Widget>[
                          Expanded(
                              child: RaisedButton(
                                  color: Color(0xff3F5D8E),
                                  textColor: Colors.yellow[50],
                                  padding: EdgeInsets.fromLTRB(10, 10, 10, 10),
                                  splashColor: Colors.grey,
                                  child: Text(
                                    "Guardar",
                                    style: TextStyle(fontSize: 20.0),
                                  ),
                                  onPressed: () async {
                                    setState(() async {
                                      if (prefs.validarregistro()) {
                                        alignaux = Alignment.center;
                                        guardarCotizacion();
                                      }
                                    });
                                  })),
                          SizedBox(height: 5.0, width: 20),
                        ]),
                      ),
                    ),
                  ),

                  Visibility(
                    visible: !isCotiza,
                    child: Center(
                        child: Container(
                      padding: new EdgeInsets.all(10.0),
                      width: MediaQuery.of(context).size.width * 0.9,
                      decoration: BoxDecoration(
                          border: Border.all(color: Colors.blueAccent),
                          borderRadius:
                              BorderRadius.all(Radius.circular(10.0))),
                      child: FittedBox(
                        child: Text(
                          "Tiene que completar los parámetros de cotización.",
                          textAlign: TextAlign.center,
                          style: TextStyle(color: Colors.blue),
                        ),
                      ),
                    )),
                  )
                ]))),
          ],
        ),
        floatingActionButton: FloatingActionButton(
          onPressed: () {
            setState(() {
              prefs.toastmessage("Se canceló el registro de la cotización",
                  Colors.redAccent, Colors.white);
              prefs.limpiarcotizacion();
              prefs.clienteencontrado = false;
              prefs.datosevaluar = true;
              Navigator.push(
                  context,
                  new MaterialPageRoute(
                      builder: (context) => EvaluarCliente()));
              alignaux = Alignment.center;
            });
          },
          child: Icon(FontAwesomeIcons.times),
          backgroundColor: Colors.redAccent,
        ),
      ),
    );
  }

  validarcotizacion(double inicial) async {
    preciolista = 0.0;
    precioreal = 0.0;
    descuento = 0.0;
    bool _ban = true;
    double montototal = 0;
    double inicialaux = 0;
    if (inicial == -1) {
      _ban = prefs.validarparametroscotizacion();
    }
    if (isCombo) {
      if (dataSelProducto
              .where((element) =>
                  element.codigolinea == 50 || element.codigolinea == 51)
              .toList()
              .length ==
          0) {
        prefs.toastmessage(
            "Tiene que agregar productos melamina para efectuar el combo.",
            Colors.redAccent,
            Colors.white);
        return false;
      }
      if (dataSelProducto.length < 2) {
        prefs.toastmessage("Tiene que tener en lista mínimo 2 productos.",
            Colors.redAccent, Colors.white);
        return false;
      }
      double combomin = await db.getComboMontoMin();
      double montomel = 0.0;
      dataSelProducto
          .where((element) =>
              element.codigolinea == 50 || element.codigolinea == 51)
          .toList()
          .forEach((element) {
        montomel += element.precioreal;
      });
      if (combomin > montomel) {
        prefs.toastmessage(
            "La suma de monto en productos leoncito es de S/ ${montomel.toStringAsFixed(2)} \n y para efectuar el combo el monto mínimo debe ser de S/ ${combomin.toStringAsFixed(2)}.",
            Colors.redAccent,
            Colors.white);
        return false;
      }
    }
    if (_ban) {
      alignaux = Alignment.topRight;
      String vivienda = "";
      switch (valuevivienda) {
        case 8:
          vivienda = "PROPIA";
          break;
        case 9:
          vivienda = "FAMILIAR";
          break;
        case 10:
          vivienda = "ALQUILADA";
          break;
      }
      res = await db.obtenerparametroscotizacion(
          (valuetipocot == 'MOTO' ? 1 : 0),
          valuecuotas.toString(),
          edadaux,
          vivienda,
          (isDescuento ? 0 : clienteauxdata.idnivelriesgo),
          valuetipocot,
          (isDescuento ? 0 : clienteauxdata.idvigencia),
          (isDescuento ? 0 : clienteauxdata.idrecurrencia),
          valuecentropoblado);

      setState(() {
        isCotiza = true;
        dataSelProducto.forEach((element) {
          if (isCombo) {
            if (element.codigolinea == 50 || element.codigolinea == 51) {
              element.dsctocombo = res.dsctocomboleo;
            } else {
              element.dsctocombo = res.dsctocombootros;
            }
          } else {
            element.dsctocombo = 0;
          }
          element.dsctosegmento = res.dsctosegmentacion * 100;
          element.dsctonivel = res.dsctonivelriesgo * 100;
          element.dsctototal =
              element.dsctocombo + res.dsctosegmentacion + res.dsctonivelriesgo;
          element.preciodscto = double.parse(
              (element.preciototal * element.dsctototal).toStringAsFixed(2));
          element.precioreal = element.preciototal - element.preciodscto;
          montototal += element.precioreal;
          preciolista += element.preciototal;
          precioreal += element.precioreal;
          descuento += element.preciodscto;
        });

        plazoinicial = res.plazoinicial;
        print(inicial.toString());
        print("INICIAL CRONOGRAMA" + (montototal * res.porcinicial).toString());
        if (inicial == -1) {
          inicialaux = valuetipocot == 'MOTO'
              ? res.montoinicial
              : montototal * res.porcinicial;
          inicialorig = inicialaux;
        } else {
          inicialaux = inicial;
        }
        print("inicial aux " + inicialaux.toString());
        prestamo = precioreal - inicialaux;

        porcentajeinicial = (inicialaux / precioreal) * 100;
        prefs.vercronograma(valuecuotas, prestamo, (prestamo * res.factor),
            res.tem, res.factor, res.factortem, res.igv, inicialaux);
        prefs.toastmessage(
            "Cronograma ha sido generado.", Colors.greenAccent, Colors.black);
      });
    } else {
      setState(() {
        alignaux = Alignment.center;
      });
    }

    setState(() {
      pagoinicial = inicialaux;
      pagoInicialCtrl.text = pagoinicial.toStringAsFixed(2);
    });
  }

  onSelectedRow(bool selected, Producto product) async {
    setState(() {
      if (selected) {
        selectedProductos.add(product);
      } else {
        selectedProductos.remove(product);
      }
    });
  }

  estadovariables(String msj, bool state) async {
    setState(() {
      showmsj = state;
      mensajeval = msj;
    });

    await Future.delayed(const Duration(milliseconds: 2500), () async {
      if (mounted) {
        setState(() {
          showmsj = false;
          mensajeval = "";
        });
      }
    });
  }

  cuotaslistar() {
    valuecuotas = 0;
    cbocuotas = [new Combos(0, 'CUOTAS')];
    Future<List<Afectaciones>> _futureOfListCtas =
        prefs.cargarcuotas(isAmpliarCta, isCombo);
    _futureOfListCtas.then((value) => {
          value.toList().forEach((element) {
            cbocuotas.add(new Combos(
                int.parse(element.descripcion), element.descripcion));
          })
        });
  }

  Future<List<Cliente>> getData(String filter) async {
    Future<List<Cliente>> _futureOfList = clientesData();
    setState(() {
      dataCliente = new List<Cliente>();
      _futureOfList.then((value) => {
            value.toList().forEach((element) {
              dataCliente.add(element);
            })
          });
    });
    return dataCliente;
  }

  Future<List<Cliente>> clientesData() async {
    return await db.fetchCliente();
  }

  Future<List<Producto>> productosData() async {
    return await db.fetchProductoFilter(valuetipocot);
  }

  Future<List<Categoria>> categoriasuperior(int codigo) async {
    List<Categoria> dataCat = await db.fetchCategoriaSuperior(codigo);
    return dataCat;
  }

  Future<List<CentroPoblado>> centropobladodistrito(int codigo) async {
    List<CentroPoblado> dataCat = await db.fetchCentroPobladoDistrito(codigo);
    return dataCat;
  }

  guardarCotizacion() async {
    try {
      int idCotizacion = (await db.maxIdCotizacion())[0]['idmax'];
      int idDetalleCotizacion = (await db.maxIdDetalleCotizacion())[0]['idmax'];
      int idCronograma = (await db.maxIdCronograma())[0]['idmax'];
      Usuario usuarioaux = await db.fetchUsuario();
      Map<String, dynamic> useraux = jsonDecode(prefs.usuario);
      List<double> cuotalst = new List<double>();
      cronogramaData.forEach((element) {
        cuotalst.add(element.monto - element.igv);
      });
      idCotizacion = idCotizacion == null ? 1 : idCotizacion + 1;
      idDetalleCotizacion =
          idDetalleCotizacion == null ? 1 : idDetalleCotizacion + 1;
      idCronograma = idCronograma == null ? 1 : idCronograma + 1;
      var tcemaux = prefs.obtenertcem(cuotalst, (precioreal - pagoinicial));
      var tceaaux = pow((1 + tcemaux), 12) - 1;
      final mdCotizacion = Cotizacion(
        id: idCotizacion,
        dni: clienteauxdata.dni,
        enviada: 0,
        combo: isCombo ? 1 : 0,
        edad: edadaux,
        idvivienda: valuevivienda,
        idsituacionlaboral: "0",
        idestabilidadlaboral: "0",
        idgiroactividad: "0",
        idzona: prefs.local.toString(),
        cuotas: valuecuotas,
        excepcion: isAmpliarCta ? 1 : 0,
        porcentajeinicial: porcentajeinicial,
        plazoinicial: plazoinicial,
        pagoinicial: pagoinicial,
        preciocompra: preciolista,
        descuentopromo: descuento,
        precioreal: precioreal,
        montocuota: ((prestamo - (prestamo * res.porcinicial)) * res.factor),
        montocredito: totalcronograma,
        totalcredito: totalventa,
        observacion: clienteauxdata.resultado,
        negociacion: valueguardar == 1 ? 1 : 0,
        idresultado: clienteauxdata.idresultado,
        idcontacto: clienteauxdata.id,
        idpersonalregistro: usuarioaux.id,
        asesorvta: usuarioaux.id,
        proceso: valueguardar,
        direccion: clienteauxdata.direccion,
        telefono: clienteauxdata.telefono,
        codigoubigeo:
            "${valuedistrito.toString()}-${valuecentropoblado.toString()}",
        factor: res.factor,
        factortem: res.factortem,
        tea: res.tea,
        tem: res.tem,
        tceaimp: res.tceaimp,
        tcemimp: res.tcemimp,
        tcea: tcemaux,
        tcem: tceaaux,
        igv: res.igv,
        inicialorig: inicialorig,
        medio: valuecanal,
        fecharegistro:
            DateFormat("yyyy-MM-dd HH:mm:ss").format(DateTime.now()).toString(),
      );
      db.insertCotizacion(mdCotizacion);
      dataSelProducto.forEach((element) {
        final mdDetalleCotizacion = DetalleCotizacion(
          id: idDetalleCotizacion++,
          cantidad: element.cantidad,
          idproducto: element.id,
          descripcion:
              "${element.nombrelinea.toString()}  ${element.nombremodelo.toString()} - ${element.nombremarca.toString()}",
          preciounitario: element.precio,
          preciototal: element.preciototal,
          dsctoperfilcrediticio: element.dsctonivel,
          dsctosegmentacion: element.dsctosegmento,
          dsctolineaproducto: element.dsctolinea,
          dsctoplazo: element.dsctoplazo,
          dsctocombo: element.dsctocombo,
          dsctoexcepcion: element.dsctoexcepcion,
          descuentototal: element.dsctototal,
          precioreal: element.precioreal,
          idcotizacion: idCotizacion,
        );
        db.insertDetalleCotizacion(mdDetalleCotizacion);
      });

      cronogramaData.forEach((element) {
        final mdCronograma = Cronograma(
          id: idCronograma++,
          nrocuota: element.nrocuota,
          amortizacion: element.amortizacion,
          interes: element.interes,
          gasto: element.gasto,
          monto: element.monto,
          igv: element.igv,
          saldo: element.saldo,
          facturar: element.facturar,
          idcotizacion: idCotizacion,
        );
        db.insertCronograma(mdCronograma);
      });
      var stateaux = await conectnet();
      if (stateaux) {
        prefs.enviarcotizacionapi(idCotizacion);
      }
      List<Cotizacion> dataax = await db.fetchCotizacion(true);
      setState(() {
        prefs.pendientes = dataax.length;
      });
      prefs.limpiarcotizacion();
      prefs.toastmessage("Se realizó el registro de la cotización",
          Colors.greenAccent, Colors.black);
      Navigator.push(context,
          new MaterialPageRoute(builder: (context) => EvaluarCliente()));
      // reportView(context, idCotizacion);

    } catch (e, _) {
      prefs.toastmessage("No se efectuó el registro.\n ${e.toString()}",
          Colors.redAccent, Colors.white);
    }
  }

  cargardatos() async {
    clienteaux = await db.getCliente(prefs.dnicliente);
    edad = calcularedad(clienteaux.fechanacimiento);
  }

  int calcularedad(String fechanac) {
    // error_log($fechanacimiento);
    var fecha = formatter.format(DateTime.now()).toString().split('-');
    var fechanacaux = fechanac.split('-');
    int anios = (int.parse(fecha[0]) - int.parse(fechanacaux[0]));
    int meses = (int.parse(fecha[1]) - int.parse(fechanacaux[1]));
    int dias = (int.parse(fecha[2]) - int.parse(fechanacaux[2]));
    if (meses < 0) {
      anios -= 1;
    } else if (meses == 0) {
      if (dias < 0) anios -= 1;
    }
    return anios;
  }
}
