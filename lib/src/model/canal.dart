class Canal{  
  final int id;
  final String descripcion;
  final int tipo;

  Canal({this.id,this.descripcion,this.tipo});

  Map<String,dynamic> toMap(){ // used when inserting data to the database
    return <String,dynamic>{
      "id" : id,
      "descripcion" : descripcion,
      "tipo" : tipo
    };
  }
}