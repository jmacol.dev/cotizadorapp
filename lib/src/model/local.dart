class Local{  
  final int codigo;
  final String descripcion;
  final String tipo;
  final String estado;

  Local({this.codigo,this.descripcion,this.tipo,this.estado});

  Map<String,dynamic> toMap(){ // used when inserting data to the database
    return <String,dynamic>{
      "codigo" : codigo,
      "descripcion" : descripcion,
      "tipo" : tipo,
      "estado" : estado,
    };
  }
}