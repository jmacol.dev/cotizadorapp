import 'package:appcotizador/src/pages/evaluarcliente.dart';
import 'package:appcotizador/src/widgets/menu.dart';
import 'package:flutter/material.dart';
import 'package:flutter_full_pdf_viewer/full_pdf_viewer_scaffold.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';

class PdfViewerPage extends StatelessWidget {
  final String path;
  const PdfViewerPage({Key key, this.path}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(      
      body: Stack(
        children: <Widget>[
          
          PDFViewerScaffold(
            appBar: AppBar(
              backgroundColor: Color(0xff084B8A),
              title: Text("Reporte de Cotización"),
              // actions: <Widget>[
              //   IconButton(
              //     icon: Icon(FontAwesomeIcons.save),
              //     onPressed: () => {
              //       Navigator.pushReplacementNamed(context, 'evaluar')
              //     },
              //   )
              // ],
            ),
            path: path,
            
          ),
          Padding(                
                    padding: EdgeInsets.only(top:15),
                    child:   
          FloatingActionButton(
            
          onPressed: () async {
            
            // await db.deleteCotizaciones();
            // prefs.pendientes = (await db.fetchCotizacion(true)).length;
            Navigator.pushReplacementNamed(context, 'evaluar');
            // Add your onPressed code here!
          },
          child: Icon(FontAwesomeIcons.userTag),
          backgroundColor: Color(0xff084B8A),
        ), )
        ],
      ),
      
    );
    
  }
}