class CentroPoblado{  
  final int id;
  final String descripcion;
  final int idsegmentacion;
  final int iddistrito;

  CentroPoblado({this.id,this.descripcion,this.idsegmentacion, this.iddistrito});

  Map<String,dynamic> toMap(){ // used when inserting data to the database
    return <String,dynamic>{
      "id" : id,
      "descripcion" : descripcion,
      "idsegmentacion" : idsegmentacion,
      "iddistrito":iddistrito
    };
  }
}